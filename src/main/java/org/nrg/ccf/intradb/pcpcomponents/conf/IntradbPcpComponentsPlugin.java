package org.nrg.ccf.intradb.pcpcomponents.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbPcpComponentsPlugin",
			name = "Intradb PCP Components Plugin"
		)
@ComponentScan({ 
		"org.nrg.ccf.intradb.pcpcomponents.conf",
		"org.nrg.ccf.intradb.pcpcomponents.components",
		"org.nrg.ccf.intradb.pcpcomponents.xapi"
	})
public class IntradbPcpComponentsPlugin {
	
	public static Logger logger = Logger.getLogger(IntradbPcpComponentsPlugin.class);

	public IntradbPcpComponentsPlugin() {
		logger.info("Configuring the Intradb PCP Components Plugin.");
	}
	
}
