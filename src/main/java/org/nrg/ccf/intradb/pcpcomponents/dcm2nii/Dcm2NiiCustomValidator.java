package org.nrg.ccf.intradb.pcpcomponents.dcm2nii;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;


/**
 * The Class Dcm2NiiCustomValidator.
 */
@PipelineValidator
public class Dcm2NiiCustomValidator extends Dcm2NiiValidator {

	/** The Constant TFMRI. */
	protected static final String TFMRI = "tfMRI";

	/** The Constant RFMRI. */
	protected static final String RFMRI = "rfMRI";

	/** The Constant SKIP_SPLIT_FRAME_BY_SERIES_DESC. */
	protected static final String SKIP_SPLIT_FRAME_BY_SERIES_DESC = "skip_split_frame_by_series_desc";

	/** The scans to check. */
	protected final List<String> scansToCheck= new ArrayList<String>(Arrays.asList(RFMRI,TFMRI));

	/**
	 * Initialize scan count values.
	 *
	 * @param entity the entity
	 */
	@Override
	protected void initializeScanCountValues(PcpStatusEntity entity) {
		if (skipScanBySeriesDesc==null) {
			try {
				String submitterClassName = getSubmitterClassNameForDCM2NiiPipeline(entity);
				final String yamlConfig = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(), 
						submitterClassName!=null?submitterClassName:this.getClass().getName(),
								"-parameters-yaml", false).toString();
			
				ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
				@SuppressWarnings("unchecked")
				Map<String, Map<String,String>> fileMap = mapper.readValue(yamlConfig, Map.class);
				
				for(Entry<String, Map<String, String>> entry : fileMap.entrySet()) {
					if(SKIP_SPLIT_FRAME_BY_SERIES_DESC.equalsIgnoreCase(entry.getKey()))
					{
						Map<String,String> values = (Map<String, String>) entry.getValue();
						if(values.get("value") != null) {
							skipScanBySeriesDesc=Arrays.asList(values.get("value").split(","));
						}
					}
				}
			} catch (Exception e) {
				skipScanBySeriesDesc = null;
			}
		}
	}

	/**
	 * Gets the submitter class name for DCM2nii pipeline.
	 *
	 * @param entity the entity
	 * @return the submitter class name for DCM 2 nii pipeline
	 * @throws PcpAutomationScriptExecutionException the pcp automation script execution exception
	 */
	private String getSubmitterClassNameForDCM2NiiPipeline(PcpStatusEntity entity)
			throws PcpAutomationScriptExecutionException {
		String submitterClassName=null;
		final List<PcpConfigInfo> configInfoList = PcpComponentUtils.getPcpConfigInfoList(entity.getProject());
		for (Iterator<PcpConfigInfo> iterator = configInfoList.iterator(); iterator.hasNext();) {
			PcpConfigInfo pcpConfigInfo = iterator.next();
			if (this.getClass().getName().equals(pcpConfigInfo.getValidator())) {
				submitterClassName=pcpConfigInfo.getSubmitter();
			}

		}
		return submitterClassName;
	}

	/**
	 * Nifti count ok?.
	 *
	 * @param scanType the scan type
	 * @param resource the resource
	 * @param seriesDescription the series description
	 * @return true, if successful
	 */
	@Override
	protected boolean niftiCountOk(XnatMrsessiondata session, String scanType, XnatResource resource, String seriesDescription) {

		if (resource == null || !_niftiCounts.containsKey(scanType)) {
			return true;
		}
		List<Integer> count=null;
		if(scansToCheck.contains(scanType) && skipScanBySeriesDesc.contains(seriesDescription))
		{
			count=Arrays.asList(new Integer[] {_niftiCounts.get(scanType).get(0)-1});
		}
		else
		{
			count=_niftiCounts.get(scanType);
		}
		final List<Integer> expectedCounts = count;
		final Integer fileCount = resource.getCorrespondingFiles(CommonConstants.ROOT_PATH).size();
		return expectedCounts.contains(fileCount);

	}
}
