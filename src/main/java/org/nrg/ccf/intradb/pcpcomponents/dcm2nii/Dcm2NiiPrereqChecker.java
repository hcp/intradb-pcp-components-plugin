package org.nrg.ccf.intradb.pcpcomponents.dcm2nii;

import java.util.Date;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ScanUtils;
import org.nrg.ccf.intradb.pcpcomponents.prepipeline.PrePipelineValidator;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class Dcm2NiiPrereqChecker implements PipelinePrereqCheckerI {

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		// We're ready for DCM2NII if we have FaceMasking output.
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		boolean sessionOk = true;
		if (!PrePipelineValidator.prePipelineValidate(statusEntity.getProject(),session,user)) {
			sessionOk = false;
			infoSB.append("<li>PrePipeline appears not to have run successfully for this session.</li>");
		}
		for (XnatImagescandataI scan : session.getScans_scan()) {
			final String scanType = scan.getType();
			if (!(scanType.matches("^[Tt][12][Ww].*$") || scanType.matches("Bias_Receive")) || scanType.contains("setter")) {
				continue;
			}
			final String seriesDesc = scan.getSeriesDescription();
			final String scanId = scan.getId();
			boolean hasDicomOrig = false;
			boolean dicomOrigFileCountOk = false;
			boolean hasDicom = false;
			boolean dicomFileCountOk = false;
			final boolean isUnusable = (scan.getQuality() != null) ? scan.getQuality().equalsIgnoreCase("unusable") : false;
			Integer scanFrames = scan.getFrames();
			for (XnatAbstractresourceI resource : scan.getFile()) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				final String resLabel = resource.getLabel();
				if (resLabel == null) {
					continue;
				}
				if (resLabel.equals(CommonConstants.DICOM_ORIG_RESOURCE)) {
					final Integer dicomOrigFileCount = resource.getFileCount();
					dicomOrigFileCountOk = (resource!=null && dicomOrigFileCount!=null && scanFrames != null) ? 
							((dicomOrigFileCount>=scanFrames || ScanUtils.isXa30Scan(scan))) : false;
					hasDicomOrig = true;
				} else if (resLabel.equals(CommonConstants.DICOM_RESOURCE)) {
					final Integer dicomFileCount = resource.getFileCount();
					dicomFileCountOk = (resource!=null && dicomFileCount!=null && scanFrames != null) ? 
							((dicomFileCount>=scanFrames || ScanUtils.isXa30Scan(scan))) : false;
					hasDicom = true;
				}
			}
			if (!hasDicom) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - Missing DICOM resource</li>");
				sessionOk = false;
			}
			if (!hasDicomOrig && !isUnusable) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - Missing DICOM_ORIG resource</li>");
				sessionOk = false;
			}
			if (!dicomFileCountOk && !ScanUtils.isXa30Scan(scan)) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - DICOM resource has fewer files than scan frames.</li>");
				sessionOk = false;
			}
			if (!dicomOrigFileCountOk && !isUnusable && !ScanUtils.isXa30Scan(scan)) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - DICOM_ORIG resource has fewer files than scan frames.</li>");
				sessionOk = false;
			}
		}
		if (sessionOk) {
			if (!statusEntity.getPrereqs()) {
				statusEntity.setPrereqs(true);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo("");
			} 
		} else {
			if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoSB.toString())) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo(infoSB.toString());
			} 
		}
	}
	
}
