package org.nrg.ccf.intradb.pcpcomponents.dcm2nii;

import java.util.Date;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ScanUtils;
import org.nrg.ccf.intradb.pcpcomponents.prepipeline.PrePipelineValidator;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

/**
 * The Class Dcm2NiiPrereqCheckerWithoutFaceMasking.
 */
@PipelinePrereqChecker
public class Dcm2NiiPrereqCheckerWithoutFaceMasking implements PipelinePrereqCheckerI {

	/* (non-Javadoc)
	 * @see org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI#checkPrereqs(org.nrg.ccf.pcp.entities.PcpStatusEntity, org.nrg.xft.security.UserI)
	 */
	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		boolean sessionOk = true;
		if (!PrePipelineValidator.prePipelineValidate(statusEntity.getProject(),session,user)) {
			sessionOk = false;
			infoSB.append("<li>PrePipeline appears not to have run successfully for this session.</li>");
		}
		for (XnatImagescandataI scan : session.getScans_scan()) {
			final String scanType = scan.getType();
			if (scanType == null || !scanType.matches("^[Tt][12][Ww].*$") || scanType.contains("setter")) {
				continue;
			}
			final String seriesDesc = scan.getSeriesDescription();
			final String scanId = scan.getId();
			boolean hasDicom = false;
			boolean dicomFileCountOk = false;
			Integer scanFrames = scan.getFrames();
			for (XnatAbstractresourceI resource : scan.getFile()) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				final String resLabel = resource.getLabel();
				if (resLabel == null) {
					continue;
				}
				if (resLabel.equals(CommonConstants.DICOM_RESOURCE)) {
					dicomFileCountOk = (resource!=null && resource.getFileCount()!=null && scanFrames != null) ? 
							(resource.getFileCount()>=scanFrames || ScanUtils.isXa30Scan(scan)) : false;
					hasDicom = true;
				}
			}
			if (!hasDicom) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - Missing DICOM resource</li>");
			}

			if (!dicomFileCountOk && !ScanUtils.isXa30Scan(scan)) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - DICOM resource has fewer files than scan frames.</li>");
			}
			if (!hasDicom || !dicomFileCountOk) {
				sessionOk = false;
			}
		}
		if (sessionOk) {
			if (!statusEntity.getPrereqs()) {
				statusEntity.setPrereqs(true);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo("");
			} 
		} else {
			if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoSB.toString())) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo(infoSB.toString());
			} 
		}
	}
	
}
