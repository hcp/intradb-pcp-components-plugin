package org.nrg.ccf.intradb.pcpcomponents.dcm2nii;

import java.util.Date;
import java.util.List;

import org.nrg.ccf.pcp.abst.AbstractStatusUpdater;
import org.nrg.ccf.pcp.anno.PipelineStatusUpdater;
import org.nrg.ccf.pcp.constants.PcpConstants.PcpStatus;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineStatusUpdaterI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PipelineStatusUpdater
public class Dcm2NiiStatusUpdater extends AbstractStatusUpdater implements PipelineStatusUpdaterI {
	
	final static Logger _logger = LoggerFactory.getLogger(Dcm2NiiStatusUpdater.class);
    final PcpStatusEntityService service = XDAT.getContextService().getBean(PcpStatusEntityService.class);

	@Override
	public void updateStatus(PcpStatusEntity statusEntity, UserI user) {
		super.updateStatus(statusEntity, user);
		final String status = statusEntity.getStatus();
		if (status.equals(PcpStatus.COMPLETE.toString()) || 
				status.equals(PcpStatus.EXT_COMPLETE.toString())) {
		    // Status is aready complete.  Don't need to do anything.
		    _logger.debug("DCM2NII_StatusUpdate - Status Already Complete - Returning");
		    return;
		}
		final CriteriaCollection cc = new CriteriaCollection("AND");
		cc.addClause("wrk:workFlowData.ID",statusEntity.getEntityId());
		cc.addClause("wrk:workFlowData.status","Complete");
		final CriteriaCollection subCC = new CriteriaCollection("OR");
		subCC.addClause("wrk:workFlowData.pipeline_name","Executed PCP automation script DicomToNifti2017");
		cc.addClause(subCC);
		_logger.debug(cc.toString());
		final List<WrkWorkflowdata> workflows = WrkWorkflowdata.getWrkWorkflowdatasByField(cc, user, false);
		_logger.debug("PCP DCM2NII status update script workflows found = " + workflows.size());
		if (workflows.size()>0){
		   for (final WrkWorkflowdata workflow : workflows) {
		       final Date workflowMod = workflow.getItem().getLastModified();
		       final Date submitted = statusEntity.getSubmitTime();
		       if (workflowMod.after(submitted)) {
		           statusEntity.setStatus(PcpStatus.COMPLETE);
		           statusEntity.setStatusTime(new Date());
		           if (service == null) {
		               _logger.error("ERROR:  Could not obtain status entity service");
		               continue;
		           }
		       }
		   }
		}
	}
	
}
