package org.nrg.ccf.intradb.pcpcomponents.dcm2nii;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceCheckUtils;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineValidator
public class Dcm2NiiValidator implements PipelineValidatorI, ConfigurableComponentI, Dcm2NiiValidatorI {

	protected static final String JSON_UPPERCASE_ENDING = ".JSON";
	protected static final String CONVERSION_SOFTWARE = "ConversionSoftware";
	protected static final String CONVERSION_SOFTWARE_VERSION = "ConversionSoftwareVersion";
	protected static final String CONVERSION_SOFTWARE_VALUE = "dcm2niix";
	protected static final Pattern PATTERN = Pattern.compile("20[12][0-9][01][0-9][0-3][0-9]");
	protected final String _className = this.getClass().getName().replace(".", "");
	protected Date softwareVersion;
	protected Integer[] dmriCount;
	protected Boolean softwareVersionInitialized = false;
	// Scans to be completely excluded from checks
	protected List<String> excludedSeriesDescs;
	protected List<String> excludedScanTypes;
    //
	protected List<String> skipScanBySeriesDesc;
	protected final Map<String, List<Integer>> _niftiCounts = new HashMap<>();
	private final DateFormat YYMMDD8_FORMAT = CommonConstants.getFormatYYMMDD8();

	public Dcm2NiiValidator() {
		super();
		// Let's stop checking scouts and localizers.  We don't use them and sometimes we 
		// can't get their counts to come out as expected.
		//_niftiCounts.put("AAHScout", Arrays.asList(new Integer[] { 2, 6, 8 }));
		//_niftiCounts.put("AAHScout", Arrays.asList(new Integer[] { 2, 6, 8 }));
		//_niftiCounts.put("Localizer", Arrays.asList(new Integer[] { 6 }));
		//_niftiCounts.put("Localizer_aligned", Arrays.asList(new Integer[] { 6, 8 }));
		_niftiCounts.put("FieldMap_SE_EPI", Arrays.asList(new Integer[] { 2 }));
		_niftiCounts.put("T1w", Arrays.asList(new Integer[] { 2 }));
		_niftiCounts.put("T1w_4e", Arrays.asList(new Integer[] { 8 }));
		_niftiCounts.put("T1w_Norm", Arrays.asList(new Integer[] { 2 }));
		_niftiCounts.put("T1w_Norm_4e", Arrays.asList(new Integer[] { 8 }));
		_niftiCounts.put("T1w_setter", Arrays.asList(new Integer[] { 2 }));
		_niftiCounts.put("T2w", Arrays.asList(new Integer[] { 2 }));
		_niftiCounts.put("T2w_Norm", Arrays.asList(new Integer[] { 2 }));
		_niftiCounts.put("T2w_setter", Arrays.asList(new Integer[] { 2 }));
		_niftiCounts.put("TSE_HiResHp", Arrays.asList(new Integer[] { 2 }));
		_niftiCounts.put("TSE_Norm_HiResHp", Arrays.asList(new Integer[] { 2 }));
		_niftiCounts.put("rfMRI", Arrays.asList(new Integer[] { 3 }));
		_niftiCounts.put("rfMRI_SBRef", Arrays.asList(new Integer[] { 2 }));
		_niftiCounts.put("tfMRI", Arrays.asList(new Integer[] { 3 }));
		_niftiCounts.put("tfMRI_SBRef", Arrays.asList(new Integer[] { 2 }));
		// This will now get set later based on configuration
		// _niftiCounts.put("dMRI",Arrays.asList(new Integer[] {4}));
		// 2023/01/02:  dMRI SBRefs now sometimes have bvec/bval files.  Okaying "4" as a valid count, in addition to "2"
		// Per MH, it's okay for SBRefs to have bvec/bval files.
		_niftiCounts.put("dMRI_SBRef", Arrays.asList(new Integer[] { 2,4 }));
		_niftiCounts.put("mbPCASLhr", Arrays.asList(new Integer[] { 2 }));
	}

	@Override
	public List<String> getConfigurationYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = "versionDate:\n" + "    id: " + _className + "-software-version\n" + "    name: "
				+ _className + "-software-version\n" + "    kind:  panel.input.text\n"
				+ "    label: Software Version Date\n" + "    placeholder:  YYYYMMDD\n"
				+ "    description:  Version date in YYYYMMDD Format.  Will require configured version or later. "
				+ " Enter 99999999 to skip check.  This is a resource intensive check, so it's best to skip the"
				+ " check, if possible, once all sessions have been run through with the desired version.\n"
				+ "dmriAlwaysHaveBvecBval:\n" + "    id: " + _className + "-have-bvec-bval\n" + "    name: "
				+ _className + "-have-bvec-bval\n" + "    kind:  panel.input.switchbox\n"
				+ "    label: DMRI Always have Bvec/Bval?\n" + "    value:  true\n" + "    onText:  Yes\n"
				+ "    offText:  No\n"
				+ "    description:  Does DTI always generate bvec/bval files?  If no, expected count will be adjusted.\n";

		returnList.add(ele);
		return returnList;
	}

	@Override
	public void validate(final PcpStatusEntity statusEntity, final UserI user) {
		doValidationReturnProblemScans(statusEntity, user, true);
	}

	@Override
	public List<XnatImagescandataI> doValidationReturnProblemScans(final PcpStatusEntity statusEntity, final UserI user,
			final Boolean updateStatusEntity) {
		final List<XnatImagescandataI> problemScans = new ArrayList<>();
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user,
				false);
		final StringBuilder infoSB = new StringBuilder();
		if (softwareVersion == null) {
			initializeSoftwareVersion(statusEntity);
		}
		if (dmriCount == null) {
			initializeDmriCount(statusEntity);
		}
		if (skipScanBySeriesDesc == null) {
			initializeScanCountValues(statusEntity);
		}
		boolean sessionOk = true;
		for (XnatImagescandataI scan : session.getScans_scan()) {
			final String scanType = scan.getType();
			final String lCaseScanType = scanType.toLowerCase();
			if (lCaseScanType.contains("physio") || lCaseScanType.contains("phoenixzip")
					|| lCaseScanType.contains("collection") || lCaseScanType.contains("posdisp:")
					|| (lCaseScanType.contains("&lt;") && lCaseScanType.contains("&gt;"))
					|| (lCaseScanType.contains("<") && lCaseScanType.contains(">"))) {
				continue;
			}
			final String scanQuality = scan.getQuality();
			if (scanQuality != null && scan.getQuality().equalsIgnoreCase("UNUSABLE")) {
				continue;
			}
			final String seriesDesc = scan.getSeriesDescription();
			if ((excludedScanTypes != null && excludedScanTypes.contains(scanType)) ||
					(excludedSeriesDescs != null && excludedSeriesDescs.contains(seriesDesc))) {
				continue;
			}
			final String scanId = scan.getId();
			boolean hasDicom = false;
			boolean hasSecondary = false;
			boolean hasNifti = false;
			boolean niftiVersionOk = false;
			boolean niftiFilesOk = true;
			boolean niftiCountOk = true;
			boolean skipSecondaryScan = false;
			// Let's disable this check for now and let sanity checks do it. It takes way
			// too long to run.
			// Date dicomDate = null;
			// Date niftiDate = null;
			for (XnatAbstractresourceI resource : scan.getFile()) {
				final XnatResource xnatResource;
				if (resource instanceof XnatResource) {
					xnatResource = (XnatResource) resource;
				} else {
					xnatResource = null;
					log.warn("WARNING:  Resource " + resource.getLabel()
							+ " is, unexpectedly, not an instance of XnatResource.");
				}
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				if (resource.getLabel().equals(CommonConstants.DICOM_RESOURCE)) {
					hasDicom = true;
					// Let's disable this check for now and let sanity checks do it. It takes way
					// too long to run.
					// dicomDate = getFileDate(xnatResource);
				} else if (resource.getLabel().equals(CommonConstants.NIFTI_RESOURCE)) {
					hasNifti = true;
					// Let's disable this check for now and let sanity checks do it. It takes way
					// too long to run.
					// niftiDate = getFileDate(xnatResource);
					niftiFilesOk = ResourceCheckUtils.checkNiftiFiles((XnatResourcecatalog) resource, infoSB, scanId,
							seriesDesc);
					niftiCountOk = niftiCountOk(session, scanType, xnatResource, scan.getSeriesDescription());
					if (!niftiFilesOk || !niftiCountOk) {
						sessionOk = false;
						if (!problemScans.contains(scan))
							problemScans.add(scan);
					}
					niftiVersionOk = checkDcm2niiVersion(xnatResource);
				} else if (resource.getLabel().equalsIgnoreCase(CommonConstants.SECONDARY_RESOURCE)) {
					hasSecondary = true;
					if (!hasNIFTIResource(scan)) {
						skipSecondaryScan = true;
						break;
					}
				}
			}
			if (skipSecondaryScan) {
				continue;
			}
			if ((!hasNifti && hasDicom) || (!hasNifti && !hasSecondary)) {
				sessionOk = false;
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - Missing NIFTI resource</li>");
				if (!problemScans.contains(scan))
					problemScans.add(scan);
			} else if (!niftiFilesOk) {
				sessionOk = false;
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - NIFTI resource missing files.</li>");
				if (!problemScans.contains(scan))
					problemScans.add(scan);
			} else if (!niftiCountOk) {
				sessionOk = false;
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc
						+ ") - NIFTI count doesn't match expected count for scan type.</li>");
				if (!problemScans.contains(scan))
					problemScans.add(scan);
				// } else if (niftiDate != null && dicomDate != null &&
				// dicomDate.after(niftiDate)) {
				// sessionOk = false;
				// infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - NIFTI resource
				// files appear to be older than DICOM resource files.</li>");
			} else if (!niftiVersionOk) {
				sessionOk = false;
				try {
					infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc
							+ ") - DCM2NIIX version info missing or output run with older version (CHECKED VERSION:  "
							+ YYMMDD8_FORMAT.format(softwareVersion) + ").</li>");
				} catch (Exception e) {
					infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc
							+ ") - DCM2NIIX version info missing or output run with older version (CHECKED VERSION:  "
							+ "(SOFTWARE_VERSION_PARSE_ERROR))</li>");

				}
				if (!problemScans.contains(scan))
					problemScans.add(scan);
			}
		}
		if (updateStatusEntity) {
			if (sessionOk) {
				if (!statusEntity.getValidated()) {
					statusEntity.setValidated(true);
					statusEntity.setValidatedTime(new Date());
					statusEntity.setValidatedInfo("");
				}
			} else {
				if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoSB.toString())) {
					statusEntity.setValidated(false);
					statusEntity.setValidatedTime(new Date());
					statusEntity.setValidatedInfo(infoSB.toString());
				}
			}
		}
		return problemScans;
	}

	/**
	 * @param statusEntity
	 */
	protected void initializeScanCountValues(PcpStatusEntity statusEntity) {
	}

	/**
	 * @param scan
	 */
	private boolean hasNIFTIResource(XnatImagescandataI scan) {
		for (XnatAbstractresourceI resource : scan.getFile()) {
			if (resource.getLabel().equals(CommonConstants.NIFTI_RESOURCE)) {
				return true;
			}
		}
		return false;
	}

	private void initializeDmriCount(PcpStatusEntity entity) {
		if (dmriCount == null) {
			try {
				final String configValue = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(),
						this.getClass().getName(), "-have-bvec-bval", false).toString();
				dmriCount = (configValue == null || configValue.toLowerCase().contains("true")) ? new Integer[] { 4 }
						: new Integer[] { 2, 4 };
			} catch (Exception e) {
				dmriCount = new Integer[] { 4 };
			}
			_niftiCounts.put("dMRI", Arrays.asList(dmriCount));
		}
	}

	private void initializeSoftwareVersion(PcpStatusEntity entity) {
		if (softwareVersionInitialized) {
			return;
		}
		if (softwareVersion == null) {
			try {
				final String configValue = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(),
						this.getClass().getName(), "-software-version", false).toString();
				softwareVersion = (configValue != null && !configValue.equals("99999999"))
						? YYMMDD8_FORMAT.parse(configValue)
						: null;
				softwareVersionInitialized = true;
			} catch (Exception e) {
				softwareVersion = null;
			}
			if (softwareVersion != null) {
				log.debug("Dcm2NiiValidator - softwareVersionRequirement - " + YYMMDD8_FORMAT.format(softwareVersion));
			} else {
				log.debug("Dcm2NiiValidator - NULL VALUE FOR softwareVersionRequirement");
			}
		}
	}

	@SuppressWarnings("unused")
	private Date getFileDate(XnatResource resource) {

		if (resource == null) {
			return null;
		}
		Date fileDate = null;
		// This takes a while, so let's only look at a few files.
		int counter = 1;
		for (final File file : resource.getCorrespondingFiles(CommonConstants.ROOT_PATH)) {
			// final File file = rFile.getF();
			final Date thisDate = new Date(file.lastModified());
			if (fileDate == null || fileDate.before(thisDate)) {
				fileDate = thisDate;
			}
			if (counter > 3) {
				break;
			}
			counter += 1;
		}
		return fileDate;
	}

	protected boolean niftiCountOk(XnatMrsessiondata session, String scanType, XnatResource resource,
			String seriesDescription) {

		if (resource == null || !_niftiCounts.containsKey(scanType)) {
			return true;
		}
		final List<Integer> expectedCounts = _niftiCounts.get(scanType);
		final Integer fileCount = resource.getCorrespondingFiles(CommonConstants.ROOT_PATH).size();
		return expectedCounts.contains(fileCount);

	}

	private boolean checkDcm2niiVersion(final XnatResource resource) {

		if (softwareVersion == null || resource == null) {
			return true;
		}
		for (final File file : resource.getCorrespondingFiles(CommonConstants.ROOT_PATH)) {
			if (!file.getName().toUpperCase().endsWith(JSON_UPPERCASE_ENDING)) {
				continue;
			}
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				FileUtils.copyFile(file, baos);
				final String fileString = baos.toString(StandardCharsets.UTF_8.name());
				baos.close();
				final JsonElement jsonEle;
				try {
					jsonEle = new JsonParser().parse(fileString);
					if (jsonEle.isJsonObject()) {
						final JsonObject jsonObj = jsonEle.getAsJsonObject();
						return handleJsonObject(jsonObj);
					} else if (jsonEle.isJsonArray()) {
						final JsonArray jsonArr = jsonEle.getAsJsonArray();
						return handleJsonArray(jsonArr);
					}
				} catch (Exception e) {
					log.error("JSON Parsing Error (FILE=" + file.getName() + "): \n" + fileString);
				}
			} catch (IOException e1) {
				log.error("JSON Parsing Error (IOException) (FILE=" + file.getName() + ")");
			}
		}
		return false;

	}

	private boolean handleJsonObject(final JsonObject jsonObj) {
		try {
			if (jsonObj.has(CONVERSION_SOFTWARE)) {
				if (!jsonObj.get(CONVERSION_SOFTWARE).getAsString().equals(CONVERSION_SOFTWARE_VALUE)) {
					return false;
				}
				if (jsonObj.has(CONVERSION_SOFTWARE_VERSION)) {
					final JsonElement versionEle = jsonObj.get(CONVERSION_SOFTWARE_VERSION);
					final String version = versionEle.getAsString();
					final Matcher m = PATTERN.matcher(version);
					if (m.find()) {
						final String fileVersionDate = m.group(0);
						if (YYMMDD8_FORMAT.parse(fileVersionDate).compareTo(softwareVersion) >= 0) {
							return true;
						}

					}
				}
			}
		} catch (Exception e) {
			log.error("ERROR retrieving dcm2niix version date from JSON sidecar file:  ", e);
		}
		return false;
	}

	private boolean handleJsonArray(final JsonArray jsonArr) {
		for (final JsonElement innerEle : jsonArr) {
			if (innerEle.isJsonArray()) {
				final JsonArray innerArr = innerEle.getAsJsonArray();
				handleJsonArray(innerArr);
			} else if (innerEle.isJsonObject()) {
				final JsonObject innerObj = innerEle.getAsJsonObject();
				boolean rc = handleJsonObject(innerObj);
				if (rc) {
					return true;
				}
			}
		}
		return false;
	}

}
