package org.nrg.ccf.intradb.pcpcomponents.dcm2nii;

import java.util.List;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xft.security.UserI;

public interface Dcm2NiiValidatorI extends PipelineValidatorI {

	List<XnatImagescandataI> doValidationReturnProblemScans(PcpStatusEntity statusEntity, UserI user,
			Boolean updateStatusEntity);

}
