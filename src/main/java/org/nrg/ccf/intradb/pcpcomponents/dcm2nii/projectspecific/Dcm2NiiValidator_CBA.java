package org.nrg.ccf.intradb.pcpcomponents.dcm2nii.projectspecific;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.intradb.pcpcomponents.dcm2nii.Dcm2NiiCustomValidator;
import org.nrg.ccf.intradb.pcpcomponents.dcm2nii.Dcm2NiiValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResource;

@PipelineValidator
public class Dcm2NiiValidator_CBA extends Dcm2NiiCustomValidator {
	
	
	
	public Dcm2NiiValidator_CBA() {
		super();
		if (excludedScanTypes == null) {
			excludedScanTypes = new ArrayList<>();
		}
		if (excludedSeriesDescs == null) {
			excludedSeriesDescs = new ArrayList<>();
		}
		excludedScanTypes.add("Localizer");
		excludedScanTypes.add("AAHScout");
	}

	@Override
	protected boolean niftiCountOk(XnatMrsessiondata session, String scanType, XnatResource resource, String seriesDescription) {
		
		if (resource == null || !_niftiCounts.containsKey(scanType)) {
			return true;
		}
		List<Integer> count=null;
		if(scansToCheck.contains(scanType) && skipScanBySeriesDesc.contains(seriesDescription))
		{
			count=Arrays.asList(new Integer[] {_niftiCounts.get(scanType).get(0)-1});
		}
		else
		{
			count=_niftiCounts.get(scanType);
		}
		List<Integer> expectedCounts = count;
		final Integer fileCount = resource.getCorrespondingFiles(CommonConstants.ROOT_PATH).size();
		// 
		if (seriesDescription.contains("Magnitude") || seriesDescription.toLowerCase().contains("fieldmap")) {
			expectedCounts = new ArrayList<Integer>(Arrays.asList(2,4));
		}
		return expectedCounts.contains(fileCount);

	}

}
