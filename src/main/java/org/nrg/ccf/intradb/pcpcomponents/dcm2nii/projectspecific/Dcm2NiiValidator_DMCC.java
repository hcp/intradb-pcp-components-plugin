package org.nrg.ccf.intradb.pcpcomponents.dcm2nii.projectspecific;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.intradb.pcpcomponents.dcm2nii.Dcm2NiiValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResource;

@PipelineValidator
public class Dcm2NiiValidator_DMCC extends Dcm2NiiValidator {

	final private String[] _fmriArr = new String[] { "tfMRI", "rfMRI" };

	public Dcm2NiiValidator_DMCC() {
		super();
		if (excludedScanTypes == null) {
			excludedScanTypes = new ArrayList<>();
		}
		if (excludedSeriesDescs == null) {
			excludedSeriesDescs = new ArrayList<>();
		}
		excludedScanTypes.add("Localizer");
		excludedScanTypes.add("AAHScout");
		excludedSeriesDescs.add("PhoenixZIPReport");
	}

	@Override
	protected boolean niftiCountOk(XnatMrsessiondata session, String scanType, XnatResource resource,
			String seriesDescription) {

		if (resource == null || !_niftiCounts.containsKey(scanType)) {
			return true;
		}
		final List<Integer> expectedCounts = _niftiCounts.get(scanType);
		final Integer fileCount = resource.getCorrespondingFiles(CommonConstants.ROOT_PATH).size();
		// For DMCC, not skipping frames
		if (Arrays.asList(_fmriArr).contains(scanType)) {
			return (fileCount.equals(2));
		}
		return expectedCounts.contains(fileCount);

	}

}
