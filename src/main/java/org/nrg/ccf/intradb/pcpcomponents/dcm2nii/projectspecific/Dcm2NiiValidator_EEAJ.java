package org.nrg.ccf.intradb.pcpcomponents.dcm2nii.projectspecific;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.intradb.pcpcomponents.dcm2nii.Dcm2NiiValidatorAllImaging;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatResource;

@PipelineValidator
public class Dcm2NiiValidator_EEAJ extends Dcm2NiiValidatorAllImaging {
	
	//final private String[] _fmriArr = new String[] { "tfMRI", "rfMRI" };

	@Override
	protected boolean niftiCountOk(XnatImagesessiondata session, String scanType, XnatResource resource, String seriesDescription) {
		
		if (resource == null || !_niftiCounts.containsKey(scanType)) {
			return true;
		}
		//final List<Integer> expectedCounts = _niftiCounts.get(scanType);
		final Integer fileCount = resource.getCorrespondingFiles(CommonConstants.ROOT_PATH).size();
		// For now, let's just make sure we have files.
		return (fileCount != null && fileCount >= 2);

	}

}
