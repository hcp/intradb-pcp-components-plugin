package org.nrg.ccf.intradb.pcpcomponents.dcm2nii.projectspecific;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.intradb.pcpcomponents.dcm2nii.Dcm2NiiValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResource;

@PipelineValidator
public class Dcm2NiiValidator_HCLV extends Dcm2NiiValidator {
	
	//final private String[] _fmriArr = new String[] { "tfMRI", "rfMRI" };

	@Override
	protected boolean niftiCountOk(XnatMrsessiondata session, String scanType, XnatResource resource, String seriesDescription) {
		
		// TODO:  Need to modify this based on final initial frams decisions.
		if (resource == null || !_niftiCounts.containsKey(scanType)) {
			return true;
		}
		//final List<Integer> expectedCounts = _niftiCounts.get(scanType);
		final Integer fileCount = resource.getCorrespondingFiles(CommonConstants.ROOT_PATH).size();
		// For now, let's just make sure we have files.
		return (fileCount != null && fileCount >= 2);

	}

}
