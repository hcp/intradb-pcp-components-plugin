package org.nrg.ccf.intradb.pcpcomponents.dcm2niicleanup;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.intradb.pcpcomponents.dcm2nii.Dcm2NiiValidatorI;
import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.constants.PcpConstants.PcpStatus;
import org.nrg.ccf.pcp.dto.ComponentSet;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.constants.Scope;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.springframework.util.FileSystemUtils;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineSubmitter
public class Dcm2NiiCleanupSubmitter implements PipelineSubmitterI {
	
	private final ConfigService _configService = XDAT.getConfigService();
	private final PcpUtils _pcpUtils = XDAT.getContextService().getBean(PcpUtils.class);
	private final PcpStatusEntityService _statusEntityService = XDAT.getContextService().getBean(PcpStatusEntityService.class);
	private final Gson _gson = new Gson();
	public static final Map<String,PcpConfigInfo> DCM2NII_PIPELINE_MAP = new HashMap<>();
	private static final List<String> DCM2NII_PACKAGES = 
			Arrays.asList(new String[] { "org.nrg.ccf.intradb.pcpcomponents.dcm2nii" });

	@Override
	public List<String> getParametersYaml(String projectId, String pipelineId) {
		final PcpConfigInfo configInfo = getDcm2NiiPipelineInfo(projectId);
		ComponentSet dcm2niiCS;
		try {
			dcm2niiCS = _pcpUtils.getComponentSet(projectId, configInfo.getPipeline());
			return dcm2niiCS.getSubmitter().getParametersYaml(projectId, configInfo.getPipeline());
		} catch (PcpComponentSetException e) {
			return ImmutableList.of();
		}
	}
	
	@Override
	public boolean submitJob(PcpStatusEntity entity, PcpStatusEntityService entityService, PipelineValidatorI validator,
			Map<String, String> parameters, UserI user) throws PcpSubmitException {
		final PcpConfigInfo configInfo = getDcm2NiiPipelineInfo(entity.getProject());
		ComponentSet dcm2niiCS;
		try {
			dcm2niiCS = _pcpUtils.getComponentSet(entity.getProject(), configInfo.getPipeline());
		} catch (PcpComponentSetException e) {
			dcm2niiCS = null;
		}
		if (configInfo == null || dcm2niiCS ==null || dcm2niiCS.getValidator() == null || 
				!(dcm2niiCS.getValidator() instanceof Dcm2NiiValidatorI)) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("DCM2NII pipeline is not configured correctly for this project.");
			entity.setStatusTime(new Date());
			return false;
		}
		final PcpStatusEntity dcm2niiEntity = _statusEntityService.getStatusEntity(entity.getProject(), configInfo.getPipeline(), entity.getExecutionGroup());
		if (dcm2niiEntity == null) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("Could not find matching DCM2NII entity.");
			entity.setStatusTime(new Date());
			return false;
		}
		final Dcm2NiiValidatorI dcm2niiValidator = (Dcm2NiiValidatorI)dcm2niiCS.getValidator();
		final PipelineSubmitterI dcm2niiSubmitter = dcm2niiCS.getSubmitter();
		final List<XnatImagescandataI> scanList = dcm2niiValidator.doValidationReturnProblemScans(dcm2niiEntity, user, false);
		if (scanList.size()==0) {
			entity.setStatus(PcpStatus.COMPLETE);
			entity.setStatusInfo("No scans failed DCM2NII validation.  Nothing to do.");
			entity.setStatusTime(new Date());
			return true;
		}
		StringBuilder sb = new StringBuilder();
		final Iterator<XnatImagescandataI> i = scanList.iterator();
		while (i.hasNext()) {
			final XnatImagescandataI scan = i.next();
			cleanupNiftiResources(scan);
			sb.append(scan.getId());
			if (i.hasNext()) {
				sb.append(",");
			}
		}
		parameters.put("scans", sb.toString());
		boolean rtnVal;
		try { 
			rtnVal = dcm2niiSubmitter.submitJob(dcm2niiEntity, entityService, dcm2niiValidator, parameters, user);
		} catch (Exception e) {
			log.error("Exception thrown processing  entity (" + entity + "):" + ExceptionUtils.getStackTrace(e));
			rtnVal = false;
		}
		entityService.refresh(dcm2niiEntity);
		dcm2niiValidator.validate(dcm2niiEntity, user);
		entityService.update(dcm2niiEntity);
		if (rtnVal) {
			entity.setStatus(PcpConstants.PcpStatus.COMPLETE);
			entity.setStatusInfo("DCM2NII processing completed.");
		} else {
			entity.setStatus(PcpConstants.PcpStatus.ERROR);
			entity.setStatusInfo("DCM2NII did not complete successfully.");
		}
		entity.setStatusTime(new Date());
		entityService.update(entity);
		return rtnVal;
	}
	
	private void cleanupNiftiResources(XnatImagescandataI scan) {
		
		final XnatResourcecatalog resource = ResourceUtils.getResource(scan,  CommonConstants.NIFTI_RESOURCE);
		if (resource != null) {
			resource.removeResource();
		}
		// There should be a DICOM resource adjacent to any NIFTI resource.  Let's only delete if we have expected directories.
		final XnatResourcecatalog dicomResource = ResourceUtils.getResource(scan,  CommonConstants.DICOM_RESOURCE);
		final File dicomFile = dicomResource.getCatalogFile(CommonConstants.ROOT_PATH);
		if (dicomFile == null) {
			return;
		}
		final File niftiDir = new File(dicomFile.getParentFile().getParentFile(),CommonConstants.NIFTI_RESOURCE);
		if (niftiDir != null && niftiDir.exists() && niftiDir.isDirectory()) {
			FileSystemUtils.deleteRecursively(niftiDir);
		}
		
	}

	private PcpConfigInfo getDcm2NiiPipelineInfo(final String project) {
		if (!DCM2NII_PIPELINE_MAP.containsKey(project)) {
			final Configuration conf = _configService.getConfig(PcpConfigConstants.CONFIG_TOOL, PcpConfigConstants.CONFIG_PATH, Scope.Project, project);
			final List<PcpConfigInfo> projectConfigs;
			if (conf != null) {
				projectConfigs = _gson.fromJson(conf.getContents(),new TypeToken<List<PcpConfigInfo>>(){}.getType());
			} else {
				projectConfigs = new ArrayList<>();
			}
			outerLoop:
			for (final PcpConfigInfo projectConfig : projectConfigs) {
				for (final String dcm2niiPackage : DCM2NII_PACKAGES) {
					if (projectConfig.getValidator().startsWith(dcm2niiPackage + ".")) {
						DCM2NII_PIPELINE_MAP.put(project, projectConfig);
						break outerLoop;
					}
				}
			}
		}
		return DCM2NII_PIPELINE_MAP.get(project);
	}

}
