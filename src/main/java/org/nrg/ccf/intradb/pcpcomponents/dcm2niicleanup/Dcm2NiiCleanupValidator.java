package org.nrg.ccf.intradb.pcpcomponents.dcm2niicleanup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.constants.Scope;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@PipelineValidator
public class Dcm2NiiCleanupValidator implements PipelineValidatorI {
	
	private final ConfigService _configService = XDAT.getConfigService();
	private final PcpStatusEntityService _statusEntityService = XDAT.getContextService().getBean(PcpStatusEntityService.class);
	private final Gson _gson = new Gson();
	public static final Map<String,String> DCM2NII_PIPELINE_MAP = new HashMap<>();
	private static final List<String> DCM2NII_PACKAGES = Arrays.asList(new String[] { "org.nrg.ccf.intradb.pcpcomponents.dcm2nii" });
	
	@Override
	public void validate(PcpStatusEntity entity, UserI user) {
		// NOTE:  We can't afford to recalculate DCM2NII validation, so we'll just use the validation from the DCM2NII pipeline
		final String dcmToNiiPipeline = getDcm2NiiPipeline(entity.getProject());
		if (dcmToNiiPipeline == null || dcmToNiiPipeline.isEmpty()) {
			if (entity.getValidated() || entity.getValidatedInfo().isEmpty()) {
				entity.setValidated(false);
				entity.setValidatedInfo("DCM2NII pipeline is not configured for this project.  Status is determined from DCM2NII pipeline.");
				entity.setValidatedTime(new Date());
			}
			return;
		}
		final PcpStatusEntity dcm2niiEntity = _statusEntityService.getStatusEntity(entity.getProject(), dcmToNiiPipeline, entity.getExecutionGroup()); 
		if (dcm2niiEntity == null) {
			entity.setPrereqs(false);
			entity.setPrereqsInfo("DCM2NII entity row does not yet exist (update cache for DCM2NII pipeline)\n");
			entity.setPrereqsTime(new Date());
			entity.setValidated(false);
			entity.setValidatedInfo("DCM2NII entity row does not yet exist (update cache for DCM2NII pipeline)\n");
			entity.setValidatedTime(new Date());
			return;
		}
		if (dcm2niiEntity.getValidatedInfo() == null) {
			dcm2niiEntity.setValidatedInfo("");
		}
		if (dcm2niiEntity == null || dcm2niiEntity.getValidated() == null) {
			entity.setValidated(false);
			entity.setValidatedInfo("Could not get validation status from DCM2NII pipeline entity\n\n" + dcm2niiEntity.getValidatedInfo());
			entity.setValidatedTime(new Date());
		}
		if ((entity.getValidated() == null || !entity.getValidated()) && dcm2niiEntity.getValidated()) {
			entity.setValidated(true);
			entity.setValidatedInfo("DCM2NII pipeline reports successful validation");
			entity.setValidatedTime(new Date());
			
		} else if ((entity.getValidated() || entity.getValidatedInfo().isEmpty()) && !dcm2niiEntity.getValidated() || (entity.getValidatedInfo() == null || 
				!entity.getValidatedInfo().equals(dcm2niiEntity.getValidatedInfo()))) {
			entity.setValidated(false);
			entity.setValidatedInfo("DCM2NII pipeline reports validation failed:\n\n" + dcm2niiEntity.getValidatedInfo());
			entity.setValidatedTime(new Date());
		}
	}
	
	private String getDcm2NiiPipeline(final String project) {
		if (!DCM2NII_PIPELINE_MAP.containsKey(project)) {
			final Configuration conf = _configService.getConfig(PcpConfigConstants.CONFIG_TOOL, PcpConfigConstants.CONFIG_PATH, Scope.Project, project);
			final List<PcpConfigInfo> projectConfigs;
			if (conf != null) {
				projectConfigs = _gson.fromJson(conf.getContents(),new TypeToken<List<PcpConfigInfo>>(){}.getType());
			} else {
				projectConfigs = new ArrayList<>();
			}
			outerLoop:
			for (final PcpConfigInfo projectConfig : projectConfigs) {
				for (final String dcm2niiPackage : DCM2NII_PACKAGES) {
					if (projectConfig.getValidator().startsWith(dcm2niiPackage + ".")) {
						DCM2NII_PIPELINE_MAP.put(project, projectConfig.getPipeline());
						break outerLoop;
					}
				}
			}
		}
		return DCM2NII_PIPELINE_MAP.get(project);
	}

}
