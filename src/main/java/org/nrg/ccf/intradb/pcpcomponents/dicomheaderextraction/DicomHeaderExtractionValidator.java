package org.nrg.ccf.intradb.pcpcomponents.dicomheaderextraction;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.model.XnatAddfieldI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatMrscandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

@PipelineValidator
public class DicomHeaderExtractionValidator implements PipelineValidatorI {

	private static String[] CHECKED_FIELDS = { "Image Comments", "Station Name", "Institution Name", "Device Serial Number" };

	@Override
	public void validate(final PcpStatusEntity statusEntity, final UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		boolean sessionOk = doValidation(session, infoSB);
		if (sessionOk) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo("");
			} 
		} else {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoSB.toString())) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(infoSB.toString());
			} 
		}
	}

	public static boolean doValidation(XnatMrsessiondata session, StringBuilder infoSB) {
		// Let's just check to make sure that one or more of the scans has some of the newly extracted fields
		boolean sessionOk = true;
		for (final XnatImagescandataI scan : session.getScans_scan()) {
			final String lcScanType = scan.getType().toLowerCase();
			if (!(lcScanType.contains("rfmri") || lcScanType.contains("tfmri") || lcScanType.contains("t1w") ||
					lcScanType.contains("t2w") || lcScanType.contains("dmri"))) { 
				continue;
			}
			if (lcScanType.contains("physio") || lcScanType.contains("sbref") || lcScanType.contains("setter")) {
				continue;
			}
			if (!(scan instanceof XnatMrscandataI)) {
				continue;
			}
			final XnatMrscandataI mrScan = (XnatMrscandataI) scan;
			final List<XnatAddfieldI> addParams = mrScan.getParameters_addparam();
			int countMatches = 0;
			for (final XnatAddfieldI addField : addParams) {
				final String addFieldName = addField.getName();
				if (Arrays.asList(CHECKED_FIELDS).contains(addFieldName)) {
					countMatches++;
				}
			}
			if (countMatches==0) {
				infoSB.append("<li>SCAN " + scan.getId() + " (" + scan.getSeriesDescription() + ") - Doesn't contain any of the " + 
						"expected recently added extraction fields " + Arrays.toString(CHECKED_FIELDS) + ".</li>");
				sessionOk = false;
			}
		}
		return sessionOk;
	}

}
