package org.nrg.ccf.intradb.pcpcomponents.facemasking;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.common.utilities.utils.ScanUtils;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

@PipelineValidator
public class FacemaskingValidatorNoQcCheck implements PipelineValidatorI {

	@Override
	public void validate(final PcpStatusEntity statusEntity, final UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		boolean sessionOk = true;
		for (XnatImagescandataI scan : session.getScans_scan()) {
			final String scanType = scan.getType();
			if (!scanType.matches("^[Tt][12][Ww].*$") || scanType.contains("setter")) {
				continue;
			}
			final String seriesDesc = scan.getSeriesDescription();
			final String scanId = scan.getId();
			boolean hasDicomOrig = false;
			boolean dicomOrigFileCountOk = false;
			Integer dicomOrigFileCount = 0;
			boolean hasDicom = false;
			boolean dicomFileCountOk = false;
			final boolean isUnusable = (scan.getQuality() != null) ? scan.getQuality().equalsIgnoreCase("unusable") : false;
			Integer dicomFileCount = 0;
			Integer scanFrames = scan.getFrames();
			for (XnatAbstractresourceI resource : scan.getFile()) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				final String resLabel = resource.getLabel();
				// Occasionally REST puts resources back without labels
				if (resLabel == null) {
					continue;
				}
				if (!isUnusable && resLabel.equals(CommonConstants.DICOM_ORIG_RESOURCE)) {
					dicomOrigFileCount = (resource.getFileCount() != null) ? resource.getFileCount() : 0;
					dicomOrigFileCountOk = (dicomOrigFileCount!=null && scanFrames != null &&
							(dicomOrigFileCount>=scanFrames || ScanUtils.isXa30Scan(scan)));
					hasDicomOrig = true;
				} else if (resLabel.equals(CommonConstants.DICOM_RESOURCE)) {
					dicomFileCount = (resource.getFileCount() != null) ? resource.getFileCount() : 0;
					dicomFileCountOk = (dicomFileCount!=null && scanFrames != null &&
							(dicomFileCount>=scanFrames || ScanUtils.isXa30Scan(scan)));
					hasDicom = true;
					// Checked that DICOM resource contains files that appear to be facemasked files
					final List<File> dicomFiles = ResourceUtils.getFiles(resource);
					for (final File dicomFile : dicomFiles) {
						if (!dicomFile.getName().matches("^[0-9][0-9][0-9]_.*$")) {
							infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - " +
							"DICOM resource contains files that appear not to be facemasked DICOM (by filename)"
									+ "</li>");
							sessionOk = false;
							break;
						}
					}
				}
				// Make sure we have a DICOM Snapshots resource
			}
			if (!hasDicomOrig && !isUnusable) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - Missing DICOM_ORIG resource</li>");
				sessionOk = false;
			}
			if (!hasDicom) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - Missing DICOM resource</li>");
				sessionOk = false;
			}
			if (!dicomOrigFileCountOk && !isUnusable && !ScanUtils.isXa30Scan(scan)) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - DICOM_ORIG resource has fewer files than scan frames.</li>");
				sessionOk = false;
			}
			if (!dicomFileCountOk && !ScanUtils.isXa30Scan(scan)) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - DICOM resource has fewer files than scan frames.</li>");
				sessionOk = false;
			}
			if ((hasDicomOrig && hasDicom) && !dicomOrigFileCount.equals(dicomFileCount) && !isUnusable) {
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - DICOM resource and DICOM_ORIG resource have non-equal file count.</li>");
				sessionOk = false;
			}
		}
		if (sessionOk) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo("");
			} 
		} else {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoSB.toString())) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(infoSB.toString());
			} 
		}
	}

}
