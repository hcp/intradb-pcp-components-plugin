package org.nrg.ccf.intradb.pcpcomponents.level2qc;

import java.util.Date;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceCheckUtils;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class Level2QcPrereqChecker implements PipelinePrereqCheckerI {

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		boolean sessionOk = true;
		for (XnatImagescandataI scan : session.getScans_scan()) {
			final String scanType = scan.getType();
			if (scanType.toLowerCase().contains("physio") || 
				!scanType.toLowerCase().contains("fmri") || 
				scanType.toLowerCase().contains("phoenixzip") ||
				scanType.toLowerCase().contains("sbref")) {
				continue;
			}
			// Let's not consider unusable scans
			if (scan.getQuality().equalsIgnoreCase("unusable")) {
				continue;
			}
			final String seriesDesc = scan.getSeriesDescription();
			final String scanId = scan.getId();
			boolean hasDicom = false;
			boolean hasSecondary = false;
			boolean hasNifti = false;
			boolean niftiFilesOk = true;
			for (XnatAbstractresourceI resource : scan.getFile()) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				if (resource.getLabel().equals(CommonConstants.DICOM_RESOURCE)) {
					hasDicom = true;
				} else if (resource.getLabel().equals(CommonConstants.NIFTI_RESOURCE)) {
					hasNifti = true;
					niftiFilesOk = ResourceCheckUtils.checkNiftiFiles((XnatResourcecatalog)resource, infoSB, scanId, seriesDesc);
					if (!niftiFilesOk) {
						sessionOk = false;
					}
					
				} else if (resource.getLabel().equalsIgnoreCase(CommonConstants.SECONDARY_RESOURCE)) {
					hasSecondary = true;
				}
			}
			if ((!hasNifti && hasDicom) || (!hasNifti && !hasSecondary)) {
				sessionOk = false;
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - Missing NIFTI resource</li>");
			} else if (!niftiFilesOk) {
				sessionOk = false;
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - NIFTI resource missing files.</li>");
			}
		}
		if (sessionOk) {
			if (!statusEntity.getPrereqs()) {
				statusEntity.setPrereqs(true);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo("");
			} 
		} else {
			if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoSB.toString())) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo(infoSB.toString());
			} 
		}
		
	}

}
