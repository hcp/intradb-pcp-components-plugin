package org.nrg.ccf.intradb.pcpcomponents.level2qc;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatAbstractstatisticsI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatQcassessmentdataScanI;
import org.nrg.xdat.model.XnatStatisticsdataAdditionalstatisticsI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatQcassessmentdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.XnatStatisticsdata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;

@PipelineValidator
public class Level2QcValidator implements PipelineValidatorI {
	
	private final DateFormat YYMMDD8_FORMAT = CommonConstants.getFormatYYMMDD8();

	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		boolean sessionOk = true;
		List<String> qcList = new ArrayList<>();
		final org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
	      	cc.addClause("xnat:qcAssessmentData.imageSession_ID",statusEntity.getEntityId());
	      	cc.addClause("xnat:qcAssessmentData.project",statusEntity.getProject());
		final List<XnatQcassessmentdata> qcdatas = XnatQcassessmentdata.getXnatQcassessmentdatasByField(cc, user, false);
		final List<XnatImagescandataI> scanList = session.getScans_scan();
		for (final XnatQcassessmentdata qcdata : qcdatas) {
			List<XnatQcassessmentdataScanI> qcscans = qcdata.getScans_scan();
			for (final XnatQcassessmentdataScanI qcscan : qcscans) {
				final String qcscanid = qcscan.getId();
				if (!qcList.contains(qcscanid)) {
					qcList.add(qcscanid);
				}
				final XnatAbstractstatisticsI scanstats = qcscan.getScanstatistics();
				if (scanstats instanceof XnatStatisticsdata) {
					XnatStatisticsdata stats = (XnatStatisticsdata)scanstats;
					for (final XnatStatisticsdataAdditionalstatisticsI addStat : stats.getAdditionalstatistics()) {
						if (addStat.getAdditionalstatistics() == null &&
								!getScanQuality(scanList, qcscanid).equalsIgnoreCase("unusable")) {
							infoSB.append("<li>SCAN " + qcscanid + " - QC assessment has one or more missing statistics values.");
							sessionOk = false;
							break;
						}
					}
				}
			}
		}
		for (final XnatImagescandataI scan : scanList) {
			final String sd = (scan.getSeriesDescription() != null ) ? scan.getSeriesDescription().toLowerCase() : "";
			final String st = scan.getType().toLowerCase();
			if (sd.contains("physiolog") || sd.contains("sbref")) {
				continue;
			}
			// Let's try using contains in case scan type has not yet been set.  These were showing 
			// up as validated.
			if ((st.contains("tfmri") || st.contains("rfmri")) && 
					!scan.getQuality().equalsIgnoreCase("unusable")) {
				if (!qcList.contains(scan.getId())) {
					infoSB.append("<li>SCAN " + scan.getId() + " (" + scan.getSeriesDescription() + ") - No QC Assessment");
					sessionOk = false;
				} else {
					final Date niftiDate = getNiftiDate(scan);
					final Date qcDate = getQcDate(qcdatas,scan);
					if (niftiDate != null && qcDate != null && niftiDate.after(qcDate)) {
						infoSB.append("<li>SCAN " + scan.getId() + " (" + scan.getSeriesDescription() + ") - QC assessment is older than NIFTI.");
						sessionOk = false;
					}
				}
			}
		}
		if (sessionOk) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setIssues(false);
				statusEntity.setIssuesInfo("");
				statusEntity.setIssuesTime(new Date());
			}
		} else {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoSB.toString())) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(infoSB.toString());
			}
		}
	}

	private String getScanQuality(List<XnatImagescandataI> scanList, String qcscanid) {
		for (XnatImagescandataI scan : scanList) {
			if (scan.getId().equalsIgnoreCase(qcscanid) && scan.getQuality() != null) {
				return scan.getQuality();
			}
		}
		return "";
	}

	private Date getQcDate(List<XnatQcassessmentdata> qcdatas, XnatImagescandataI scan) {
		Date qcDate = null;
		for (final XnatQcassessmentdata qcdata : qcdatas) {
			if (qcdata.getDate() == null || (!(qcdata.getDate() instanceof Date))) {
				continue;
			}
			if (qcDate == null || ((Date)qcdata.getDate()).before(qcDate)) {
				qcDate = (Date)qcdata.getDate();
			}
			// Let's use insert date for more accurate date, unless the date value doesn't match (re-upload of old qcdata).
			final Date insertDate = qcdata.getInsertDate();
			if (YYMMDD8_FORMAT.format(qcDate).equals(YYMMDD8_FORMAT.format(insertDate))) {
				qcDate = insertDate;
				
			}
			List<XnatQcassessmentdataScanI> qcscans = qcdata.getScans_scan();
			for (final XnatQcassessmentdataScanI qcscan : qcscans) {
				final String qcscanid = qcscan.getId();
				if (qcscanid.equals(scan.getId())) {
					return qcDate;
					
				}
			}
		}
		return null;
	}

	private Date getNiftiDate(XnatImagescandataI scan) {
		
		Date returnDate = null;
		for (XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (!resource.getLabel().equals(CommonConstants.NIFTI_RESOURCE)) {
				continue;
			}
			final XnatResourcecatalog catalog = (XnatResourcecatalog)resource;
			for (final ResourceFile cFile : catalog.getFileResources(CommonConstants.ROOT_PATH)) {
				File f = cFile.getF();
				if (f.getName().toLowerCase().contains(".nii")) {
					final Date fDate = new Date(f.lastModified());
					if (returnDate == null || fDate.after(returnDate)) {
						returnDate = fDate;
					}
				}
			}
			
		}
		return returnDate;
	}

}
