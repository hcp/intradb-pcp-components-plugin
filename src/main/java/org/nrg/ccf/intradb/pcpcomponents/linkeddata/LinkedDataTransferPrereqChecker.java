package org.nrg.ccf.intradb.pcpcomponents.linkeddata;

import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.common.utilities.components.XsyncUtils;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcpcomponents.abst.AbstractPipelinePrereqChecker;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xsync.configuration.ProjectSyncConfiguration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelinePrereqChecker
public class LinkedDataTransferPrereqChecker extends AbstractPipelinePrereqChecker implements PipelinePrereqCheckerI, ConfigurableComponentI {
	
	private static final String DEFAULT_REGEX_VALUE = "^.*$";
	private final XsyncUtils _xsyncUtils = XDAT.getContextService().getBean(XsyncUtils.class);
	protected final String _className = this.getClass().getName().replace(".", "");
	private Boolean _certainScansOnly = null; 
	private String _linkedDataScanRegex = null; 
	
	public List<String> getConfigurationYaml() {
		List<String> configYaml = new ArrayList<>();
		final StringBuilder sb = new StringBuilder()
				.append("certainScansOnly:\n")
				.append("    id: " + _className + "-regex-scans-only\n")
				.append("    name: " + _className + "-regex-scans-only\n")
				.append("    kind: panel.input.switchbox\n")
				.append("    label: LinkedData only for some scans?\n")
				.append("    description: WARNING: When yes, prereqs will pass if session has no linked data but ")
				.append("the session does not have scans matching the linked data regex.\n")
				.append("    value: false\n")
				.append("    onText: Yes\n")
				.append("    offText: No\n")
				.append("linkedDataScanRegex:\n")
				.append("    id: " + _className + "-linkeddata-scan-regex\n")
				.append("    name: " + _className + "-linkeddata-scan-regex\n")
				.append("    kind: panel.input.text\n")
				.append("    size: 50\n")
				.append("    label: Linked data scan regex\n")
				.append("    description: Regex to match by series desciription for scans requiring linked data.\n")
				.append("    placeholder: (?i)^.*(task|tfMRI).*$\n")
				;
		configYaml.add(sb.toString());
		return configYaml;
	}
	
	@Override
	public void checkPrereqs(PcpStatusEntity entity, UserI user) {
		log.info("Begin XSyncUtils validation:  " + entity.getEntityLabel());
		final ProjectSyncConfiguration xsyncConfig = _xsyncUtils.getProjectSyncConfiguration(entity.getProject(), user);
		if (xsyncConfig == null) {
			prereqsNotMet(entity, "XSync is not configured for this project");
			return;
		}
		final String remoteProject = xsyncConfig.getProjectSyncConfigurationFromDB().getSyncinfo().getRemoteProjectId();
		if (remoteProject == null) {
			prereqsNotMet(entity, "Remote project is not defined in this project's configuration");
			return;
		}
		final XnatImagesessiondata remoteSession = ExperimentUtils.getImagesessionByLabel(remoteProject, entity.getEntityLabel(), user);
		if (remoteSession == null) {
			prereqsNotMet(entity, "No matching session in the destination project");
			return;
		}
		final XnatImagesessiondata localSession = ExperimentUtils.getImagesessionByLabel(entity.getProject(), entity.getEntityLabel(), user);
		if (localSession == null) {
			prereqsNotMet(entity, "Could not return local session");
			return;
		}
		boolean hasLinkedDataResource = false;
		boolean hasLinkedDataScans = false;
		if (certainScansOnly(entity)) {
			for (final XnatImagescandataI scan : localSession.getScans_scan()) {
				final String sd = scan.getSeriesDescription().toLowerCase();
				if (sd.matches(linkedDataScanRegex(entity))) {
					hasLinkedDataScans = true;
					break;
				}
			}
			
		} 
		for (final XnatAbstractresourceI resource : localSession.getAllResources()) {
			if (resource.getLabel().equals("LINKED_DATA")) {
				hasLinkedDataResource = true;
				break;
			}
		}
		if (!hasLinkedDataResource && !_certainScansOnly) {
			prereqsNotMet(entity, "Session has no LINKED_DATA resources.");
			return;
		} if (!hasLinkedDataResource && hasLinkedDataScans) {
			prereqsNotMet(entity, "Session has linked-data-expected scans  but has no LINKED_DATA resources.");
			return;
		}
		prereqsMet(entity);
		log.info("XSyncUtils validation complete:  " + entity.getEntityLabel());
		return;
		
	}
	
	protected boolean certainScansOnly(PcpStatusEntity entity) {
		if (_certainScansOnly == null) {
			try {
				final String configValue = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(), 
					this.getClass().getName(), "-regex-scans-only", false).toString();
				_certainScansOnly = (configValue!=null) ? Boolean.valueOf(configValue) : false;
			} catch (Exception e) {
				_certainScansOnly = true;
			}
		}
		return _certainScansOnly;
	}
	
	protected String linkedDataScanRegex(PcpStatusEntity entity) {
		if (_linkedDataScanRegex == null) {
			try {
				final String configValue = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(), 
					this.getClass().getName(), "-linkeddata-scan-regex", false).toString();
				_linkedDataScanRegex = (configValue!=null) ? configValue : DEFAULT_REGEX_VALUE;
			} catch (Exception e) {
				_linkedDataScanRegex = DEFAULT_REGEX_VALUE;
			}
		}
		return _linkedDataScanRegex;
	}
	
}
