package org.nrg.ccf.intradb.pcpcomponents.linkeddata;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.common.utilities.components.NodeUtils;
import org.nrg.ccf.common.utilities.components.XsyncUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.commons.utilities.exception.CcfResourceException;
import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConstants.PcpStatus;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatImagescandataI;
//import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.base.auto.AutoXnatImagescandata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnat.services.archive.CatalogService.Operation;
import org.nrg.xsync.configuration.ProjectSyncConfiguration;

import com.google.common.collect.ImmutableList;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineSubmitter
public class LinkedDataTransferSubmitter implements PipelineSubmitterI {
	
	private final XsyncUtils _xsyncUtils = XDAT.getContextService().getBean(XsyncUtils.class);
	private final NodeUtils _nodeUtils = XDAT.getContextService().getBean(NodeUtils.class);
	private final CatalogService _catalogService = XDAT.getContextService().getBean(CatalogService.class);
	
	@Override
	public List<String> getParametersYaml(String projectId, String pipelineId) {
		return ImmutableList.of();
	}
	
	@Override
	public boolean submitJob(PcpStatusEntity entity, PcpStatusEntityService entityService, PipelineValidatorI validator,
			Map<String, String> parameters, UserI user) throws PcpSubmitException {
		final ProjectSyncConfiguration xsyncConfig = _xsyncUtils.getProjectSyncConfiguration(entity.getProject(), user);
		final String remoteProject = xsyncConfig.getProjectSyncConfigurationFromDB().getSyncinfo().getRemoteProjectId();
		if (remoteProject == null) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("Remote project is not defined in this project's configuration");
			entity.setStatusTime(new Date());
			entityService.update(entity);
			return false;
		}
		final XnatImagesessiondata localSession = ExperimentUtils.getImagesessionByLabel(entity.getProject(), entity.getEntityLabel(), user);
		if (localSession == null) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("Could not retrieve local session");
			entity.setStatusTime(new Date());
			entityService.update(entity);
			return false;
		}
		final XnatImagesessiondata remoteSession = ExperimentUtils.getImagesessionByLabel(remoteProject, entity.getEntityLabel(), user);
		if (remoteSession == null) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("Could not retrieve remote session");
			entity.setStatusTime(new Date());
			entityService.update(entity);
			return false;
		}
		entity.setStatus(PcpStatus.RUNNING);
		final Date statusTime = new Date();
		entity.setStatusInfo("Status set to running at (TIME=" + statusTime + ", NODE=" + _nodeUtils.getXnatNode() + ")");
		entity.setStatusTime(statusTime);
		entityService.update(entity);
		if (transferLinkedData(localSession, remoteSession, user)) {
			entity.setStatus(PcpStatus.COMPLETE);
			entity.setStatusInfo("Linked data successfully transferred to remote session");
		} else {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("ERROR:  Linked data transfer process was not successful.  Please check files."); 
		}
		entity.setStatusTime(new Date());
		entityService.update(entity);
		return false;
	}

	private boolean transferLinkedData(XnatImagesessiondata localSession, XnatImagesessiondata remoteSession, UserI user) {
		boolean returnStatus = true;
		// Session Resource
		if (ResourceUtils.hasResource(localSession, CommonConstants.LINKED_DATA_RESOURCE)) {
			final XnatResourcecatalog localResource = ResourceUtils.getResource(localSession, CommonConstants.LINKED_DATA_RESOURCE);
			XnatResourcecatalog remoteResource = null;
			if (ResourceUtils.hasResource(remoteSession, CommonConstants.LINKED_DATA_RESOURCE)) {
				remoteResource = ResourceUtils.getResource(remoteSession, CommonConstants.LINKED_DATA_RESOURCE);
			} else {
				try {
					remoteResource = ResourceUtils.createSessionResource(remoteSession,  CommonConstants.LINKED_DATA_RESOURCE,  user);
				} catch (ServerException | ClientException e) {
					log.error(ExceptionUtils.getStackTrace(e));
					returnStatus = false;
				}
			}
			if (localResource != null && remoteResource != null) {
				try {
					transferFiles(localResource, remoteResource);
					final StringBuilder resourceSB = new StringBuilder("/archive/projects/").append(remoteSession.getProject())
							.append("/subjects/").append(remoteSession.getSubjectId()).append("/experiments/").append(remoteSession.getId())
							.append("/resources/").append(CommonConstants.LINKED_DATA_RESOURCE);
					_catalogService.refreshResourceCatalog(user, resourceSB.toString(), Operation.ALL);
				} catch (IOException | CcfResourceException | ServerException | ClientException e) {
					log.error(ExceptionUtils.getStackTrace(e));
					returnStatus = false;
				}
			}
		}
		// Scan Resources
		final List<XnatImagescandataI> remoteScans = remoteSession.getScans_scan();
		for (final XnatImagescandataI localScan : localSession.getScans_scan()) {
			if (!ResourceUtils.hasResource(localScan, CommonConstants.LINKED_DATA_RESOURCE)) {
				continue;
			}
			final XnatImagescandataI remoteScan = ExperimentUtils.getCorrespondingScan(remoteScans, localScan);
			if (remoteScan == null) {
				returnStatus = false;
			}
			final XnatResourcecatalog localResource = ResourceUtils.getResource(localScan, CommonConstants.LINKED_DATA_RESOURCE);
			XnatResourcecatalog remoteResource = null;
			if (ResourceUtils.hasResource(remoteScan, CommonConstants.LINKED_DATA_RESOURCE)) {
				remoteResource = ResourceUtils.getResource(remoteScan, CommonConstants.LINKED_DATA_RESOURCE);
			} else {
				try {
					if (remoteScan instanceof AutoXnatImagescandata) {
						remoteResource = ResourceUtils.createScanResource(remoteSession, (AutoXnatImagescandata) remoteScan, CommonConstants.LINKED_DATA_RESOURCE,  user);
					} else {
						returnStatus = false;
					}
				} catch (ServerException | ClientException e) {
					log.error(ExceptionUtils.getStackTrace(e));
					returnStatus = false;
				}
			}
			if (localResource != null && remoteResource != null) {
				try {
					transferFiles(localResource, remoteResource);
					final StringBuilder resourceSB = new StringBuilder("/archive/projects/").append(remoteSession.getProject())
							.append("/subjects/").append(remoteSession.getSubjectId()).append("/experiments/").append(remoteSession.getId())
							.append("/scans/").append(remoteScan.getId()).append("/resources/").append(CommonConstants.LINKED_DATA_RESOURCE);
					_catalogService.refreshResourceCatalog(user, resourceSB.toString(), Operation.ALL);
				} catch (IOException | CcfResourceException | ServerException | ClientException e) {
					log.error(ExceptionUtils.getStackTrace(e));
					returnStatus = false;
				}
			}
		}
		return returnStatus;
	}

	private void transferFiles(XnatResourcecatalog localResource, XnatResourcecatalog remoteResource) throws IOException, CcfResourceException {
		final ArrayList<File> localFiles = localResource.getCorrespondingFiles(CommonConstants.ROOT_PATH);
		final String localResourceP = localResource.getCatalogFile(CommonConstants.ROOT_PATH).getParentFile().getCanonicalPath();
		for (final File localFile : localFiles) {
			final File remoteFileRequired = requiredFile(remoteResource, localResourceP, localFile);
			if (remoteFileRequired == null) { 
				continue;
			}
			FileUtils.copyFile(localFile, remoteFileRequired, true);
		}
	}

	private File requiredFile(XnatResourcecatalog remoteResource, String localResourcePath, File localFile) throws IOException, CcfResourceException {
		final String remoteResourcePath =remoteResource.getCatalogFile(CommonConstants.ROOT_PATH).getParentFile().getCanonicalPath();
		final String localFilePath = localFile.getCanonicalPath();
		if (!localFilePath.contains(localResourcePath)) {
			throw new CcfResourceException("Unable to handle resource file.  File path doesn't contain resource path (FILE=" +
						localFile.getCanonicalPath() + ")");
		}
		final String remoteFilePath = localFilePath.replaceFirst(localResourcePath, remoteResourcePath);
		final File remoteFile = new File(remoteFilePath);
		if (remoteFile.exists() && !(remoteFile.lastModified()<localFile.lastModified())) {
			return null;
		}
		return remoteFile;
	}
	
}
