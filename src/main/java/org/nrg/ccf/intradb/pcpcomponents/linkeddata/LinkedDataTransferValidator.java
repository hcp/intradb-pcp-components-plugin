package org.nrg.ccf.intradb.pcpcomponents.linkeddata;

import org.nrg.ccf.common.utilities.components.XsyncUtils;
import org.nrg.ccf.common.utilities.pojos.SessionComparisonAdvice;
import org.nrg.ccf.common.utilities.pojos.SessionComparisonResult;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcpcomponents.abst.AbstractPipelineValidator;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xsync.configuration.ProjectSyncConfiguration;
import com.google.common.collect.ImmutableList;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineValidator
public class LinkedDataTransferValidator extends AbstractPipelineValidator implements PipelineValidatorI {
	
	private final XsyncUtils _xsyncUtils = XDAT.getContextService().getBean(XsyncUtils.class);
	protected SessionComparisonAdvice _comparisonAdvice = 
		new SessionComparisonAdvice(ImmutableList.<String>of(), ImmutableList.<String>of(), ImmutableList.of("NIFTI","DICOM","DICOM_ORIG","SNAPSHOTS","DEFACE_QC"), ImmutableList.<String>of());
	
	public void setComparisonAdvice(SessionComparisonAdvice comparisonAdvice) {
		_comparisonAdvice = comparisonAdvice;
	}
	
	@Override
	public void validate(PcpStatusEntity entity, UserI user) {
		log.debug("Begin LinkedDataTransfer validation:  " + entity.getEntityLabel());
		final ProjectSyncConfiguration xsyncConfig = _xsyncUtils.getProjectSyncConfiguration(entity.getProject(), user);
		if (xsyncConfig == null) {
			validationFailed(entity, "XSync is not configured for this project");
			return;
		}
		final String remoteProject = xsyncConfig.getProjectSyncConfigurationFromDB().getSyncinfo().getRemoteProjectId();
		if (remoteProject == null) {
			validationFailed(entity, "Remote project is not defined in this project's configuration");
			return;
		}
		final XnatImagesessiondata remoteSession = ExperimentUtils.getImagesessionByLabel(remoteProject, entity.getEntityLabel(), user);
		if (remoteSession == null) {
			validationFailed(entity, "No matching session in the destination project");
			return;
		}
		final XnatImagesessiondata localSession = ExperimentUtils.getImagesessionByLabel(entity.getProject(), entity.getEntityLabel(), user);
		if (localSession == null) {
			validationFailed(entity, "Could not return local session");
			return;
		}
		final SessionComparisonResult compareResult = ExperimentUtils.compareSessions(localSession, remoteSession, _comparisonAdvice); 
		final SessionComparisonResult customResult = doCustomSessionComparison(localSession, remoteSession, _comparisonAdvice);
		if (compareResult.getIsMatch() && compareResult.getIsMatch()) {
			validationPassed(entity, "Remote session matches local session");
		} else if (!customResult.getIsMatch()) {
			validationFailed(entity, "Remote session does not match local session:  " + customResult.getResult());
		} else { 
			validationFailed(entity, "Remote session does not match local session:  " + compareResult.getResult());
		}
		log.debug("LinkedDataTransfer validation complete:  " + entity.getEntityLabel());
		return;
	}
	
	public SessionComparisonResult doCustomSessionComparison(XnatImagesessiondata localSession, XnatImagesessiondata remoteSession, 
			SessionComparisonAdvice _comparisonAdvice2) {
		// No custom comparisons in default class.  This is meant to be overridden.
		return SessionComparisonResult.matches();
	}
	
}
