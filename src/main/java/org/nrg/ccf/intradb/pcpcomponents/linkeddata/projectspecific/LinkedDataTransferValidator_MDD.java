package org.nrg.ccf.intradb.pcpcomponents.linkeddata.projectspecific;

import java.util.HashMap;
import java.util.Map;

import org.nrg.ccf.intradb.pcpcomponents.linkeddata.LinkedDataTransferValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;

@PipelineValidator
public class LinkedDataTransferValidator_MDD extends LinkedDataTransferValidator {
	
	public LinkedDataTransferValidator_MDD() {
		super();
		Map<String, Map<String, String>> alternates = _comparisonAdvice.getAlternateFilePathTransforms();
		Map<String,String> transformMap = new HashMap<String, String>();
		transformMap.put("/EVs/","/EVs_ORIG/");
		alternates.put("^.*/EVs/.*$", transformMap);
	}

}
