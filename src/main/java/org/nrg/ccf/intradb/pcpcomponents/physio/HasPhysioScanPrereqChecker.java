package org.nrg.ccf.intradb.pcpcomponents.physio;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class HasPhysioScanPrereqChecker implements PipelinePrereqCheckerI {
	
	final static List<String> MAIN_SCAN_TYPES = Arrays.asList(new String[] { "T1w","T2w","rfMRI","tfMRI","dMRI" }); 

	@Override
	public void checkPrereqs(final PcpStatusEntity statusEntity, final UserI user) {
		
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		boolean hasPhysio = false;
		for (final XnatImagescandataI scan : session.getScans_scan()) {
			
			final String scanType = scan.getType();
			final String seriesDesc = scan.getSeriesDescription();
			if (scan.getQuality().equalsIgnoreCase("unusable")) {
				continue;
			}
			if (scanType.toLowerCase().contains("_physio") || seriesDesc.toLowerCase().contains("_physio")) {
				hasPhysio = true;
				break;
			}
		}
		if (!hasPhysio) {
			final String infoString = "Session has no usable physio scans";
			if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoString)) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo(infoString);
			} 
		} else {
			boolean hasMainScan = false;
			for (final XnatImagescandataI scan : session.getScans_scan()) {
				final String scanType = scan.getType();
				if (MAIN_SCAN_TYPES.contains(scanType)) {
					hasMainScan = true;
					break;
				}
			}
			if (hasMainScan) {
				if (!statusEntity.getPrereqs()) {
					statusEntity.setPrereqs(true);
					statusEntity.setPrereqsTime(new Date());
					statusEntity.setPrereqsInfo("");
				}
			} else {
				final String infoString = "The pre-pipeline may not have run for this session. (There are no main scan types.";
				if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoString)) {
					statusEntity.setPrereqs(false);
					statusEntity.setPrereqsTime(new Date());
					statusEntity.setPrereqsInfo(infoString);
				} 
			}
		}
		
	}

}
