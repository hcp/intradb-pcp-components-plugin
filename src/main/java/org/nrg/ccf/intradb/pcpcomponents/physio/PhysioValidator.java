package org.nrg.ccf.intradb.pcpcomponents.physio;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.physioev.PhysioEvValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

@PipelineValidator
public class PhysioValidator extends PhysioEvValidator implements PipelineValidatorI {
	
	// Currently, we're not generating dMRI files.  There is a problem with the files generated for dMRI.
	final static List<String> LINKED_DATA_TYPES = Arrays.asList(new String[] { "rfMRI","tfMRI" }); 
	static final String PHYSIO_LOG_EXT = "_PhysioLog";

	@Override
	// Basically the same as physioEv validator except that EV validation is not done
	public void validate(final PcpStatusEntity statusEntity, final UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		boolean sessionOk = true;
		final List<XnatImagescandataI> scanList = session.getScans_scan();
		for (int i=0; i<scanList.size(); i++) {
		    final XnatImagescandataI scan = scanList.get(i);
			final String st = scan.getType();
			final String scanQuality = scan.getQuality();
			if (!LINKED_DATA_TYPES.contains(st) || scanQuality.equalsIgnoreCase("UNUSABLE")) {
				continue;
			}
			XnatResourcecatalog linkedDataResource = null;
			if (hasPhysioScan(scan,scanList,i)) {
				if (!hasPhysioCsv(scan,linkedDataResource)) {
					infoSB.append("<li>Scan " + scan.getId() + " is missing physio CSV file (scanQuality=" + scanQuality + ").</li>");
					sessionOk = false;
				}
			}
		}
		if (sessionOk) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo("");
			}
		} else {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoSB.toString())) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(infoSB.toString());
			}
		}
	}

}
