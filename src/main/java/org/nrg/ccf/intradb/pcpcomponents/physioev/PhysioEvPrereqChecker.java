package org.nrg.ccf.intradb.pcpcomponents.physioev;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class PhysioEvPrereqChecker implements PipelinePrereqCheckerI {
	
	final static List<String> MAIN_SCAN_TYPES = Arrays.asList(new String[] { "T1w","T2w","rfMRI","tfMRI","dMRI" }); 

	@Override
	public void checkPrereqs(final PcpStatusEntity statusEntity, final UserI user) {
		
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		boolean hasTfmri = false;
		final String entityLabel = statusEntity.getEntityLabel();
		if ((entityLabel.contains("7T") || entityLabel.contains("MRS")) && !statusEntity.getImpeded()) {
			statusEntity.setImpeded(true);
			statusEntity.setImpededInfo("No physio or EV code is in place for 7T or MRS");
			statusEntity.setImpededTime(new Date());
		}
		for (final XnatImagescandataI scan : session.getScans_scan()) {
			final String scanType = scan.getType();
			if (!scanType.equalsIgnoreCase("tfMRI")) {
				continue;
			}
			// Let's not consider unusable scans
			if (scan.getQuality().equalsIgnoreCase("unusable")) {
				continue;
			}
			hasTfmri = true;
			for (final XnatAbstractresourceI resource : scan.getFile()) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
					if (!statusEntity.getPrereqs()) {
						statusEntity.setPrereqs(true);
						statusEntity.setPrereqsTime(new Date());
						statusEntity.setPrereqsInfo("");
					}
					return;
				}
			}
		}
		if (hasTfmri) {
			final String infoString = "Linked data has not been uploaded for this session";
			if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoString)) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo(infoString);
			} 
		} else {
			// Let's just do a very rough check to weed out sessions where scan type hasn't been set
			boolean hasMainScan = false;
			for (final XnatImagescandataI scan : session.getScans_scan()) {
				final String scanType = scan.getType();
				if (MAIN_SCAN_TYPES.contains(scanType)) {
					hasMainScan = true;
					break;
				}
			}
			if (hasMainScan) {
				if (!statusEntity.getPrereqs()) {
					statusEntity.setPrereqs(true);
					statusEntity.setPrereqsTime(new Date());
					statusEntity.setPrereqsInfo("");
				}
			} else {
				final String infoString = "The pre-pipeline may not have run for this session. (There are no main scan types.";
				if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoString)) {
					statusEntity.setPrereqs(false);
					statusEntity.setPrereqsTime(new Date());
					statusEntity.setPrereqsInfo(infoString);
				} 
			}
		}
		
	}

}
