package org.nrg.ccf.intradb.pcpcomponents.physioev;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;

@PipelineValidator
public class PhysioEvValidator implements PipelineValidatorI {
	
	// Currently, we're not generating dMRI files.  There is a problem with the files generated for dMRI.
	final static List<String> LINKED_DATA_TYPES = Arrays.asList(new String[] { "rfMRI","tfMRI" }); 
	static final String PHYSIO_LOG_EXT = "_PhysioLog";

	@Override
	public void validate(final PcpStatusEntity statusEntity, final UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		boolean sessionOk = true;
		final List<XnatImagescandataI> scanList = session.getScans_scan();
		for (int i=0; i<scanList.size(); i++) {
		    final XnatImagescandataI scan = scanList.get(i);
			final String st = scan.getType();
			final String scanQuality = scan.getQuality();
			if (!LINKED_DATA_TYPES.contains(st) || scanQuality.equalsIgnoreCase("UNUSABLE")) {
				continue;
			}
			XnatResourcecatalog linkedDataResource = null;
			if (hasPhysioScan(scan,scanList,i)) {
				if (!hasPhysioCsv(scan,linkedDataResource)) {
					infoSB.append("<li>Scan " + scan.getId() + " is missing physio CSV file (scanQuality=" + scanQuality + ").</li>");
					sessionOk = false;
				}
			}
			if (st.equals("tfMRI")) {
				if (!hasWideCsvAndEv(scan,linkedDataResource,infoSB)) {
					sessionOk = false;
				}
			}
		}
		if (sessionOk) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo("");
			}
		} else {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoSB.toString())) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(infoSB.toString());
			}
		}
	}

	protected boolean hasWideCsvAndEv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource, StringBuilder infoSB) {
		final String scanQuality = scan.getQuality();
		if (scanQuality.equalsIgnoreCase("UNUSABLE")) {
			return true;
		}
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				infoSB.append("<li>Scan " + scan.getId() + " has no linked data resource (scanQuality=" + scanQuality + ").</li>");
				return false;
			}
		}
		boolean hasWideCsv = false;
		boolean hasGeneratedWide = false;
		boolean hasEV = false;
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fn = f.getName().toLowerCase();
			if (fn.contains("_wide.csv")) {
				hasWideCsv = true;
				if (fn.matches("^.*_run[0-9]_wide.csv")) {
					hasGeneratedWide = true;
				}
			} else {
				final String fp = f.getAbsolutePath();
				if (fp.contains("/EVs/")) {
					hasEV = true;
				}
			}
			if (hasWideCsv && hasGeneratedWide && hasEV) {
				return true;
			}
		}
		infoSB.append("<li>Scan " + scan.getId() + " is missing EV or wide.csv file(s) (scanQuality=" + scanQuality + ").</li>");
		return false;
	}

	protected boolean hasPhysioCsv(final XnatImagescandataI scan, XnatResourcecatalog linkedDataResource) {
		if (linkedDataResource  == null) {
			linkedDataResource = initializeLinkedDataResource(scan);
			if (linkedDataResource == null) {
				return false;
			}
		}
		for (final ResourceFile cFile : linkedDataResource.getFileResources(CommonConstants.ROOT_PATH)) {
			final File f = cFile.getF();
			final String fp = f.getAbsolutePath();
			if (!fp.contains("/PHYSIO/")) {
				continue;
			}
			if (fp.toLowerCase().endsWith(".csv")) {
				return true;
			}
		}
		return false;
	}

	protected boolean hasPhysioScan(final XnatImagescandataI scan, final List<XnatImagescandataI> scanList, int i) {
		final String sd = scan.getSeriesDescription();
		for (int j=i+1; j<=i+2; j++) {
			if (j>=scanList.size()) {
				break;
			}
			final XnatImagescandataI compScan = scanList.get(j);
			if (compScan.getSeriesDescription().equals(sd + PHYSIO_LOG_EXT)) {
				return true;
			}
		}
		for (int j=i-1; j>=0 && j>=i-2; j--) {
			final XnatImagescandataI compScan = scanList.get(j);
			if (compScan.getSeriesDescription().equals(sd + PHYSIO_LOG_EXT)) {
				return true;
			}
		}
		return false;
	}

	protected XnatResourcecatalog initializeLinkedDataResource(final XnatImagescandataI scan) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			if (resource.getLabel().equals(CommonConstants.LINKED_DATA_RESOURCE)) {
				return (XnatResourcecatalog)resource;
			}
		}
		return null;
	}

}
