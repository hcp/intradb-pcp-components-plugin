package org.nrg.ccf.intradb.pcpcomponents.prepipeline;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineValidator
public class PrePipelineValidator implements PipelineValidatorI {
	
	static final String RESOURCE_LBL = "scantype_map";
	private static final Map<String,List<List<Map<String,String>>>> _projectTypeMaps = new HashMap<>();
	private static final Type _mapType = new TypeToken<Map<String,List<Map<String,String>>>>(){}.getType();
	private static final List<String> _initializedProjects = new ArrayList<>();
	private static final Gson _gson = new Gson();

	@Override
	public void validate(final PcpStatusEntity statusEntity, final UserI user) {
		final XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(statusEntity.getProject(), user, false);
		initializeScanTypeMaps(proj);
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		boolean sessionOk = prePipelineValidate(statusEntity.getProject(), session, user, infoSB);
		if (sessionOk) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo("");
			} 
		} else {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoSB.toString())) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(infoSB.toString());
			} 
		}
	}
	
	private static void initializeScanTypeMaps(final String projectId, UserI user) {
		if (projectId != null && !_initializedProjects.contains(projectId)) {
			final List<List<Map<String,String>>> typeMapL = getTypeMaps(projectId);
			if (typeMapL.isEmpty()) {
				XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(projectId, user, false);
				populateScanTypeMap(proj,typeMapL);
			}
		}
	}

	
	private static void initializeScanTypeMaps(XnatProjectdata proj) {
		if (proj != null) {
			final String projectId = proj.getId();
			if (_initializedProjects.contains(projectId)) {
				return;
			}
			final List<List<Map<String,String>>> typeMapL = getTypeMaps(proj.getId());
			if (typeMapL.isEmpty()) {
				populateScanTypeMap(proj,typeMapL);
			}
		}
	}

	private static void populateScanTypeMap(XnatProjectdata proj, List<List<Map<String, String>>> typeMapL) {
		final List<XnatAbstractresourceI> resources = proj.getResources_resource();
		for (final XnatAbstractresourceI resource : resources) {
			if (!resource.getLabel().equals(RESOURCE_LBL)) {
				continue;
			}
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			final XnatResourcecatalog catalog = (XnatResourcecatalog) resource;
			for (final ResourceFile rf : catalog.getFileResources(CommonConstants.ROOT_PATH)) {
				final File f = rf.getF();
				String fJson;
				try {
					fJson = FileUtils.readFileToString(f);
					final Map<String,List<Map<String,String>>> typeMapM = _gson.fromJson(fJson, _mapType);
					final List<Map<String,String>> typeMap = typeMapM.get("scan_type_map");
					if (typeMap==null) {
						log.warn("WARNING:  scantype mapping file (" + f.getName() + ") not in expected format.");
						continue;
					}
					typeMapL.add(typeMap);
				} catch (IOException|JsonSyntaxException e) {
					log.warn("WARNING:  Could not read/parse scantype mapping file:  ", e);
				}
			}
		}
		_initializedProjects.add(proj.getId());
	}

	private static List<List<Map<String, String>>> getTypeMaps(String projectId) {
		if (!_projectTypeMaps.containsKey(projectId)) {
			final List<List<Map<String,String>>> typeMap = new ArrayList<>();
			_projectTypeMaps.put(projectId, typeMap);
		}
		return _projectTypeMaps.get(projectId);
	}

	public static boolean prePipelineValidate(final String projectId, final XnatMrsessiondata session, UserI user) {
		return prePipelineValidate(projectId, session, user, null);
	}
	
	private static boolean prePipelineValidate(final String projectId, final XnatMrsessiondata session, 
			UserI user, final StringBuilder infoSB) {
		boolean sessionOk = true;
		initializeScanTypeMaps(projectId, user);
		for (final XnatImagescandataI scan : session.getScans_scan()) {
			final String scanId = scan.getId();
			final String scanType = (scan.getType() != null) ? scan.getType() : null;
			final String seriesDesc = (scan.getSeriesDescription() != null) ? scan.getSeriesDescription() : "";
			final String modSeriesDesc = seriesDesc.toUpperCase().replaceAll("[^A-Z0-9]", "");
			final List<List<Map<String,String>>> typeMaps = getTypeMaps(projectId);
			if (typeMaps.size()>=1) {
				boolean isOk = true;
				boolean isMatched = false;
				outerloop:
				for (List<Map<String,String>> typeMapL : typeMaps) {
					for (Map<String,String> typeMap : typeMapL) {
						final String mapDesc = typeMap.get("series_description");
						final String mapType = typeMap.get("scan_type");
						if (mapDesc != null && mapType != null) {
							if (mapDesc.equals(modSeriesDesc)) {
								if (!isMatched) {
									isOk = false;
								}
								isMatched = true;
								if (mapType.equals(scanType)) {
									isOk = true;
									break outerloop;
								}
							}
						}
					}
				}
				if (!isOk) {
					sessionOk = scanNotOk(scan, infoSB, scanId, scanType, sessionOk);
				}
			} else {
				if (seriesDesc.contains("rfMRI")) {
					if (scanType.contains("REST") || scanType.contains("_AP") || scanType.contains("_PA")) {
						sessionOk = scanNotOk(scan, infoSB, scanId, scanType, sessionOk);
					}
				} else if (seriesDesc.contains("T1w")) {
					if (scanType.contains("_vNav") || scanType.contains("_MPR")) {
						sessionOk = scanNotOk(scan, infoSB, scanId, scanType, sessionOk);
					}
				} else if (seriesDesc.contains("T2w")) {
					if (scanType.contains("_vNav") || scanType.contains("_SPC")) {
						sessionOk = scanNotOk(scan, infoSB, scanId, scanType, sessionOk);
					}
				} else if (seriesDesc.contains("SpinEchoFieldMap")) {
					if (scanType.contains("SpinEchoFieldMap")) {
						sessionOk = scanNotOk(scan, infoSB, scanId, scanType, sessionOk);
					}
				} else if (seriesDesc.contains("tfMRI")) {
					if (scanType.contains("_AP") || scanType.contains("_PA")) {
						sessionOk = scanNotOk(scan, infoSB, scanId, scanType, sessionOk);
					}
				}
			}
		}
		return sessionOk;
		
	}

	private static Boolean scanNotOk(final XnatImagescandataI scan, final StringBuilder infoSB, final String scanId, 
			final String scanType, boolean sessionOk) {
		// Let's ignore unusable scans.  They might sometimes have weird series descriptions and/or scan types.
		if (scan.getQuality().equalsIgnoreCase("unusable")) {
			return sessionOk;
		}
		if (infoSB != null) {
			infoSB.append("<li>SCAN " + scanId + " (ScanType=" + scanType + ") - ScanType not set correctly and scan is not unusable.</li>");
		}
		return false;
	}
	
}
