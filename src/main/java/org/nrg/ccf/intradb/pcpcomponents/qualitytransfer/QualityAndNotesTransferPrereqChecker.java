package org.nrg.ccf.intradb.pcpcomponents.qualitytransfer;

import java.util.List;

import org.nrg.ccf.common.utilities.components.XsyncUtils;
import org.nrg.ccf.common.utilities.pojos.SessionComparisonResult;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcpcomponents.abst.AbstractPipelinePrereqChecker;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xsync.configuration.ProjectSyncConfiguration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelinePrereqChecker
public class QualityAndNotesTransferPrereqChecker extends AbstractPipelinePrereqChecker implements PipelinePrereqCheckerI {
	
	private final XsyncUtils _xsyncUtils = XDAT.getContextService().getBean(XsyncUtils.class);
	protected final String _className = this.getClass().getName().replace(".", "");
	
	@Override
	public void checkPrereqs(PcpStatusEntity entity, UserI user) {
		log.info("Begin XSyncUtils validation:  " + entity.getEntityLabel());
		final ProjectSyncConfiguration xsyncConfig = _xsyncUtils.getProjectSyncConfiguration(entity.getProject(), user);
		if (xsyncConfig == null) {
			prereqsNotMet(entity, "XSync is not configured for this project");
			return;
		}
		final String remoteProject = xsyncConfig.getProjectSyncConfigurationFromDB().getSyncinfo().getRemoteProjectId();
		if (remoteProject == null) {
			prereqsNotMet(entity, "Remote project is not defined in this project's configuration");
			return;
		}
		final XnatMrsessiondata remoteSession = ExperimentUtils.getMrsessionByLabel(remoteProject, entity.getEntityLabel(), user);
		if (remoteSession == null) {
			prereqsNotMet(entity, "No matching session in the destination project");
			return;
		}
		final XnatMrsessiondata localSession = ExperimentUtils.getMrsessionByLabel(entity.getProject(), entity.getEntityLabel(), user);
		if (localSession == null) {
			prereqsNotMet(entity, "Could not return local session");
			return;
		}
		if (!localSession.getXSIType().equals(remoteSession.getXSIType())) {
			prereqsNotMet(entity, "Experiments are different types");
			return;
		}
		final List<XnatImagescandataI> localScans = localSession.getScans_scan();
		final List<XnatImagescandataI> remoteScans = remoteSession.getScans_scan();
		if (!(localScans.size() == remoteScans.size())) {
			prereqsNotMet(entity, "Experiments have non-matching scan count (local=" + localScans.size() +", remote=" + remoteScans.size() + ")");
			return;
		}
		prereqsMet(entity);
		return;
		
	}
	
}
