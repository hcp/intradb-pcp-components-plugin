package org.nrg.ccf.intradb.pcpcomponents.qualitytransfer;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.common.utilities.components.NodeUtils;
import org.nrg.ccf.common.utilities.components.XsyncUtils;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConstants.PcpStatus;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xsync.configuration.ProjectSyncConfiguration;

import com.google.common.collect.ImmutableList;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineSubmitter
public class QualityAndNotesTransferSubmitter implements PipelineSubmitterI {
	
	// TODO:  Make this configurable???
	private List<String> _sourceCheckedQualityValues = ImmutableList.<String>of("unusable");
	private List<String> _destOverrideQualityValues = ImmutableList.<String>of("undetermined","usable"); 
	
	private final XsyncUtils _xsyncUtils = XDAT.getContextService().getBean(XsyncUtils.class);
	private final NodeUtils _nodeUtils = XDAT.getContextService().getBean(NodeUtils.class);
	
	@Override
	public List<String> getParametersYaml(String projectId, String pipelineId) {
		return ImmutableList.of();
	}
	
	@Override
	public boolean submitJob(PcpStatusEntity entity, PcpStatusEntityService entityService, PipelineValidatorI validator,
			Map<String, String> parameters, UserI user) throws PcpSubmitException {
		final ProjectSyncConfiguration xsyncConfig = _xsyncUtils.getProjectSyncConfiguration(entity.getProject(), user);
		final String remoteProject = xsyncConfig.getProjectSyncConfigurationFromDB().getSyncinfo().getRemoteProjectId();
		if (remoteProject == null) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("Remote project is not defined in this project's configuration");
			entity.setStatusTime(new Date());
			entityService.update(entity);
			return false;
		}
		final XnatMrsessiondata localSession = ExperimentUtils.getMrsessionByLabel(entity.getProject(), entity.getEntityLabel(), user);
		if (localSession == null) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("Could not retrieve local session");
			entity.setStatusTime(new Date());
			entityService.update(entity);
			return false;
		}
		final XnatMrsessiondata remoteSession = ExperimentUtils.getMrsessionByLabel(remoteProject, entity.getEntityLabel(), user);
		if (remoteSession == null) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("Could not retrieve remote session");
			entity.setStatusTime(new Date());
			entityService.update(entity);
			return false;
		}
		entity.setStatus(PcpStatus.RUNNING);
		final Date statusTime = new Date();
		entity.setStatusInfo("Status set to running at (TIME=" + statusTime + ", NODE=" + _nodeUtils.getXnatNode() + ")");
		entity.setStatusTime(statusTime);
		entityService.update(entity);
		try {
			if (transferQualityAndNotesValues(localSession, remoteSession, user)) {
				entity.setStatus(PcpStatus.COMPLETE);
				entity.setStatusInfo("Quality values and/or notes successfully transferred to remote session");
			} else {
				entity.setStatus(PcpStatus.ERROR);
				entity.setStatusInfo("ERROR:  Setting of quality values and/or notes was not successful.  Please check session."); 
			}
		} catch (Exception e) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("ERROR:  Setting of quality values and/or notes was not successful (threw exception).  Please check session."); 
			log.error("Exception thrown setting notes and/or quality values", e);
		}
		entity.setStatusTime(new Date());
		entityService.update(entity);
		return false;
	}

	private boolean transferQualityAndNotesValues(XnatMrsessiondata localSession, XnatMrsessiondata remoteSession, UserI user) throws Exception {
		boolean returnStatus = true;
		final List<XnatImagescandataI> localScans = localSession.getScans_scan();
		final List<XnatImagescandataI> remoteScans = remoteSession.getScans_scan();
		if (!(localScans.size() == remoteScans.size())) {
			return false;
		}
		for (final XnatImagescandataI localScan : localScans) {
			final XnatImagescandataI remoteScan = getCorrespondingScan(remoteScans, localScan);
			if (!(remoteScan instanceof XnatMrscandata)) {
				continue;
			}
			final String localQuality = localScan.getQuality();
			final String remoteQuality = remoteScan.getQuality();
			final String localNote = localScan.getNote();
			final String remoteNote = remoteScan.getNote();
			if (_sourceCheckedQualityValues.contains(localQuality) && !localQuality.equals(remoteQuality) && _destOverrideQualityValues.contains(remoteQuality)) {
				remoteScan.setQuality(localQuality);
				if (!SaveItemHelper.authorizedSave((XnatMrscandata)remoteScan,user,false,true,EventUtils.DEFAULT_EVENT(user, null))) {
					log.error("ERROR:  Unable to save scan (Session=" + remoteSession.getLabel() + ", Scan=" + remoteScan.getId() + ")");
					returnStatus = false;
				} 
			}
			if (localNote != null && localNote.trim().length()>0 && !(remoteNote != null && remoteNote.contains(localNote))) {
				if (remoteNote == null) {
					remoteScan.setNote("[ITK] " + localNote);
				} else {
					remoteScan.setNote("[ITK] " + localNote + " " + remoteNote);
				}
				if (!SaveItemHelper.authorizedSave((XnatMrscandata)remoteScan,user,false,true,EventUtils.DEFAULT_EVENT(user, null))) {
					log.error("ERROR:  Unable to save scan (Session=" + remoteSession.getLabel() + ", Scan=" + remoteScan.getId() + ")");
					returnStatus = false;
				} 
			}
		}
		final String localSessionNote = localSession.getNote();
		final String remoteSessionNote = remoteSession.getNote();
		if (localSessionNote != null && localSessionNote.trim().length()>0 && !(remoteSessionNote != null && remoteSessionNote.contains(localSessionNote))) {
			if (localSessionNote != null && localSessionNote.trim().length()>0 && !(remoteSessionNote != null && remoteSessionNote.contains(localSessionNote))) {
				if (remoteSessionNote == null) {
					remoteSession.setNote("[ITK] " + localSessionNote);
				} else {
					remoteSession.setNote("[ITK] " + localSessionNote + " " + remoteSessionNote);
				}
				if (!SaveItemHelper.authorizedSave(remoteSession,user,false,true,EventUtils.DEFAULT_EVENT(user, null))) {
					log.error("ERROR:  Unable to save session (Session=" + remoteSession.getLabel() + ")");
					returnStatus = false;
				} 
			}
		}
		return returnStatus;
	}
	
	public static XnatImagescandataI getCorrespondingScan(List<XnatImagescandataI> remoteScans, XnatImagescandataI localScan) {
		final String scanId = localScan.getId();
		for (final XnatImagescandataI remoteScan : remoteScans) {
			if (scanId.equals(remoteScan.getId())) {
				return remoteScan;
			}
		}
		return null;
	}

}
