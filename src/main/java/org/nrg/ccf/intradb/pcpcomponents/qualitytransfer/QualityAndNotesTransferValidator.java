package org.nrg.ccf.intradb.pcpcomponents.qualitytransfer;

import java.util.List;

import org.nrg.ccf.common.utilities.components.XsyncUtils;
import org.nrg.ccf.common.utilities.pojos.SessionComparisonAdvice;
import org.nrg.ccf.common.utilities.pojos.SessionComparisonResult;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcpcomponents.abst.AbstractPipelineValidator;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xsync.configuration.ProjectSyncConfiguration;
import com.google.common.collect.ImmutableList;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineValidator
public class QualityAndNotesTransferValidator extends AbstractPipelineValidator implements PipelineValidatorI {
	
	// TODO:  Make this configurable???
	private List<String> _sourceCheckedQualityValues = ImmutableList.<String>of("unusable");
	private List<String> _destOverrideQualityValues = ImmutableList.<String>of("undetermined","usable"); 
	
	private final XsyncUtils _xsyncUtils = XDAT.getContextService().getBean(XsyncUtils.class);
	
	@Override
	public void validate(PcpStatusEntity entity, UserI user) {
		log.debug("Begin LinkedDataTransfer validation:  " + entity.getEntityLabel());
		final ProjectSyncConfiguration xsyncConfig = _xsyncUtils.getProjectSyncConfiguration(entity.getProject(), user);
		if (xsyncConfig == null) {
			validationFailed(entity, "XSync is not configured for this project");
			return;
		}
		final String remoteProject = xsyncConfig.getProjectSyncConfigurationFromDB().getSyncinfo().getRemoteProjectId();
		if (remoteProject == null) {
			validationFailed(entity, "Remote project is not defined in this project's configuration");
			return;
		}
		final XnatMrsessiondata remoteSession = ExperimentUtils.getMrsessionByLabel(remoteProject, entity.getEntityLabel(), user);
		if (remoteSession == null) {
			validationFailed(entity, "No matching session in the destination project");
			return;
		}
		final XnatMrsessiondata localSession = ExperimentUtils.getMrsessionByLabel(entity.getProject(), entity.getEntityLabel(), user);
		if (localSession == null) {
			validationFailed(entity, "Could not return local session");
			return;
		}
		final SessionComparisonResult compareResult = ExperimentUtils.compareSessionQualityAndNotes(localSession, remoteSession, _sourceCheckedQualityValues, _destOverrideQualityValues); 
		if (compareResult.getIsMatch()) {
			validationPassed(entity, "Remote session notes and quality ratings okay.");
		} else { 
			validationFailed(entity, "Remote session notes/quality issue:  " + compareResult.getResult());
		}
		log.debug("LinkedDataTransfer validation complete:  " + entity.getEntityLabel());
		return;
	}
	
}
