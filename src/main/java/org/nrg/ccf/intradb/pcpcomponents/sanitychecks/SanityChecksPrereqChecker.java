package org.nrg.ccf.intradb.pcpcomponents.sanitychecks;


import org.nrg.ccf.intradb.pcpcomponents.shared.SessionCompletePrereqChecker;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;

/* Just keeping this one around even though it just implements SessionCompletePrereqChecker because it's already in use. */

@PipelinePrereqChecker
public class SanityChecksPrereqChecker extends SessionCompletePrereqChecker implements PipelinePrereqCheckerI {

}
