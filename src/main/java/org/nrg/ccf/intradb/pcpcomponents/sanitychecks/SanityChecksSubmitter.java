package org.nrg.ccf.intradb.pcpcomponents.sanitychecks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.sanitychecks.components.SanityChecksResults;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sanitychecks.exception.SanityChecksScriptRunException;
import org.nrg.ccf.sanitychecks.services.SanityChecksService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.db.DBAction;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.ActionNameAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.IDAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.JustificationAbsent;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PipelineSubmitter
public class SanityChecksSubmitter implements PipelineSubmitterI {
	
	private static final Logger _logger = LoggerFactory.getLogger(SanityChecksSubmitter.class);
	final SanityChecksService _service = XDAT.getContextService().getBean(SanityChecksService.class);
	
	@Override
	public List<String> getParametersYaml(String projectId, String pipelineId) {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"removeExisting:\n" +
				"    id: removeExisting\n" +
				"    kind: panel.input.switchbox\n" +
				"    label: Remove Existing Assessor?\n" +
				"    description: WARNING: Use with caution.  You will lose any \"Accepted\" status information " +
					"if the current assessor is removed. This should be used " +
					"in rare instances where a change in sanity checks rules leaves a check that is no longer checked but is failing in " +
					"place.  In such an instance, the current assessor needs to be removed so sanity checks can pass.\n" +
				"    value: false\n" +
				"    onText: Yes\n" +
				"    offText: No\n";
		returnList.add(ele);
		return returnList;
	}

	@Override
	public boolean submitJob(final PcpStatusEntity entity, final PcpStatusEntityService entityService,
			final PipelineValidatorI validator, final Map<String, String> parameters, final UserI user) throws PcpSubmitException {
		final XnatMrsessiondata mrSession = XnatMrsessiondata.getXnatMrsessiondatasById(entity.getEntityId(), user, false);
		entity.setStatus(PcpConstants.PcpStatus.RUNNING);
		entity.setStatusTime(new Date());
		if (mrSession==null) {
			final String errMsg = "ERROR:  MR Session not found for entity: " + entity.toString();
			entity.setStatus(PcpConstants.PcpStatus.ERROR);
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(new Date());
			throw new PcpSubmitException(errMsg);
		}
		entityService.update(entity);
		final boolean removeExisting = (parameters.get("removeExisting") != null) ? Boolean.valueOf(parameters.get("removeExisting")) : false;
		if (removeExisting) {
			try {
				removeExistingSanityChecksAssessor(entity, mrSession, user);
			} catch (SanityChecksException e) {
				entity.setStatus(PcpConstants.PcpStatus.ERROR);
				entity.setStatusInfo(e.toString());
				entity.setStatusTime(new Date());
				entityService.update(entity);
				throw new PcpSubmitException(e);
			}
		}
		try {
			SanityChecksResults outcome = _service.runSanityChecks(user, entity.getProject(), mrSession);
			if (!(outcome.getHttpStatus().is5xxServerError() || outcome.getHttpStatus().is4xxClientError())) {
				entity.setStatus(PcpConstants.PcpStatus.COMPLETE);
				entity.setStatusTime(new Date());
			} else {
				entity.setStatus(PcpConstants.PcpStatus.ERROR);
				entity.setStatusInfo(outcome.getRunInfo());
				
			}
		} catch (SanityChecksException | SanityChecksScriptRunException e) {
			final String errMsg = "EXCEPTION:  Sanity checks could not be run for " + entity.toString() + " - " + e.toString();
			entity.setStatus(PcpConstants.PcpStatus.ERROR);
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(new Date());
			throw new PcpSubmitException(errMsg, e);
		}
		entityService.update(entity);
		_logger.debug("Submitted SanityChecks job for entity:  " + entity.toString());
		return true;
		
	}

	private void removeExistingSanityChecksAssessor(PcpStatusEntity entity, XnatMrsessiondata mrSession, UserI user) throws SanityChecksException {
		final org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
	      	cc.addClause("san:SanityChecks.imageSession_ID",mrSession.getId());
	      	cc.addClause("san:SanityChecks.project",entity.getProject());
		final ArrayList<SanSanitychecks> schecks = SanSanitychecks.getSanSanitycheckssByField(cc, user, false);
		if (schecks != null && schecks.size()>0 && schecks.size()<5) {
			for (SanSanitychecks assessor : schecks) {
				if (assessor.getOverall_status().equalsIgnoreCase("ACCEPTED")) {
					throw new SanityChecksException("ERROR:  Cannot auto-remove sanity checks assessor whose status is 'ACCEPTED'");
				}
				PersistentWorkflowI wrk;
				try {
					wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, assessor.getItem(), 
								EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.DELETE_VIA_WEB_SERVICE));
				} catch (JustificationAbsent | ActionNameAbsent | IDAbsent e) {
					throw new SanityChecksException("ERROR:  Error creating workflow", e);
				}
	            EventMetaI ci = wrk.buildEvent();
				try {
					assessor.delete(assessor.getProjectData(), user, false, ci);
					//DBAction.DeleteItem(assessor.getItem().getCurrentDBVersion(), user, ci, false);
				} catch (Exception e) {
					throw new SanityChecksException("ERROR:  Could not remove assessor", e);
				}
			}
		}
		
	}

}
