package org.nrg.ccf.intradb.pcpcomponents.sanitychecks;

import java.util.Date;
import java.util.List;

import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.model.SanSanitychecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

@PipelineValidator
public class SanityChecksValidator implements PipelineValidatorI {

	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user) {
		final org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
	      	cc.addClause("san:SanityChecks.imageSession_ID",statusEntity.getEntityId());
	      	cc.addClause("san:SanityChecks.project",statusEntity.getProject());
		final List<SanSanitychecks> schecks = SanSanitychecks.getSanSanitycheckssByField(cc, user, false);
		// TODO:  Can we make sure created date is greater than submit time here?
		if (schecks.size()>=1) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
			}
			statusEntity.setIssues(false);
			statusEntity.setIssuesInfo("");
			statusEntity.setIssuesTime(new Date());
			for (final SanSanitychecks scheck : schecks) {
				if (scheck.getOverall_status().equalsIgnoreCase("FAILED")) {
					statusEntity.setIssues(true);
					statusEntity.setIssuesInfo(getIssuesInfoText(scheck));
					statusEntity.setIssuesTime(new Date());
					break;
				}
			}
		} else {
			if (statusEntity.getValidated()) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
			}
			statusEntity.setIssues(false);
			statusEntity.setIssuesInfo("");
			statusEntity.setIssuesTime(new Date());
		}
	}

	private String getIssuesInfoText(SanSanitychecks scheck) {
		final StringBuffer sb = new StringBuffer();
		List<XnatImagescandataI> scanData = null;
		for (final SanSanitychecksCheckI sessionCheck : scheck.getSession_sessionChecks_check()) {
			if (sessionCheck.getStatus().equalsIgnoreCase("FAILED")) {
				sb.append("<li>Failed session check (CHECK_ID=" + sessionCheck.getId() + "): " +
						sessionCheck.getDescription() + " [" + sessionCheck.getDiagnosis() + "]</li>\n");
			}
			
		}
		for (final SanSanitychecksScanChecksI scanCheck : scheck.getScans_scanChecks()) {
			if (scanCheck.getStatus().equalsIgnoreCase("FAILED")) {
				final String scanId = scanCheck.getScanId();
				for (final SanSanitychecksScanChecksCheckI check : scanCheck.getCheck()) {
					if (check.getStatus().equalsIgnoreCase("FAILED")) {
						if (scanData == null) {
							scanData = scheck.getImageSessionData().getScans_scan();
						}
						String seriesDesc = "";
						if (scanData != null && scanData.size()>0) {
							for (XnatImagescandataI scan : scanData) {
								if (scan.getId().equals(scanId)) {
									seriesDesc = scan.getSeriesDescription();
								}
							}
						}
						sb.append("<li>Failed scan check (SCAN=" + scanId + ", SERIES_DESC=" + seriesDesc + 
								" CHECK_ID=" + check.getId() +
								"):  " + check.getDescription() + " [" + check.getDiagnosis() + "]</li>\n");
					}
				}
			}
		}
		return (sb.length()>0) ? sb.toString() : "Sanity checks current status is FAILED for this session.";
	}

}
