package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class SessionBuildingPrereqChecker implements PipelinePrereqCheckerI {
	
	final protected List<String> _singletonSubgroups = Arrays.asList(new String[] { "ALL" });

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
        final CriteriaCollection cc = new CriteriaCollection("AND");
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/subject_ID", statusEntity.getEntityId());
        final CriteriaCollection subCC = new CriteriaCollection("OR");
        subCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", statusEntity.getProject());
        subCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", statusEntity.getProject());
        cc.add(subCC);
		final List<XnatMrsessiondata> exps=XnatMrsessiondata.getXnatMrsessiondatasByField(cc, user, false);
		if (exps.size()<1) {
			// Must have experiments
			prereqsNotMet(statusEntity, "Subject has no experiments.");
			return;
		}
		for (final XnatMrsessiondata sess : getFilteredExperimentList(exps, statusEntity, user)) {
			final org.nrg.xft.search.CriteriaCollection sanCC = new CriteriaCollection("AND");
  	      	sanCC.addClause("san:SanityChecks.imageSession_ID",sess.getId());
  	      	sanCC.addClause("san:SanityChecks.project",statusEntity.getProject());
			final List<SanSanitychecks> schecks = SanSanitychecks.getSanSanitycheckssByField(sanCC, user, false);
			if (schecks.size()<1) {
				// Those experiments must have sanity checks
				prereqsNotMet(statusEntity, "Sanity checks not run for one or more experiments.");
				return;
			}
			for (final SanSanitychecks scheck : schecks) {
				if (scheck.getOverall_status() == null || scheck.getOverall_status().equals("FAILED")) {
					// Those experiments must not be in failed status
					prereqsNotMet(statusEntity, "Sanity checks failed for one or more experiments.");
					return;
				}
			}
		}
		if (statusEntity.getPrereqs() == null || !statusEntity.getPrereqs()) {
			statusEntity.setPrereqs(true);
			statusEntity.setPrereqsInfo("");
			statusEntity.setPrereqsTime(new Date());
		}
	}

	protected List<XnatMrsessiondata> getFilteredExperimentList(List<XnatMrsessiondata> exps, PcpStatusEntity statusEntity, UserI user) {
		final List<XnatMrsessiondata> filteredExps = new ArrayList<>();
		final String entityGroup = statusEntity.getSubGroup();
		for (final XnatMrsessiondata sess : exps) {
			if (!sess.getLabel().toLowerCase().contains("_" + entityGroup.toLowerCase() + "_") &&
					!_singletonSubgroups.contains(entityGroup)) {
				continue;
			}
			filteredExps.add(sess);
		}
		return filteredExps;
	}

	protected void prereqsNotMet(final PcpStatusEntity statusEntity, final String reason) {
		final Boolean prereqs = statusEntity.getPrereqs();
		final String prereqsInfo = statusEntity.getPrereqsInfo();
		if (prereqs == null || prereqs || prereqsInfo == null || !prereqsInfo.equals(reason)) {
			statusEntity.setPrereqs(false);
			statusEntity.setPrereqsInfo(reason);
			statusEntity.setPrereqsTime(new Date());
		}
	}

}
