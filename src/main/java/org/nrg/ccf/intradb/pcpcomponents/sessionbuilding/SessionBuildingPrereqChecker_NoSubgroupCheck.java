package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding;

import java.util.Date;
import java.util.List;

import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class SessionBuildingPrereqChecker_NoSubgroupCheck extends SessionBuildingPrereqChecker {

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
        final CriteriaCollection cc = new CriteriaCollection("AND");
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/subject_ID", statusEntity.getEntityId());
        final CriteriaCollection subCC = new CriteriaCollection("OR");
        subCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", statusEntity.getProject());
        subCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", statusEntity.getProject());
        cc.add(subCC);
		final List<XnatMrsessiondata> exps=XnatMrsessiondata.getXnatMrsessiondatasByField(cc, user, false);
		if (exps.size()<1) {
			// Must have experiments
			prereqsNotMet(statusEntity, "Subject has no experiments.");
			return;
		}
		for (final XnatMrsessiondata sess : exps) {
			// Here's the change for this version (SessionBuildingPrereqChecker should maybe be LifeSpan specific????)
			//if (!sess.getLabel().toLowerCase().contains("_" + statusEntity.getSubGroup().toLowerCase() + "_")) {
			//	continue;
			//}
			final org.nrg.xft.search.CriteriaCollection sanCC = new CriteriaCollection("AND");
  	      	sanCC.addClause("san:SanityChecks.imageSession_ID",sess.getId());
  	      	sanCC.addClause("san:SanityChecks.project",statusEntity.getProject());
			final List<SanSanitychecks> schecks = SanSanitychecks.getSanSanitycheckssByField(sanCC, user, false);
			if (schecks.size()<1) {
				// Those experiments must have sanity checks
				prereqsNotMet(statusEntity, "Sanity checks not run for one or more experiments.");
				return;
			}
			for (final SanSanitychecks scheck : schecks) {
				if (scheck.getOverall_status() == null || scheck.getOverall_status().equals("FAILED")) {
					// Those experiments must not be in failed status
					prereqsNotMet(statusEntity, "Sanity checks failed for one or more experiments.");
					return;
				}
			}
		}
		if (statusEntity.getPrereqs() == null || !statusEntity.getPrereqs()) {
			statusEntity.setPrereqs(true);
			statusEntity.setPrereqsInfo("");
			statusEntity.setPrereqsTime(new Date());
		}
	}

}
