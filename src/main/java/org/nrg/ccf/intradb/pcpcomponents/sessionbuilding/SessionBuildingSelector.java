package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineSelectorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcp.services.PcpStatusUpdateService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PipelineSelector
public class SessionBuildingSelector implements PipelineSelectorI {
	
	private static final Logger _logger = LoggerFactory.getLogger(SessionBuildingSelector.class);
	final RuntimeMXBean _mxBean = ManagementFactory.getRuntimeMXBean();
	final PcpStatusUpdateService _statusUpdateService = XDAT.getContextService().getBean(PcpStatusUpdateService.class);
	
	protected final static String[] DEFAULT_GROUPS = new String[] {"ALL"}; 
	protected final static String[] CCF_HCA_GROUPS = new String[] {"V1","V2"}; 
	protected final static String[] CCF_HCD_GROUPS = new String[] {"V1","V2","V3"}; 
	protected final static String[] AABC_GROUPS = new String[] {"V1","V2","V3","V4"}; 
	
	public SessionBuildingSelector() {
		super();
		//groupList.clear();
	}
	
	@Override
	public List<PcpStatusEntity> getStatusEntities(PcpStatusEntityService _statusEntityService, String projectId,
			String pipelineId, UserI user) {
		List<String> groupList = getGroupList(projectId);
		final List<PcpStatusEntity> returnList = new ArrayList<>();
		final List<PcpStatusEntity> unmatchedEntities = _statusEntityService.getProjectPipelineStatus(projectId, pipelineId);
        final CriteriaCollection cc = new CriteriaCollection("OR");
        cc.addClause(XnatSubjectdata.SCHEMA_ELEMENT_NAME + "/project", projectId);
        cc.addClause(XnatSubjectdata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", projectId);
		final List<XnatSubjectdata> subjects = XnatSubjectdata.getXnatSubjectdatasByField(cc, user, false);
		final CriteriaCollection expCC = new CriteriaCollection("OR");
		expCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", projectId);
		expCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", projectId);
		final List<XnatMrsessiondata> mrSessions = XnatMrsessiondata.getXnatMrsessiondatasByField(expCC, user, false);
		for (final XnatSubjectdata subj : subjects) {
			for (final String group : groupList) {
				if (!hasMatchingMrsession(subj,mrSessions,group)) {
					continue;
				}
				final Iterator<PcpStatusEntity> i = unmatchedEntities.iterator();
				boolean hasMatch = false;
				while (i.hasNext()) {
					final PcpStatusEntity entity = i.next();
					if ((entity.getEntityId().equals(subj.getId()) || entity.getEntityLabel().equals(subj.getLabel())) &&
							entity.getSubGroup().equals(group)) {
						returnList.add(entity);
						i.remove();
						if (entity.getEntityId().equals(subj.getId())) {
							if (!entity.getEntityLabel().equals(subj.getLabel())) {
								_logger.warn("WARNING:  Subject Label has changed for session " + subj.getId() +
										" - updating StatusEntityID (OLDLABEL=" + entity.getEntityLabel() +  ", NEWLABEL=" + subj.getLabel() + ")");
								entity.setEntityLabel(subj.getLabel());
								_statusEntityService.update(entity);
							}
							if (entity.getStatus().equals(PcpConstants.PcpStatus.REMOVED.toString())) {
								entity.setStatus(PcpConstants.PcpStatus.UNKNOWN);
								entity.setStatusTime(new Date());
								_statusEntityService.update(entity);
							}
						} else {
							_logger.warn("WARNING:  Experiment ID has changed for session " + subj.getLabel() +
									" - updating StatusEntityID (OLDID=" + entity.getId() +  ", NEWID=" + subj.getId() + ")");
							entity.setEntityId(subj.getId());
							if (entity.getStatus().equals(PcpConstants.PcpStatus.REMOVED.toString())) {
								entity.setStatus(PcpConstants.PcpStatus.UNKNOWN);
								entity.setStatusTime(new Date());
							}
							_statusEntityService.update(entity);
						}
						hasMatch = true;
					}
				}
				if (hasMatch) {
					continue;
				}
				try {
					final PcpStatusEntity newEntity = new PcpStatusEntity();
					newEntity.setProject(projectId);
					newEntity.setPipeline(pipelineId);
					newEntity.setEntityType(XnatSubjectdata.SCHEMA_ELEMENT_NAME);
					newEntity.setEntityId(subj.getId());
					newEntity.setEntityLabel(subj.getIdentifier(projectId));
					newEntity.setSubGroup(group);
					newEntity.setStatus(PcpConstants.PcpStatus.NOT_SUBMITTED);
					newEntity.setStatusTime(new Date());
					final PcpStatusEntity createdEntity = _statusEntityService.create(newEntity);
					returnList.add(createdEntity);
					_statusUpdateService.refreshStatusEntityCheckAndValidate(createdEntity, user, false);
				} catch (Exception e) {
					// Could be an overlooked constraint violation or something.  We need to continue and try other entities.
					_logger.error("ERROR:  Exception thrown adding status entity", e);
				}
			}
		}
		//
		// Update status for sessions that no longer exist.
		//
		// CCF-326:  For some reason, this code was often executing at Tomcat startup.  It seems like this check 
		// may be executing before the XnatSubjectdatas.getXnatSubjectdatasByField method is able to 
		// return values.  Let's just skip this code near Tomcat startup to prevent it messing up status when 
		// the run is unable to access the session list.  NOTE:  This fix is probably redundant, because 
		// there's a similar check in PcpStatusUpdate (the scheduled task), but just in case....
		final Long upTime = (_mxBean!=null) ? _mxBean.getUptime() : Long.MAX_VALUE;
		if (upTime>PcpConfigConstants.UP_TIME_WAIT) {
			for (PcpStatusEntity statusEntity : unmatchedEntities) {
				if (statusEntity.getStatus() != null && statusEntity.getStatus().equals(PcpConstants.PcpStatus.REMOVED.toString())) {
					continue;
				}
				statusEntity.setStatus(PcpConstants.PcpStatus.REMOVED);
				statusEntity.setStatusTime(new Date());
				_statusEntityService.update(statusEntity);
			}
		} else {
			_logger.debug("Skipping step to set status to REMOVED due to insufficient Tomcat uptime.  (UPTIME=" +
					upTime + "ms, UNMATCHED_ENTITIES=" + unmatchedEntities.size() + ")");
		}
		return returnList;
	}

	protected boolean hasMatchingMrsession(XnatSubjectdata subj, List<XnatMrsessiondata> mrSessions, String group) {
		Iterator<XnatMrsessiondata> i = mrSessions.iterator();
		final String labelCheck = "_" + group + "_";
		while (i.hasNext()) {
			final XnatMrsessiondata session = i.next();
			if (session.getSubjectId().equals(subj.getId())) {
				final String label = session.getLabel();
				if (label != null && label.contains(labelCheck)) {
					i.remove();
					return true;
				}
			}
		}
		return false;
	}

	// TODO:  This really should be getting pulled from the ReleaseRules for the project (from configuration).
	protected List<String> getGroupList(String projectId) {
		switch(projectId) {
		case "CCF_HCA_ITK": return Arrays.asList(CCF_HCA_GROUPS);
		case "CCF_HCD_ITK": return Arrays.asList(CCF_HCD_GROUPS);
		case "AABC_WU_ITK": return Arrays.asList(AABC_GROUPS);
		case "AABC_MGH_ITK": return Arrays.asList(AABC_GROUPS);
		case "AABC_UMN_ITK": return Arrays.asList(AABC_GROUPS);
		case "AABC_UCLA_ITK": return Arrays.asList(AABC_GROUPS);
		default: return Arrays.asList(DEFAULT_GROUPS);
		}
	}
	
}
