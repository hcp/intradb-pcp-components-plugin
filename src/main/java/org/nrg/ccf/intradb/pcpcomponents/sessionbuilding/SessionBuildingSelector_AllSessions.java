package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding;

import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingSelector;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

@PipelineSelector
public class SessionBuildingSelector_AllSessions extends SessionBuildingSelector {
	
	public SessionBuildingSelector_AllSessions() {
		super();
	}
	
	@Override
	protected boolean hasMatchingMrsession(XnatSubjectdata subj, List<XnatMrsessiondata> mrSessions, String group) {
		return (mrSessions!=null && mrSessions.size()>0);
	}
	
	@Override
	protected List<String> getGroupList(String projectId) {
		return Arrays.asList(DEFAULT_GROUPS);
	}

}
