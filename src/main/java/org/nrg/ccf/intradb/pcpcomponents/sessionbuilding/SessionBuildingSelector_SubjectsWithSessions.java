package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding;

import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingSelector;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

@PipelineSelector
public class SessionBuildingSelector_SubjectsWithSessions extends SessionBuildingSelector {
	
	public SessionBuildingSelector_SubjectsWithSessions() {
		super();
	}
	
	@Override
	protected boolean hasMatchingMrsession(XnatSubjectdata subj, List<XnatMrsessiondata> mrSessions, String group) {
		for (final XnatMrsessiondata session  : mrSessions) {
			if (session.getSubjectId().equals(subj.getId())) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected List<String> getGroupList(String projectId) {
		return Arrays.asList(DEFAULT_GROUPS);
	}


}
