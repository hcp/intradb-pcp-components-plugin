

package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.action.ClientException;
import org.nrg.ccf.common.utilities.components.NodeUtils;
import org.nrg.ccf.common.utilities.utils.MiscUtils;
import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PcpStatusEntityI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.sessionbuilding.enums.ProcessingType;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.exception.SessionBuildingException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseFileHandlerI;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.interfaces.CcfResourceGeneratorI;
import org.nrg.ccf.sessionbuilding.pojo.RulesProcessingResults;
import org.nrg.ccf.sessionbuilding.preferences.CcfSessionBuildingPreferences;
import org.nrg.ccf.sessionbuilding.runner.BuildSessionRunner;
import org.nrg.ccf.sessionbuilding.runner.result.SessionBuilderRunResults;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.ccf.sessionbuilding.utils.CcfSessionBuildingUtils;
import org.nrg.ccf.sessionbuilding.utils.CcfUidUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.preferences.SiteConfigPreferences;
import org.nrg.xdat.security.helpers.Permissions;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXReader;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXWriter;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineSubmitter
public class SessionBuildingSubmitter implements PipelineSubmitterI {
	
	private final CcfSessionBuildingPreferences _sbPref = XDAT.getContextService().getBean(CcfSessionBuildingPreferences.class);
	private final SiteConfigPreferences _sitePreferences = XDAT.getContextService().getBean(SiteConfigPreferences.class);
	private final NodeUtils _nodeUtils = XDAT.getContextService().getBean(NodeUtils.class);
	private final Map<String,XnatProjectdata> projectMap = new HashMap<>();
	private static final String SELECTION_CRITERIA_PARAM = "selectionCriteria";
	private static final String SESSION_LABEL_PARAM = "sessionLabel";
	private final Gson gson = new GsonBuilder().create();
	
	@Override
	public List<String> getParametersYaml(String projectId, String pipelineId) {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"errorOverride:\n" +
				"    id: errorOverride\n" +
				"    kind: panel.input.switchbox\n" +
				"    label: Error Override?\n" +
				"    value: false\n" +
				"    onText: Yes\n" +
				"    offText: No\n" +
				/*
				"    after:\n" +
				"        script:\n" +
				"            tag: 'script'\n" +
				"            content: >\n" +
				"                 $('#errorOverride').change(function() {\n" +
				"                 	var errorOverride = $('#errorOverride').val();\n" +
				"                 	$('#overrideAsWarning').prop('checked', !(errorOverride == \"true\"));\n" +
				"                 });\n" +
				*/
				"overrideAsWarning:\n" +
				"    id: overrideAsWarning\n" +
				"    kind: panel.input.switchbox\n" +
				"    label: Treat Override As Warning?\n" +
				"    description: 'Indicates that override is not to be treated as an error condition and the session should proceed as normal (i.e. without QC failureflags, etc.).'\n" +
				"    value: false\n" +
				"    onText: Yes\n" +
				"    offText: No\n" +
				"overwriteExisting:\n" +
				"    id: overwriteExisting\n" +
				"    kind: panel.input.switchbox\n" +
				"    label: Overwrite Existing Records?\n" +
				"    value: false\n" +
				"    onText: Yes\n" +
				"    offText: No\n";
		returnList.add(ele);
		return returnList;
	}

	@Override
	public boolean submitJob(PcpStatusEntity entity, PcpStatusEntityService entityService, PipelineValidatorI validator, Map<String, String> parameters, UserI user) throws PcpSubmitException {
		
		final String projectId = entity.getProject();
		final String sbProjectId = _sbPref.getCcfSessionBuildingBuildProject(projectId);
		parameters.put(SELECTION_CRITERIA_PARAM, entity.getSubGroup());
		entity.setStatus(PcpConstants.PcpStatus.RUNNING);
		final Date statusTime = new Date();
		entity.setStatusInfo("Status set to running at (TIME=" + statusTime + ", NODE=" + _nodeUtils.getXnatNode() + ")");
		entity.setStatusTime(statusTime);
		entityService.update(entity);
		try {
			doBuildSession(entity, sbProjectId, parameters, user);
			entity.setStatus(PcpConstants.PcpStatus.COMPLETE);
			entity.setStatusTime(new Date());
			entityService.update(entity);
		} catch (PcpSubmitException e) {
			final String stackTrace = ExceptionUtils.getFullStackTrace(e);
			final String rulesErrorDisplay = getRulesErrorDisplay(stackTrace);
			final String exceptionDetail = getExceptionDetail(stackTrace);
			final String errMsg;
			if (rulesErrorDisplay != null) {
					errMsg = "EXCEPTION:  <br/><br/>Session building could not be run for " + entity.toString() + 
						"<br/><br/>RULES ERROR VIOLATIONS:  <br/><br/> " + rulesErrorDisplay + 
						"<br/><br/>EXCEPTION TRACE:<br/><br/>" + stackTrace + "<br/><br/>";
			} else if (exceptionDetail != null) {
					errMsg = "EXCEPTION:  <br/><br/>Session building could not be run for " + entity.toString() + 
						"<br/><br/>EXCEPTION DETAIL:  <br/><br/><b>" + exceptionDetail + "</b>" + 
						"<br/><br/>EXCEPTION TRACE:<br/><br/>" + stackTrace + "<br/><br/>";
			} else {
					errMsg = "EXCEPTION:  <br/><br/>Session building could not be run for " + entity.toString() + 
						"<br/><br/>EXCEPTION TRACE:<br/><br/>" + stackTrace + "<br/><br/>"
					;
			} 
			entity.setStatus(PcpConstants.PcpStatus.ERROR);
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(new Date());
			entityService.update(entity);
			throw e;
		}
		return true;
		
	}

	private String getExceptionDetail(String stackTrace) {
		if (stackTrace.contains("ClientException:")) {
			String edited = stackTrace.substring(stackTrace.lastIndexOf("ClientException:"));
			edited = edited.substring(0,edited.indexOf("at org."));
			edited = edited.replace("\u003d", "=");
			return edited;
		}
		return null;
	}

	private String getRulesErrorDisplay(final String stackTrace) {
		if (stackTrace.contains("ReleaseRulesException: [")) {
			String edited = stackTrace.substring(stackTrace.lastIndexOf("["));
			edited = edited.substring(0,edited.indexOf("]")+1);
			edited = edited.replace("\u003d", "=");
			try {
				final List<String> exMap = gson.fromJson(edited, new TypeToken<List<String>>(){}.getType());
				if (exMap != null && exMap.size()>0) {
					StringBuffer sb = new StringBuffer();
					sb.append("<ul>");
					for (final String s : exMap) {
						sb.append("<li>");
						sb.append(s);
						sb.append("</li>");
					}
					sb.append("</ul>");
					return sb.toString();
				}
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}

	private XnatProjectdata getProject(final String projectId, final UserI user) {
		if (!projectMap.containsKey(projectId)) {
			projectMap.put(projectId, XnatProjectdata.getProjectByIDorAlias(projectId, user, false));
		}
		return projectMap.get(projectId);
	}
	
	private CcfReleaseRulesI getReleaseRules(String projectId) throws PcpSubmitException {
		final String sbClassStr = _sbPref.getCcfSessionBuildingRulesClass(projectId);
		Object releaseRulesO;
		try {
			releaseRulesO = Class.forName(sbClassStr).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new PcpSubmitException("Exception thrown creating instance of release rules class", e);
		}
		if (releaseRulesO instanceof CcfReleaseRulesI) {
			return (CcfReleaseRulesI)releaseRulesO;
		} else {
			throw new PcpSubmitException("Returned release rules object was not a valid.");
		}
	}
	
	private CcfReleaseFileHandlerI getFileHandler(String projectId, List<XnatMrsessiondata> srcSessions,
			XnatMrsessiondata destSession, CcfReleaseRulesI releaseRules, UserI user) throws PcpSubmitException {
		final String fhClassStr = _sbPref.getCcfSessionBuildingFileHandler(projectId);
		try {
			return	(CcfReleaseFileHandlerI) Class.forName(fhClassStr)
					.getDeclaredConstructor(List.class, XnatImagesessiondata.class, CcfReleaseRulesI.class, UserI.class)
					.newInstance(srcSessions, destSession, releaseRules, user);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			throw new PcpSubmitException("Could not instantiate file handler.", e);
		}
	}
	
	private CcfResourceGeneratorI getResourceGenerator(String projectId, List<XnatMrsessiondata> srcSessions,
			XnatMrsessiondata destSession, CcfReleaseRulesI releaseRules, UserI user) throws PcpSubmitException {
		final String rgClassStr = _sbPref.getCcfSessionBuildingResourceGen(projectId);
		try {
			return (CcfResourceGeneratorI) Class.forName(rgClassStr)
					.getDeclaredConstructor(List.class, XnatImagesessiondata.class, CcfReleaseRulesI.class, UserI.class)
					.newInstance(srcSessions, destSession, releaseRules, user);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			throw new PcpSubmitException("Could not instantiate resource generator.", e);
		}
	}

	private void doBuildSession(PcpStatusEntityI entity, final String combSessProjId,
			final Map<String, String> params, final UserI user) throws PcpSubmitException {
		
		final String projectId = entity.getProject();
		final String subjectId = entity.getEntityLabel();
		 
		final XnatProjectdata proj=getProject(projectId, user);
		if (proj == null) {
			throw new PcpSubmitException("Could not retrieve project.");
		}
		final CcfReleaseRulesI releaseRules = getReleaseRules(projectId);
		XnatSubjectdata subj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjectId, user, true);
		if (subj == null) {
			subj = XnatSubjectdata.getXnatSubjectdatasById(subjectId, user, true);
		}
		if (subj == null) {
			throw new PcpSubmitException("Subject not specified or does not exist (SUBJECT_ID=" + subjectId + ")");
		}
		
		try {
			if (!Permissions.canRead(user,subj)) {
				throw new PcpSubmitException("Specified user account has insufficient privileges for subjects in this project.");
			}
		} catch (Exception e) {
			log.error("SERVER EXCEPTION:  " + ExceptionUtils.getStackTrace(e));
			throw new PcpSubmitException("Server threw exception:  " + e.toString());
		}
		
		try {
			if (!Permissions.canRead(user,  proj)) {
				throw new PcpSubmitException("The destination project for this session either does not exist or your account doesn't have access to it (PROJECT=" +
						combSessProjId + ").");
			}
		} catch (Exception e1) {
			// Do nothing, we'll just try to continue.  Process will fail if user doesn't have appropriate access.
		}
		
		// First process scans to set scan values
		final RulesProcessingResults rpResults = processScanValues(entity, proj, subj, releaseRules, params, user);
		if (!rpResults.getSuccess()) {
			entity.setStatusInfo("<h3>Warnings were encountered during building of this session:</h3><br>" + listToHtml(rpResults.getWarningList()));
			entity.setStatusTime(new Date());
		}
		// Then build session
		try {
			// Need to re-pull subject to update its experiment list, otherwise it will not include the scan updates
			// from the processing above.
		    subj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjectId, user, true);
			continueBuildingSession(entity, proj, subj, params, releaseRules, combSessProjId, rpResults, user);
		} catch (PcpSubmitException e) {
			throw e;
		} catch (Exception e) {
			log.error("SERVER EXCEPTION:  " + ExceptionUtils.getStackTrace(e));
			throw new PcpSubmitException("Server threw exception: ", e);
		}

	}

	private String listToHtml(List<String> returnList) {
		final StringBuilder sb = new StringBuilder("<ul>");
		for (final String str : returnList) {
			sb.append("<li>").append(str).append("</li>");
		}
		sb.append("</ul>");
		return sb.toString();
	}

	private RulesProcessingResults processScanValues(PcpStatusEntityI entity, XnatProjectdata proj, XnatSubjectdata subj, CcfReleaseRulesI releaseRules,
			Map<String, String> params, UserI user) throws PcpSubmitException {
		
		final List<XnatMrsessiondata> sessionList = new ArrayList<>();
		final CriteriaCollection criteria=new CriteriaCollection("AND");
		final CriteriaCollection cc= new CriteriaCollection("OR");
		//// Currently we'll only retrieve MR session types
		cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME+"/project", proj.getId());
		cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME+"/sharing/share/project", proj.getId());
		criteria.addClause(cc);
		criteria.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/subject_ID", subj.getId());
		sessionList.addAll(XnatMrsessiondata.getXnatMrsessiondatasByField(criteria, user, false));
		final Iterator<XnatMrsessiondata> i = sessionList.iterator();
		while (i.hasNext()) {
			final XnatImagesessiondata session = i.next();
			if (!(session instanceof XnatMrsessiondata)) {
				i.remove();
			}
		}
		if (sessionList.size()<1) {
			final String errMsg = "ERROR:  No MR sessions found for this subject (PROJECT=" + proj.getId() + ",SUBJECT=" + subj.getLabel() + ").  " + 
					"Session building currently supports only MR session data.";
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(new Date());
			throw new PcpSubmitException(errMsg);
		}
		final List<XnatMrsessiondata> filteredList = new ArrayList<>();
		try {
			filteredList.addAll((Collection<XnatMrsessiondata>) releaseRules.filterExptList(sessionList, params, user));
		} catch (ReleaseRulesException e) {
			log.error("ERROR",e);
		}
		return processSessionList(entity, proj, subj, filteredList, params, releaseRules, user);
		
	}

	private RulesProcessingResults processSessionList(PcpStatusEntityI entity, XnatProjectdata proj, XnatSubjectdata subj, List<XnatMrsessiondata> sessionList,
			Map<String, String> params, CcfReleaseRulesI releaseRules, UserI user) throws PcpSubmitException {
		
		// First, create ordered list of scans with their associated sessions
		final List<XnatMrsessiondata> subjectSessions = sessionList;
		final List<XnatMrscandata> subjectScans = getOrderedScanList(sessionList, releaseRules);
		//log.debug("subjectSessions found=" + subjectSessions.size());
		//log.debug("subjectScans found=" + subjectScans.size());
		try {
			return processScans(entity, proj, subj, params, subjectScans, subjectSessions, releaseRules, user);
		} catch (Exception e) {
			log.debug(ExceptionUtils.getFullStackTrace(e));
			throw new PcpSubmitException("Exception thrown processing scans:", e);
		}
	}
	
	private RulesProcessingResults processScans(PcpStatusEntityI entity, XnatProjectdata proj, XnatSubjectdata subj, 
				Map<String, String> params, List<XnatMrscandata> subjectScans,
					List<XnatMrsessiondata> subjectSessions, CcfReleaseRulesI releaseRules, UserI user) throws Exception {
		final RulesProcessingResults rpResults = new RulesProcessingResults();
		final Boolean overrideStatus = params.containsKey("errorOverride") && params.get("errorOverride").matches("(?i)^[TY].*$");
		String selectionCriteria = params.get("selectionCriteria");
		if (selectionCriteria == null || selectionCriteria.trim().length()<1) {
			List<String> criteriaList = releaseRules.getSelectionCriteria();
			if (criteriaList.size()==1) {
				selectionCriteria = criteriaList.get(0);
			}
		}
		try {
			final List<String> warningList;
			if (overrideStatus) {
				warningList = releaseRules.applyRulesToScans(subjectScans,subjectSessions,params,true);
			} else {
				warningList = releaseRules.applyRulesToScans(subjectScans,subjectSessions,params,false);
			}
			CcfSessionBuildingUtils.persistRulesErrors(proj, subj, selectionCriteria, releaseRules, warningList);
			rpResults.setSuccess(true);
			rpResults.setStatus((warningList.size()>0) ? MiscUtils.listToHtml(warningList) :
					"Release target processing complete.  No warnings or errors reported.");
			rpResults.setWarningList(warningList);
		} catch (ReleaseRulesException e) {
			final StringBuilder sb = new StringBuilder("<h3>Rules Error Thrown During Processing</h3><br>");
			sb.append("<b>Error Override Requested?:</b> ").append(overrideStatus.toString()).append("<br>");
			sb.append("<b>Exception Summary:</b> ").append(e.toString()).append("<br>");
			entity.setStatusInfo(sb.toString());
			entity.setStatusTime(new Date());
			CcfSessionBuildingUtils.persistRulesErrors(proj, subj, selectionCriteria, releaseRules, e.getErrorList());
			throw e;
		} catch (Exception e) {
			log.error("RulesProcessor threw exception:  ");
			log.error(ExceptionUtils.getStackTrace(e));
			throw e;
		}
	 	final String dbLabel = params.containsKey(SESSION_LABEL_PARAM) ? params.get(SESSION_LABEL_PARAM) : releaseRules.getDefaultSessionLabel(proj, subj, params);
	 	final String dataRelease = params.containsKey("dataRelease") ? params.get("dataRelease") : "Not Specified";
		for (final XnatMrscandata scan : subjectScans) {
			scan.setDatarelease(dataRelease);
			scan.setDbsession(dbLabel);
			final PersistentWorkflowI wrk;
			try {
				wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, scan.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.MODIFY_VIA_WEB_SERVICE, null, null));
				final EventMetaI ci = wrk.buildEvent();
				if (SaveItemHelper.authorizedSave(scan,user,false,true,ci)) {
					//log.debug("SAVED SCAN (SCAN=" + scan.getId() + ", " + scan.getSeriesDescription() + ")");
					PersistentWorkflowUtils.complete(wrk, ci);
				} else {
					PersistentWorkflowUtils.fail(wrk,ci);
					//log.debug("WARNING:  Failed to save scan (SCAN=" + scan.getId() + ", " + scan.getSeriesDescription() + ")." +
					//     "  This likely means there were no changes.");
				}
			} catch (Exception e) {
				log.error("SERVER EXCEPTION:  " + ExceptionUtils.getStackTrace(e));
				throw(e);
			}
		}
		return rpResults;
	}

	private void continueBuildingSession(PcpStatusEntityI entity, XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params,
					CcfReleaseRulesI releaseRules, String combSessProjId, RulesProcessingResults rpResults, UserI user) throws PcpSubmitException {
		
		try {
			if (!Permissions.canEdit(user, proj)) {
				final String errMsg = "ERROR:  Your account does not have write permission on the destination project (PROJECT=" +
						combSessProjId + ").";
				entity.setStatusInfo(errMsg);
				entity.setStatusTime(new Date());
				throw new PcpSubmitException(errMsg);
			}
		} catch (Exception e1) {
			// Do nothing, we'll just try to build the session.  It will fail if the user doesn't have access.
		}
		doCurrentBuild(entity, proj, subj, params, releaseRules, combSessProjId, rpResults, user);
	}
	
	private void doCurrentBuild(PcpStatusEntityI entity, XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, CcfReleaseRulesI releaseRules, String combSessProjId, 
									RulesProcessingResults rpResults, UserI user) throws PcpSubmitException {
	
		final String validationInfo = entity.getStatusInfo();
		final List<XnatMrsessiondata> expts = new ArrayList<>(); 
		log.debug("doCurrentBuild - " + entity + "  --  " + params);
		try {
			ArrayList<XnatMrsessiondata> subjSessions = new ArrayList<>();
			for (final XnatSubjectassessordata assessor : subj.getExperiments_experiment(XnatMrsessiondata.SCHEMA_ELEMENT_NAME)) {
				subjSessions.add((XnatMrsessiondata)assessor);
			}
			expts.addAll(releaseRules.filterExptList(getProjectExpts(proj, subjSessions),params,user));
		} catch (ReleaseRulesException e) {
			final String errMsg = "Could not retrieve sessions from release rules: " + e.toString();
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(new Date());
			throw new PcpSubmitException(errMsg);
		}
		log.debug("Experiments used in build - " + expts.size());
		XnatMrsessiondata combSess;
		try {
			combSess = buildAndReturnCombinedSession(entity, proj, subj, params, releaseRules, expts, user);
			if (combSess == null) {
				final String errMsg = "ERROR: Could not build combined session - null session returned";
				entity.setStatusInfo(errMsg);
				entity.setStatusTime(new Date());
				throw new PcpSubmitException(errMsg);
			}
		} catch (Exception e) {
			throw new PcpSubmitException("ERROR: Could not build session: " + e.toString());
		}
		final CcfReleaseFileHandlerI fileHandler = getFileHandler(proj.getId(),expts,combSess,releaseRules,user);
		final CcfResourceGeneratorI resourceGenerator = getResourceGenerator(proj.getId(),expts,combSess,releaseRules,user);
		
		combSess.setProject(combSessProjId);
		final BuildSessionRunner runner = new BuildSessionRunner(subj, combSess, user, fileHandler, resourceGenerator, params, 
				_sitePreferences.getSiteUrl(), ProcessingType.BUILD_SESSION, rpResults);
		final SessionBuilderRunResults runResults = runner.runWait(false);
		final StringBuilder sb = new StringBuilder();
		sb.append("<h3>Session Building Results (SESSION=").append(combSess.getLabel()).append(")</h3><br>");
		sb.append("<b>Build Successful: </b>").append(runResults.getSuccess()).append("<br><br>");
		sb.append("<b>Build Information </b><br><br>").append(runResults.getStatus()).append("<br><br>");
		sb.append("<b>Session Validation Information (Build Warnings) </b><br><br>").append(validationInfo).append("<br><br>");
		entity.setStatusInfo(sb.toString());
		entity.setStatusTime(new Date());
	}
	
	private XnatMrsessiondata buildAndReturnCombinedSession(PcpStatusEntityI entity, XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, CcfReleaseRulesI releaseRules, List<XnatMrsessiondata> expts, UserI user) throws Exception {
		final XnatMrsessiondata combSess = new XnatMrsessiondata();
		if (params.containsKey("exptID")) {
			combSess.setId(params.get("exptID"));
		}
		if (params.containsKey("date")) {
			final String dateStr = params.get("date");
			try {
				parseAndSetDate(dateStr, combSess);
			} catch (Exception e) {
				combSess.setDate(null);

			}
		} else {
			combSess.setDate(null);
		}
		combSess.setDcmpatientname(CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj));
		// This doesn't seem to be used
		//combSess.setDcmpatientid(combSessLabel);
		combSess.setDcmpatientbirthdate(null);
		
		boolean anyRelease = false;
		// Per meeting 2012-12-18, ordering all scans by acquisition time
		final List<XnatMrscandata> scanList = getOrderedScanList(expts, releaseRules);
		XnatMrsessiondata prevSess = null;
		String intraLabel = null;
		for (final XnatMrscandata intraScan : scanList) {
			final XnatMrsessiondata intraSess = getScanSession(intraScan, expts);
			if (intraLabel == null) {
				intraLabel = intraScan.getDbsession();
			}
			if (!(intraSess == null || intraSess.equals(prevSess))) {
				prevSess = intraSess;
				if (combSess.getAcquisitionSite() == null
						|| combSess.getAcquisitionSite().length() < 1) {
					combSess.setAcquisitionSite((String) getCombinedAttr(
							combSess.getAcquisitionSite(),
							intraSess.getAcquisitionSite(), "Acqusition Site"));
				}
				if (combSess.getScanner() == null
						|| combSess.getScanner().length() < 1) {
					combSess.setScanner((String) getCombinedAttr(
							combSess.getScanner(), intraSess.getScanner(),
							"Scanner"));
				}
				if (combSess.getOperator() == null
						|| combSess.getOperator().length() < 1) {
					combSess.setOperator((String) getCombinedAttr(
							combSess.getOperator(), intraSess.getOperator(),
							"Operator"));
				}
			}

			// TO BE SET LATER
			// combSess.setId()
			// whole data release
			// Should vary - leave blank
			// combSess.setDcmaccessionnumber()
			// combSess.setDcmpatientid()
			// combSess.setDcmpatientname()
			// combSess.setTime()
			// Not typically set - leave blank
			// combSess.setDuration()
			// HIPAA: Leave blank
			// combSess.setDcmpatientbirthdate()

			// continue only if targeted for export
			final Boolean isTarget = intraScan.getTargetforrelease();
			final Integer isOverride;
			if (!releaseRules.isStructuralScan(intraScan) || intraScan.getReleaseoverride()==null) {
				isOverride = 0;
			} else {
				isOverride = intraScan.getReleaseoverride();
			}
			// Override is currently only applicable for rated structural scans 
			// Newly added conditional for ACP project 2024/08/15, where we're releasing all structurals 
			if (!isAlternateScan(intraScan)) {
			 if (!((isOverride>0) || (isOverride>=0 && (isTarget!=null && isTarget)))) {
				continue;
			 }
			}
			anyRelease = true;
			
			// Need to remove file objects from dbScan to permit upload (references Intradb file path)
			// These two lines are important, otherwise the scan will be MOVED.
			final XnatMrscandata dbScan = new XnatMrscandata((ItemI)intraScan.getItem().clone());
			if (dbScan.getFile().size()>0) {
				while (true) {
					try { 
						dbScan.removeFile(0);
					} catch (Exception e) {
						break;
					}
				}
			}
			try {
				dbScan.setId(dbScan.getDbid());
			} catch (NumberFormatException e) {
				throw new ClientException("ERROR:  Couldn't generate scan ID", e);
			}
			dbScan.setImageSessionId(combSess.getId());
			if (releaseRules.requireScanUidAnonymization()) {
				dbScan.setUid(CcfUidUtils.anonymizeUid(dbScan.getUid()));
			}
			// This seems to no longer be getting set
			//dbScan.setDatarelease(dataRelease);
			dbScan.setReleasecountscan(null);
			dbScan.setReleasecountscanoverride(null);
			dbScan.setTargetforrelease(null);
			dbScan.setReleaseoverride(null);
			dbScan.setDbsession(null);
			dbScan.setDbid(null);
			// Keep Intradb description for now
			final String idbDesc = dbScan.getSeriesDescription();
			dbScan.setSeriesDescription(dbScan.getDbdesc());
			dbScan.setDbdesc(idbDesc);
			dbScan.setType(dbScan.getDbtype());
			dbScan.setDbtype(null);
			// Per meeting, for now we won't populate scan notes
			dbScan.setNote(null);
			// We need to exclude all hidden fields, so let's convert to XML and back. 
			// TODO:  This could certainly be done more efficiently by removing the hidden fields from the item directly.
			final StringWriter sw = new StringWriter();
			SAXWriter writer = null;
			// I don't know why, but an exception is occasionally thrown getting the SAXWriter.  It's usually successful
			// on reruns of the same session.  Just putting it in a loop to try a few times before giving up.
			for (int i=0; i<5; i++) {
				try {
					writer = new SAXWriter(sw,false);
					break;
				} catch (Exception e) {
					log.debug("NOTE:  Exception thrown obtaining SAXWriter:  ", e);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException intE) {
						// Do nothing
					}
				}
			}
			if (writer == null) {
				throw new SessionBuildingException("Couldn't obtain SAXWriter");
			}
			writer.setWriteHiddenFields(false);
			writer.write(dbScan.getItem());
			final SAXReader reader = new SAXReader(user);
			final XFTItem newItem = reader.parse(new ByteArrayInputStream(sw.toString().getBytes()));
			sw.close();
			//sr.close();
			combSess.setScans_scan(newItem);
		}
		combSess.setLabel(params.containsKey(SESSION_LABEL_PARAM) ? params.get(SESSION_LABEL_PARAM) : releaseRules.getDefaultSessionLabel(proj, subj, params));
		if (!anyRelease) {
			final String errorMsg = "ERROR:  Session includes no scans marked for export";
			entity.setStatusInfo(errorMsg);
			entity.setStatusTime(new Date());
			throw new ClientException(errorMsg);
		}
		return combSess;
	}
	
	private boolean isAlternateScan(XnatMrscandata intraScan) {
		final String type = intraScan.getDbtype();
		final String desc = intraScan.getDbdesc();
		if (type == null || desc == null) {
			return false;
		}
		return (type.endsWith("_Alt") && desc.endsWith("_Alt"));
	}
	
	private List<XnatMrsessiondata> getProjectExpts(XnatProjectdata proj, ArrayList<XnatMrsessiondata> expts) {
		final Iterator<XnatMrsessiondata> i = expts.iterator();
		while (i.hasNext()) {
			final XnatMrsessiondata expt = i.next();
			if (!expt.getProject().equals(proj.getId())) {
				i.remove();
			}
		}
		return expts;
	}

	private XnatMrsessiondata getScanSession(XnatMrscandata intraScan, List<XnatMrsessiondata> expts) {
		for (final XnatMrsessiondata expt : expts) {
			if (intraScan.getImageSessionId().equals(expt.getId())) {
				return (XnatMrsessiondata)expt;
			}
		}
		return null;
	}

	private List<XnatMrscandata> getOrderedScanList(final List<XnatMrsessiondata> sessionList, final CcfReleaseRulesI releaseRules) {
		
		return CcfReleaseRulesUtils.getOrderedScanList(sessionList, releaseRules);
		
	}

	private void parseAndSetDate(String dateStr, XnatSubjectassessordata sess) {
		// Per Sandy, 2013-01-17, do not set anything as a session date
		sess.setDate(null);
	}

	private Object getCombinedAttr(Object combVal, Object mrVal,
			String attrString) {
		if (combVal == null) {
			combVal = mrVal;
		} else if (!combVal.equals(mrVal)) {
			final String returnString = "WARNING:  " + attrString
					+ " value differs between sessions (CURRENT="
					+ mrVal.toString() + ",PREVIOUS=" + combVal.toString()
					+ ").";
			log.warn(returnString);
		}
		return combVal;
	}
	
}
