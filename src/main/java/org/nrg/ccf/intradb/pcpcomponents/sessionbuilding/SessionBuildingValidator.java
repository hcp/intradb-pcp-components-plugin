package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.interfaces.CcfResourceGeneratorI;
import org.nrg.ccf.sessionbuilding.pojo.ResourceValidationResults;
import org.nrg.ccf.sessionbuilding.preferences.CcfSessionBuildingPreferences;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineValidator
public class SessionBuildingValidator implements PipelineValidatorI {
	
	protected static final String NOT_ENABLED_MSG = "Session building is not enabled for this project.";
	protected final CcfSessionBuildingPreferences _sbPref = XDAT.getContextService().getBean(CcfSessionBuildingPreferences.class);
	protected final Map<String,Map<String,String>> prefMap = new HashMap<>();
	protected final static String ENABLED = "enabled";
	protected final static String PROJECT = "project";
	
	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user) {
		
		if (!isEnabledProject(statusEntity.getProject())) {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(NOT_ENABLED_MSG)) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedInfo(NOT_ENABLED_MSG);
				statusEntity.setValidatedTime(new Date());
			}
			return;
		};
		final String sbProject = getSbProject(statusEntity.getProject()); 
		final String expLabel = getExpectedLabelForEntity(statusEntity);
		final org.nrg.xft.search.CriteriaCollection criteria = new CriteriaCollection("AND");
      	criteria.addClause("xnat:mrSessionData.label",expLabel);
      	criteria.addClause("xnat:mrSessionData.project",sbProject);
      	final String notFoundInfo = "No session found in project with expected session label (PROJECT=" + sbProject + 
      			", EXP=" + expLabel + ", USER=" + user.getLogin() + ").";
		List<XnatMrsessiondata> mrSessions = XnatMrsessiondata.getXnatMrsessiondatasByField(criteria, user, false);
		if (mrSessions.size()>0) {
			boolean resourceCheck = true;
			final String resourceGenClass = _sbPref.getCcfSessionBuildingResourceGen(statusEntity.getProject());
			try {
				final CcfResourceGeneratorI resourceGenerator = (CcfResourceGeneratorI) Class.forName(resourceGenClass)
						.getDeclaredConstructor(List.class, XnatImagesessiondata.class, CcfReleaseRulesI.class, UserI.class)
						.newInstance(null, mrSessions.get(0), null, null);
				final ResourceValidationResults validationResult = resourceGenerator.validateUnprocResources();
				if (!validationResult.getIsValid()) {
					final String validationInfo = validationResult.getErrorList().toString();
					if (statusEntity.getValidated() || statusEntity.getValidatedInfo() == null ||
							!statusEntity.getValidatedInfo().equals(validationInfo)) {
						statusEntity.setValidatedInfo(validationInfo);
						statusEntity.setValidated(false);
						statusEntity.setValidatedTime(new Date());
						resourceCheck = false;
					}
					return;
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException
					| ClassNotFoundException e) {
				log.warn("WARNING:  Could not obtain unprocessed resource generator " + resourceGenClass);
			} catch (ReleaseResourceException e) {
				log.warn("WARNING:  Exception thrown validation unproc resources " + e.toString());
			}
			if (resourceCheck && !statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedInfo("");
				statusEntity.setValidatedTime(new Date());
				if (statusEntity.getStatus().equals(PcpConstants.PcpStatus.ERROR.toString())) {
					statusEntity.setStatus(PcpConstants.PcpStatus.EXT_COMPLETE);
					statusEntity.setStatusTime(new Date());
				}
			}
		} else {
			if (statusEntity.getValidated() || statusEntity.getValidatedInfo() == null ||
					!statusEntity.getValidatedInfo().equals(notFoundInfo)) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(notFoundInfo);
			}
		}
		
	}

	protected String getExpectedLabelForEntity(PcpStatusEntity statusEntity) {
		return (!statusEntity.getSubGroup().equalsIgnoreCase("ALL")) ? 
				statusEntity.getEntityLabel() + "_" + statusEntity.getSubGroup() + "_MR" :
				statusEntity.getEntityLabel() + "_MR"
					;
	}

	protected boolean isEnabledProject(String project) {
		if (_sbPref==null) {
			return false;
		}
		if (!prefMap.containsKey(project)) {
			prefMap.put(project, new HashMap<String,String>());
		}
		final Map<String,String> thisMap = prefMap.get(project);
		if (!thisMap.containsKey(ENABLED)) {
			final Boolean isEnabled = _sbPref.getCcfSessionBuildingEnabled(project);
			thisMap.put(ENABLED, (isEnabled !=null) ? isEnabled.toString() : "false");
		}
		return Boolean.valueOf(thisMap.get(ENABLED));
	}

	protected String getSbProject(String project) {
		if (_sbPref==null) {
			return "";
		}
		if (!prefMap.containsKey(project)) {
			prefMap.put(project, new HashMap<String,String>());
		}
		final Map<String,String> thisMap = prefMap.get(project);
		if (!thisMap.containsKey(PROJECT)) {
			final String sbProj = _sbPref.getCcfSessionBuildingBuildProject(project);
			thisMap.put(PROJECT, (sbProj !=null) ? sbProj.toString() : "");
		}
		return thisMap.get(PROJECT);
	}

}
