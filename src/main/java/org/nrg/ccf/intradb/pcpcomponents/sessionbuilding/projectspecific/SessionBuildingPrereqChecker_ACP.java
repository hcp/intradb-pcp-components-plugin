package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingPrereqChecker;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class SessionBuildingPrereqChecker_ACP extends SessionBuildingPrereqChecker {
	
	final Map<String,List<XnatMrsessiondata>> _projectCache = new HashMap<>();
	
	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
        final CriteriaCollection cc = new CriteriaCollection("AND");
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/subject_ID", statusEntity.getEntityId());
        final CriteriaCollection subCC = new CriteriaCollection("OR");
        subCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", statusEntity.getProject());
        subCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", statusEntity.getProject());
        cc.add(subCC);
		final List<XnatMrsessiondata> exps=XnatMrsessiondata.getXnatMrsessiondatasByField(cc, user, false);
		if (exps.size()<1) {
			// Must have experiments
			prereqsNotMet(statusEntity, "Subject has no experiments.");
			return;
		}
		for (final XnatMrsessiondata sess : exps) {
			final String entityGroup = statusEntity.getSubGroup();
			final String lcLabel = sess.getLabel().toLowerCase();
			boolean retestSession = (lcLabel.contains("retest") && lcLabel.contains("calibration"));
			if ((!retestSession && entityGroup.equals("RETEST")) ||
				(retestSession && !entityGroup.equals("RETEST"))) { 
				continue;
			}
			final org.nrg.xft.search.CriteriaCollection sanCC = new CriteriaCollection("AND");
  	      	sanCC.addClause("san:SanityChecks.imageSession_ID",sess.getId());
  	      	sanCC.addClause("san:SanityChecks.project",statusEntity.getProject());
			final List<SanSanitychecks> schecks = SanSanitychecks.getSanSanitycheckssByField(sanCC, user, false);
			if (schecks.size()<1) {
				// Those experiments must have sanity checks
				prereqsNotMet(statusEntity, "Sanity checks not run for one or more experiments.");
				return;
			}
			for (final SanSanitychecks scheck : schecks) {
				if (scheck.getOverall_status() == null || scheck.getOverall_status().equals("FAILED")) {
					// Those experiments must not be in failed status
					prereqsNotMet(statusEntity, "Sanity checks failed for one or more experiments.");
					return;
				}
			}
		}
		if (statusEntity.getPrereqs() == null || !statusEntity.getPrereqs()) {
			statusEntity.setPrereqs(true);
			statusEntity.setPrereqsInfo("");
			statusEntity.setPrereqsTime(new Date());
		}
	}
	
}
