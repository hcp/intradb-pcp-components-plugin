package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingPrereqChecker;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.hcp.releaserules.projectutils.ccf_ecp.EcpUtils;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class SessionBuildingPrereqChecker_ECP extends SessionBuildingPrereqChecker {
	
	@Override
	protected List<XnatMrsessiondata> getFilteredExperimentList(List<XnatMrsessiondata> exps, PcpStatusEntity statusEntity, UserI user) {
		final List<XnatMrsessiondata> filteredExps = new ArrayList<>();
		final String entityGroup = statusEntity.getSubGroup();
		for (final XnatMrsessiondata session : exps) {
			final boolean isBaseline = session.getLabel().matches("^.*_[12]_.*$");
			final boolean isFollowup = session.getLabel().matches("^.*_3_.*$");
			final boolean isControl = EcpUtils.isControl(statusEntity.getEntityLabel());
			if (!(isBaseline || isFollowup || isControl)) {
				continue;
			}
			if ((entityGroup.equals("BASELINE") && (isBaseline && !isControl)) || (entityGroup.equals("FOLLOWUP") && isFollowup) ||
					(entityGroup.equals("CONTROL") && (isControl && isBaseline))) {
				filteredExps.add(session);
			}
		}
		return filteredExps;
	}
	
}
