package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingPrereqChecker;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.hcp.releaserules.projectutils.ccf_ftd.ReleaseRules_CCF_FTD;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelinePrereqChecker
public class SessionBuildingPrereqChecker_FTD extends SessionBuildingPrereqChecker {
	
	protected CcfReleaseRulesUtils _releaseRulesUtils = XDAT.getContextService().getBean(CcfReleaseRulesUtils.class);
	final Map<String,List<XnatMrsessiondata>> _projectCache = new HashMap<>();
	CcfReleaseRulesI _releaseRules = null;
	
	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		if (statusEntity.getSubGroup().equals(ReleaseRules_CCF_FTD.UNKNOWN_LABEL)) {
			prereqsNotMet(statusEntity,"Subgroup is UNOWWN for this entity.  NDA fields have not been set.");
			return;
		}
		final XnatSubjectdata subject = XnatSubjectdata.getXnatSubjectdatasById(statusEntity.getEntityId(), user, false);
		// Is there a specified nda_subject_label for this subject?
		final Object ndaLabelObj = subject.getFieldByName("nda_subject_label");
		if (ndaLabelObj != null && ndaLabelObj instanceof String && ((String)ndaLabelObj).length()>0) {
			super.checkPrereqs(statusEntity,user);
		} else {
			prereqsNotMet(statusEntity,"The nda_subject_label field value has not been set for this subject.");
		} 
	}
	

	@Override
	protected List<XnatMrsessiondata> getFilteredExperimentList(List<XnatMrsessiondata> exps, PcpStatusEntity statusEntity, UserI user) {
		if (_releaseRules == null) {
			_releaseRules = _releaseRulesUtils.getReleaseRulesForProject(statusEntity.getProject()).getReleaseRules();
		}
		final Map<String,String> params = new HashMap<>();
		params.put("selectionCriteria", statusEntity.getSubGroup());
		List<XnatMrsessiondata> filteredExps;
		try {
			filteredExps = _releaseRules.filterExptList(exps, params, user);
		} catch (ReleaseRulesException e) {
			filteredExps = new ArrayList<>();
			log.error("Error getting filtered experiment list:   " + e.toString());
		}
		final String entityGroup = statusEntity.getSubGroup();
		for (final XnatMrsessiondata sess : exps) {
			if (!sess.getLabel().toLowerCase().contains("_" + entityGroup.toLowerCase() + "_") &&
					!_singletonSubgroups.contains(entityGroup)) {
				continue;
			}
			filteredExps.add(sess);
		}
		return filteredExps;
	}
	
}
