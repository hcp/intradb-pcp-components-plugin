package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingPrereqChecker;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class SessionBuildingPrereqChecker_HarmSuppl extends SessionBuildingPrereqChecker {
	
	@Override
	protected List<XnatMrsessiondata> getFilteredExperimentList(List<XnatMrsessiondata> exps, PcpStatusEntity statusEntity, UserI user) {
		final List<XnatMrsessiondata> filteredExps = new ArrayList<>();
		final String entityGroup = "_" + statusEntity.getSubGroup();
		for (final XnatMrsessiondata session : exps) {
			if (session.getLabel().contains(entityGroup)) {
				filteredExps.add(session);
			}
		}
		return filteredExps;
	}
	
}
