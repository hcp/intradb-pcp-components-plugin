package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingPrereqChecker;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class SessionBuildingPrereqChecker_MDD extends SessionBuildingPrereqChecker {
	
	final Map<String,List<XnatMrsessiondata>> _projectCache = new HashMap<>();

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		final List<XnatMrsessiondata> exps = getCacheForProject(statusEntity.getProject(), user);
		final Iterator<XnatMrsessiondata> i = exps.iterator();
		// MDD-specific code here
		while (i.hasNext()) {
			final XnatMrsessiondata exp = i.next();
			if (!pullSubjectFromLabel(exp.getLabel()).equals(pullSubjectFromLabel(statusEntity.getEntityLabel()))) {
				i.remove();
			} else {
				final String expLabelVisit = pullVisitFromLabel(exp.getLabel());
				final String entLabelVisit = pullVisitFromLabel(statusEntity.getEntityLabel());
				if (!(
						expLabelVisit.equals(entLabelVisit) ||
						(expLabelVisit.equals("") && (entLabelVisit.equals("01"))) ||
						(expLabelVisit.equals("01") && (entLabelVisit.equals("")))
						)) {
					i.remove();
				}
			}
		}
		if (exps.size()<1) {
			// Must have experiments
			prereqsNotMet(statusEntity, "Subject has no experiments.");
			return;
		}
		for (final XnatMrsessiondata sess : exps) {
			// Here's the change for this version (SessionBuildingPrereqChecker should maybe be LifeSpan specific????)
			//if (!sess.getLabel().toLowerCase().contains("_" + statusEntity.getSubGroup().toLowerCase() + "_")) {
			//	continue;
			//}
			final org.nrg.xft.search.CriteriaCollection sanCC = new CriteriaCollection("AND");
  	      	sanCC.addClause("san:SanityChecks.imageSession_ID",sess.getId());
  	      	sanCC.addClause("san:SanityChecks.project",statusEntity.getProject());
			final List<SanSanitychecks> schecks = SanSanitychecks.getSanSanitycheckssByField(sanCC, user, false);
			if (schecks.size()<1) {
				// Those experiments must have sanity checks
				prereqsNotMet(statusEntity, "Sanity checks not run for one or more experiments.");
				return;
			}
			for (final SanSanitychecks scheck : schecks) {
				if (scheck.getOverall_status() == null || scheck.getOverall_status().equals("FAILED")) {
					// Those experiments must not be in failed status
					prereqsNotMet(statusEntity, "Sanity checks failed for one or more experiments.");
					return;
				}
			}
		}
		if (statusEntity.getPrereqs() == null || !statusEntity.getPrereqs()) {
			statusEntity.setPrereqs(true);
			statusEntity.setPrereqsInfo("");
			statusEntity.setPrereqsTime(new Date());
		}
	}

	private List<XnatMrsessiondata> getCacheForProject(String project, UserI user) {
		if (!_projectCache.containsKey(project)) {
			final CriteriaCollection cc = new CriteriaCollection("OR");
			//cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/subject_ID", statusEntity.getEntityId());
			//final CriteriaCollection subCC = new CriteriaCollection("OR");
			//subCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", statusEntity.getProject());
			//subCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", statusEntity.getProject());
			cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", project);
			cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", project);
			final List<XnatMrsessiondata> cacheList = XnatMrsessiondata.getXnatMrsessiondatasByField(cc, user, false);
			_projectCache.put(project, cacheList);
		}
		return new ArrayList<XnatMrsessiondata>(_projectCache.get(project));
	}

	private String pullSubjectFromLabel(String label) {
		return label.replaceAll("[^a-zA-Z0-9]","").substring(0, 5).toLowerCase();
	}

	private String pullVisitFromLabel(String label) {
		String returnVal = label.replaceAll("[^0-9]","").substring(4).toLowerCase();
		if (returnVal.length()==1) {
			returnVal = "0" + returnVal;
		}
		return returnVal;
	}

}
