package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingPrereqChecker;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils.ReleaseRulesResponse;
import org.nrg.prefs.services.NrgPreferenceService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class SessionBuildingPrereqChecker_PHCP extends SessionBuildingPrereqChecker {
	
	final Map<String,List<XnatMrsessiondata>> _projectCache = new HashMap<>();
	final NrgPreferenceService _preferenceService = XDAT.getContextService().getBeanSafely(NrgPreferenceService.class);
	ReleaseRulesResponse _rulesResponse = null;
	final CcfReleaseRulesUtils _rulesUtils;
	
	
	public SessionBuildingPrereqChecker_PHCP() {
		super();
		_rulesUtils = new CcfReleaseRulesUtils(_preferenceService);
	}


	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		final CcfReleaseRulesI releaseRules = _rulesUtils.getReleaseRulesForProject(statusEntity.getProject()).getReleaseRules();
		final Map<String,String> params = new HashMap<>();
		params.put("selectionCriteria", statusEntity.getSubGroup());
        final CriteriaCollection cc = new CriteriaCollection("AND");
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/subject_ID", statusEntity.getEntityId());
        final CriteriaCollection subCC = new CriteriaCollection("OR");
        subCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", statusEntity.getProject());
        subCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", statusEntity.getProject());
        cc.add(subCC);
		final List<XnatMrsessiondata> exps=XnatMrsessiondata.getXnatMrsessiondatasByField(cc, user, false);
		if (exps.size()<1) {
			// Must have experiments
			prereqsNotMet(statusEntity, "Subject has no experiments.");
			return;
		}
		for (final XnatMrsessiondata sess : exps) {
			final String entityGroup = statusEntity.getSubGroup();
			final String lcLabel = sess.getLabel().toLowerCase();
			final String scanner = sess.getScanner();
			final String sessionType = sess.getSessionType();
			boolean isMRS = scanner.contains("7TAS") && (lcLabel.contains("mrs") || sessionType.endsWith("MRS"));
			boolean is7T = scanner.contains("7TAS") && !isMRS;
			boolean is3T = scanner.equals("TRIOC");
			if (!((entityGroup.equals("3T") && is3T) || (entityGroup.contains("_7T_") && is7T) || 
					(entityGroup.contains("_MRS_") && isMRS))) {
				continue;
			}
			if (!(
					(is3T && entityGroup.equals("3T")) ||
					(is7T && sess.getLabel().contains(entityGroup.replaceAll("T_",""))) || 
					(isMRS && sess.getLabel().contains(entityGroup.replaceAll("_MRS_","_7"))) 
					)) {
				continue;
			}
			if (is3T && entityGroup.equals("3T")) {
				// Currently only looking at sanity checks for 3T
				final org.nrg.xft.search.CriteriaCollection sanCC = new CriteriaCollection("AND");
	  	      	sanCC.addClause("san:SanityChecks.imageSession_ID",sess.getId());
	  	      	sanCC.addClause("san:SanityChecks.project",statusEntity.getProject());
				final List<SanSanitychecks> schecks = SanSanitychecks.getSanSanitycheckssByField(sanCC, user, false);
				if (schecks.size()<1) {
					// Those experiments must have sanity checks
					prereqsNotMet(statusEntity, "Sanity checks not run for one or more experiments.");
					return;
				}
				for (final SanSanitychecks scheck : schecks) {
					if (scheck.getOverall_status() == null || scheck.getOverall_status().equals("FAILED")) {
						// Those experiments must not be in failed status
						prereqsNotMet(statusEntity, "Sanity checks failed for one or more experiments.");
						return;
					}
				}
			} 
			// If we made it this far, let's check whether or not we have a release class that's runnable for this
			// subgroup.
			try {
				List<XnatMrsessiondata> filteredExpts = releaseRules.filterExptList(exps,params,user);
				if (filteredExpts.size()<1) {
					prereqsNotMet(statusEntity,"Current rules class returns no filtered experiments (" + 
							releaseRules.getClass().getName() + ")");
					return;
				}
			} catch (ReleaseRulesException e) {
					prereqsNotMet(statusEntity,"Current rules class threw exception obtaining filtered experiment list (" + 
							releaseRules.getClass().getName() + ") " + e.toString());
					return;
			}
		}
		if (statusEntity.getPrereqs() == null || !statusEntity.getPrereqs()) {
			statusEntity.setPrereqs(true);
			statusEntity.setPrereqsInfo("");
			statusEntity.setPrereqsTime(new Date());
		}
	}
	
}
