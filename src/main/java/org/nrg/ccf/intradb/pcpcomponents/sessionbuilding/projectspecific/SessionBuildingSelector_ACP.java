package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingSelector;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

@PipelineSelector
public class SessionBuildingSelector_ACP extends SessionBuildingSelector {
	
	public SessionBuildingSelector_ACP() {
		super();
	}
	
	@Override
	protected boolean hasMatchingMrsession(XnatSubjectdata subj, List<XnatMrsessiondata> mrSessions, String group) {
		for (final XnatMrsessiondata session  : mrSessions) {
			final String lcLabel = session.getLabel().toLowerCase();
			// NOT RELEASING the "N" subjects.
			if (lcLabel.startsWith("n")) {
				continue;
			}
			boolean retestSession = (lcLabel.contains("retest") && lcLabel.contains("calibration"));
			if ((!retestSession && group.equals("RETEST")) ||
			    (retestSession && !group.equals("RETEST"))) {
					continue;
			}
			if (session.getSubjectId().equals(subj.getId())) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected List<String> getGroupList(final String projectId) {
		return Arrays.asList(new String[] {"PRIMARY","RETEST"});
	}


}
