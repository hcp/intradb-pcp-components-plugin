package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingSelector;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

@PipelineSelector
public class SessionBuildingSelector_BWH extends SessionBuildingSelector {
	
	final static String[] CCF_BWH_GROUPS = new String[] {"01"}; 
	
	public SessionBuildingSelector_BWH() {
		super();
	}
	
	@Override
	protected boolean hasMatchingMrsession(XnatSubjectdata subj, List<XnatMrsessiondata> mrSessions, String group) {
		for (final XnatMrsessiondata session  : mrSessions) {
			if (session.getSubjectId().equals(subj.getId())) {
				String labelCheck = session.getLabel().toUpperCase(); 
				if ((group.equals("01") && labelCheck.endsWith("_MR1")) || (group.equals("02") && labelCheck.endsWith("_MR2"))) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	protected List<String> getGroupList(String projectId) {
		switch(projectId) {
		case "CCF_BWH_PRC": return Arrays.asList(CCF_BWH_GROUPS);
		default: return Arrays.asList(DEFAULT_GROUPS);
		}
	}

}
