package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingSelector;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

@PipelineSelector
public class SessionBuildingSelector_DMCC extends SessionBuildingSelector {
	
	final static String[] DMCC_GROUPS = new String[] {"PHASE2","PHASE3","PHASE4"}; 

	public SessionBuildingSelector_DMCC() {
		super();
	}
	
	@Override
	protected List<String> getGroupList(String projectId) {
		switch(projectId) {
		case "CCF_DMCC_ITK": return Arrays.asList(DMCC_GROUPS);
		default: return Arrays.asList(DEFAULT_GROUPS);
		}
	}

}
