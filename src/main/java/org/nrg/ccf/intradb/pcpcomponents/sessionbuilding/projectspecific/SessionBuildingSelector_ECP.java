package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingSelector;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.hcp.releaserules.projectutils.ccf_ecp.EcpUtils;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

@PipelineSelector
public class SessionBuildingSelector_ECP extends SessionBuildingSelector {
	
	public SessionBuildingSelector_ECP() {
		super();
	}
	
	@Override
	protected boolean hasMatchingMrsession(XnatSubjectdata subj, List<XnatMrsessiondata> mrSessions, String group) {
		for (final XnatMrsessiondata session : mrSessions) {
			boolean isBaseline = session.getLabel().matches("^.*_[12]_.*$");
			boolean isFollowup = session.getLabel().matches("^.*_3_.*$");
			boolean isControl = EcpUtils.isControl(subj.getLabel());
			if (!(isBaseline || isFollowup || isControl)) {
				continue;
			}
			if ((group.equals("BASELINE") && (isBaseline && !isControl)) || (group.equals("FOLLOWUP") && isFollowup) || 
					(group.equals("CONTROL") && (isControl && isBaseline))) {
				if (session.getSubjectId().equals(subj.getId())) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	protected List<String> getGroupList(final String projectId) {
		return Arrays.asList(new String[] {"BASELINE","FOLLOWUP","CONTROL"});
	}

}
