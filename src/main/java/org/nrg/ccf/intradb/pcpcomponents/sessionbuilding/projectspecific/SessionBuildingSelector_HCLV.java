package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;


import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingSelector_SubjectsWithSessions;
import org.nrg.ccf.pcp.anno.PipelineSelector;

@PipelineSelector
public class SessionBuildingSelector_HCLV extends SessionBuildingSelector_SubjectsWithSessions {
	
	public SessionBuildingSelector_HCLV() {
		super();
	}
	
}
