package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingSelector;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.hcp.releaserules.projectutils.ccf_ecp.EcpUtils;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

@PipelineSelector
public class SessionBuildingSelector_HarmSuppl extends SessionBuildingSelector {
	
	public SessionBuildingSelector_HarmSuppl() {
		super();
	}
	
	@Override
	protected boolean hasMatchingMrsession(XnatSubjectdata subj, List<XnatMrsessiondata> mrSessions, String group) {
		for (final XnatMrsessiondata session : mrSessions) {
			boolean isABCD = session.getLabel().contains("_ABCD");
			boolean isADNI =  session.getLabel().contains("_ADNI");
			boolean isHCP = session.getLabel().contains("_HCP");
			if (!(isABCD || isADNI || isHCP)) {
				continue;
			}
			if ((group.equals("ABCD") && isABCD) || (group.equals("ADNI") && isADNI) || 
					(group.equals("HCP") && isHCP)) {
				if (session.getSubjectId().equals(subj.getId())) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	protected List<String> getGroupList(final String projectId) {
		return Arrays.asList(new String[] {"ABCD","ADNI","HCP"});
	}

}
