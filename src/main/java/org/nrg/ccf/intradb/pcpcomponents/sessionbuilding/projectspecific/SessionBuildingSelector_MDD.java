package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingSelector;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

@PipelineSelector
public class SessionBuildingSelector_MDD extends SessionBuildingSelector {
	
	final static String[] CCF_MDD_GROUPS = new String[] {"01","02","03","04"}; 

	public SessionBuildingSelector_MDD() {
		super();
	}
	
	@Override
	protected boolean hasMatchingMrsession(XnatSubjectdata subj, List<XnatMrsessiondata> mrSessions, String group) {
		final Iterator<XnatMrsessiondata> i = mrSessions.iterator();
		while (i.hasNext()) {
			final XnatMrsessiondata session = i.next();
			if (session.getSubjectId().equals(subj.getId())) {
				String labelCheck = session.getLabel().toLowerCase().replaceFirst("[a-z]*$", ""); 
				if ((group.equals("01") && labelCheck.length()<=5) || (labelCheck.length()>=7 && labelCheck.substring(5,7).equals(group))) {
					i.remove();
					return true;
				}
			}
		}
		return false;
	}
		
	@Override
	protected List<String> getGroupList(String projectId) {
		switch(projectId) {
		case "CCF_MDD_PRC": return Arrays.asList(CCF_MDD_GROUPS);
		default: return Arrays.asList(DEFAULT_GROUPS);
		}
	}

}
