package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingSelector;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

@PipelineSelector
public class SessionBuildingSelector_PHCP extends SessionBuildingSelector {
	
	public SessionBuildingSelector_PHCP() {
		super();
	}
	
	@Override
	protected boolean hasMatchingMrsession(XnatSubjectdata subj, List<XnatMrsessiondata> mrSessions, String group) {
		for (final XnatMrsessiondata session  : mrSessions) {
			if (!session.getSubjectId().equals(subj.getId())) {
				continue;
			}
			final String sessionLabel = session.getLabel();
			final String lcLabel = sessionLabel.toLowerCase();
			final String scanner = session.getScanner();
			final String sessionType = session.getSessionType();
			boolean isMRS = scanner.contains("7TAS") && (lcLabel.contains("mrs") || sessionType.endsWith("MRS"));
			boolean is7T = scanner.contains("7TAS") && !isMRS;
			boolean is3T = scanner.equals("TRIOC");
			if (!((group.equals("3T") && is3T) || (group.contains("_7T_") && is7T) || 
					(group.contains("_MRS_") && isMRS))) {
				continue;
			}
			if (is3T && group.equals("3T" )) {
				return true;
			}
			final String compareGroup = group.replace("_MRS_","_7").replace("_7T_", "_7");
			if (is7T && sessionLabel.contains(compareGroup)) {
				return true;
			} else if (isMRS && sessionLabel.contains(compareGroup)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected List<String> getGroupList(final String projectId) {
		//return Arrays.asList(new String[] {"3T","7T","MRS"});
		return Arrays.asList(new String[] {"3T","V2_7T_A","V2_7T_B","V2_7T_Z","V3_7T_A","V3_7T_B","V3_7T_Z",
				"V2_MRS_A","V2_MRS_B","V2_MRS_Z","V3_MRS_A","V3_MRS_B","V3_MRS_Z"});
	}

}
