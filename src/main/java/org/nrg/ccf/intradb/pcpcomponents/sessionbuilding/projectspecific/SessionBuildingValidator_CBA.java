package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Date;
import java.util.List;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

@PipelineValidator
public class SessionBuildingValidator_CBA extends SessionBuildingValidator {

	public SessionBuildingValidator_CBA() {
		super();
	}
	
	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user) {
		
		if (!isEnabledProject(statusEntity.getProject())) {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(NOT_ENABLED_MSG)) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedInfo(NOT_ENABLED_MSG);
				statusEntity.setValidatedTime(new Date());
			}
			return;
		};
		final String sbProject = getSbProject(statusEntity.getProject()); 
		final String expLabel = statusEntity.getEntityLabel() + 
				((statusEntity.getSubGroup().equals("RETEST")) ? "_RT" : "") + "_MR";
		final org.nrg.xft.search.CriteriaCollection criteria = new CriteriaCollection("AND");
      	criteria.addClause("xnat:mrSessionData.label",expLabel);
      	criteria.addClause("xnat:mrSessionData.project",sbProject);
      	final String notFoundInfo = "No session found in project with expected session label (PROJECT=" + sbProject + 
      			", EXP=" + expLabel + ", USER=" + user.getLogin() + ").";
		List<XnatMrsessiondata> mrSessions = XnatMrsessiondata.getXnatMrsessiondatasByField(criteria, user, false);
		if (mrSessions.size()>0) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedInfo("");
				statusEntity.setValidatedTime(new Date());
				if (statusEntity.getStatus().equals(PcpConstants.PcpStatus.ERROR.toString())) {
					statusEntity.setStatus(PcpConstants.PcpStatus.EXT_COMPLETE);
					statusEntity.setStatusTime(new Date());
				}
			}
		} else {
			if (statusEntity.getValidated() || statusEntity.getValidatedInfo() == null ||
					!statusEntity.getValidatedInfo().equals(notFoundInfo)) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(notFoundInfo);
			}
		}
		
	}

}
