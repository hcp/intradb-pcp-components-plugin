package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;

@PipelineValidator
public class SessionBuildingValidator_EEAJ extends SessionBuildingValidator {

	public SessionBuildingValidator_EEAJ() {
		super();
	}

	@Override
	protected String getExpectedLabelForEntity(PcpStatusEntity statusEntity) {
		return statusEntity.getSubGroup();
	}
	
}
