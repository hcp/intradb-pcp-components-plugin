package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;

@PipelineValidator
public class SessionBuildingValidator_FTD extends SessionBuildingValidator {

	public SessionBuildingValidator_FTD() {
		super();
	}

	@Override
	protected String getExpectedLabelForEntity(PcpStatusEntity statusEntity) {
		return statusEntity.getSubGroup();
	}
	
}
