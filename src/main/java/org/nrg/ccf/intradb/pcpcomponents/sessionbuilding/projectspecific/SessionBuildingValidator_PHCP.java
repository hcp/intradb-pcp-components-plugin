package org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.projectspecific;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.nrg.ccf.intradb.pcpcomponents.sessionbuilding.SessionBuildingValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils.ReleaseRulesResponse;
import org.nrg.prefs.services.NrgPreferenceService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

@PipelineValidator
public class SessionBuildingValidator_PHCP extends SessionBuildingValidator {
	
	//final Map<String,List<XnatMrsessiondata>> _projectCache = new HashMap<>();
	final NrgPreferenceService _preferenceService = XDAT.getContextService().getBeanSafely(NrgPreferenceService.class);
	ReleaseRulesResponse _rulesResponse = null;
	final CcfReleaseRulesUtils _rulesUtils;
	

	public SessionBuildingValidator_PHCP() {
		super();
		_rulesUtils = new CcfReleaseRulesUtils(_preferenceService);
	}
	
	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user) {
		
		final CcfReleaseRulesI releaseRules = _rulesUtils.getReleaseRulesForProject(statusEntity.getProject()).getReleaseRules();
		if (!isEnabledProject(statusEntity.getProject())) {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(NOT_ENABLED_MSG)) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedInfo(NOT_ENABLED_MSG);
				statusEntity.setValidatedTime(new Date());
			}
			return;
		};
		final String sbProject = getSbProject(statusEntity.getProject()); 
		final Map<String,String> params = new HashMap<>();
		params.put("selectionCriteria", statusEntity.getSubGroup());
		final org.nrg.xft.search.CriteriaCollection subjCriteria = new CriteriaCollection("AND");
      	subjCriteria.addClause("xnat:subjectData.label",statusEntity.getEntityId());
      	subjCriteria.addClause("xnat:subjectData.project",sbProject);
		final XnatSubjectdata subj = XnatSubjectdata.getXnatSubjectdatasById(statusEntity.getEntityId(), user, false);
		final String expLabel = releaseRules.getDefaultSessionLabel(subj.getProjectData(), subj, params);
		final org.nrg.xft.search.CriteriaCollection criteria = new CriteriaCollection("AND");
      	criteria.addClause("xnat:mrSessionData.label",expLabel);
      	criteria.addClause("xnat:mrSessionData.project",sbProject);
      	final String notFoundInfo = "No session found in project with expected session label (PROJECT=" + sbProject + 
      			", EXP=" + expLabel + ", USER=" + user.getLogin() + ").";
		List<XnatMrsessiondata> mrSessions = XnatMrsessiondata.getXnatMrsessiondatasByField(criteria, user, false);
		if (mrSessions.size()>0) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedInfo("");
				statusEntity.setValidatedTime(new Date());
				if (statusEntity.getStatus().equals(PcpConstants.PcpStatus.ERROR.toString())) {
					statusEntity.setStatus(PcpConstants.PcpStatus.EXT_COMPLETE);
					statusEntity.setStatusTime(new Date());
				}
			}
		} else {
			if (statusEntity.getValidated() || statusEntity.getValidatedInfo() == null ||
					!statusEntity.getValidatedInfo().equals(notFoundInfo)) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(notFoundInfo);
			}
		}
		
	}

}
