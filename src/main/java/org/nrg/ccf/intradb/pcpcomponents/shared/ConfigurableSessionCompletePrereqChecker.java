package org.nrg.ccf.intradb.pcpcomponents.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xft.security.UserI;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@PipelinePrereqChecker
public class ConfigurableSessionCompletePrereqChecker extends SessionCompletePrereqChecker implements PipelinePrereqCheckerI, ConfigurableComponentI {
	
    private String _subgroupRegex = null;    
	
	public List<String> getConfigurationYaml() {
		final List<String> configYaml = new ArrayList<>();
		configYaml.addAll(super.getConfigurationYaml());
		final StringBuilder sb = new StringBuilder()
				.append("subgroupRegex:\n")
				.append("    id: " + getClassNameForConfig() + "-subgroup-regex\n")
				.append("    name: " + getClassNameForConfig() + "-subgroup-regex\n")
				.append("    kind: panel.input.text\n")
				.append("    size: 50\n")
				.append("    label: (OPTIONAL) Subgroup Regex\n")
				.append("    description: Regex to match included subgroups (functionality only available for certain subgroups)\n")
				.append("    placeholder: (?i)^.*(3T|7T).*$\n")
				;
		configYaml.add(sb.toString());
		return configYaml;
	}

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		if (subgroupRegex(statusEntity).length()>0) {
			if (!statusEntity.getSubGroup().matches(subgroupRegex(statusEntity))) {
				statusEntity.setPrereqsInfo("Sanity checks not implemented for this subgroup.");
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				return;
			}
		} 
		super.checkPrereqs(statusEntity, user);
	}
	
	
	protected String subgroupRegex(PcpStatusEntity entity) {
		if (_subgroupRegex == null) {
			try {
				final String configValue = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(), 
					getClassNameForConfig(), "-subgroup-regex", false).toString();
				_subgroupRegex = (configValue!=null && configValue.trim().length()>0) ? configValue : "";
			} catch (Exception e) {
				_subgroupRegex = "";
			}
		}
		return _subgroupRegex;
	}

}
