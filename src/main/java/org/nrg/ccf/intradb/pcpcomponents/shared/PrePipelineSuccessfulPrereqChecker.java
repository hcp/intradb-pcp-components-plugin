package org.nrg.ccf.intradb.pcpcomponents.shared;

import java.util.Date;

import org.nrg.ccf.intradb.pcpcomponents.prepipeline.PrePipelineValidator;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class PrePipelineSuccessfulPrereqChecker extends SessionCompletePrereqChecker implements PipelinePrereqCheckerI {

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		final Boolean prereqs = statusEntity.getPrereqs();
		final boolean completionWorkflow = hasCompletedWorkflows(statusEntity, user);
		if (completionWorkflow){
			final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
			if (PrePipelineValidator.prePipelineValidate(statusEntity.getProject(),session,user)) {
				if (prereqs == null || !prereqs) {
					statusEntity.setPrereqs(true);
					statusEntity.setPrereqsInfo("");
					statusEntity.setPrereqsTime(new Date());
				}
				return;
			} else {
				statusEntity.setPrereqsInfo("PrePipeline has not run successfully for session.");
			}
		} else {
			statusEntity.setPrereqsInfo(SessionCompletePrereqChecker.FAILURE_MESSAGE);
		}
		if (prereqs == null || prereqs) {
			statusEntity.setPrereqs(false);
			statusEntity.setPrereqsTime(new Date());
		}
	}

}
