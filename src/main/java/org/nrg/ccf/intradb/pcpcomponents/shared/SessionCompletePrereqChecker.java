package org.nrg.ccf.intradb.pcpcomponents.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

import com.google.common.collect.ImmutableList;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@PipelinePrereqChecker
public class SessionCompletePrereqChecker implements PipelinePrereqCheckerI, ConfigurableComponentI {
	
	public final static String FAILURE_MESSAGE = "No workflow exists indicating that session transfer/store is complete.";
	public static final String COMPLETION_PIPELINE_PROPERTY_PART = "-completion-pipeline-names";
	//public static final Map<String,CriteriaCollection> CC_CACHE = new HashMap<>();
	public static final Map<String,List<String>> CONFIG_CACHE = new HashMap<>();
	
	// TODO:  Make this list configurable?
	public static final String[] COMPLETION_PIPELINE_NAMES = {
		"Transfer",
		"Transferred",
		"Xsync",
		"Stored XAR",
		"AutoRun",
		"Stored XML",
		"SessionVerifiedComplete",
		"Executed script PrePipeline"
	};

	@Override
	public List<String> getConfigurationYaml() {
		final StringBuilder sb = new StringBuilder();
		sb.append("CompletionPipelineNames:\n")
        	.append("    id: " + getClassNameForConfig() + COMPLETION_PIPELINE_PROPERTY_PART + "\n")
        	.append("    name: " + getClassNameForConfig() + COMPLETION_PIPELINE_PROPERTY_PART + "\n")   
        	.append("    kind: panel.select.multiple\n")
        	.append("    label: Select Completion Indicator Workflows\n")
        	.append("    options:\n");
		for (final String pipel : Arrays.asList(COMPLETION_PIPELINE_NAMES)) {
			sb.append("        ").append(pipel).append("': '").append(pipel).append("'\n");
		}
		return ImmutableList.of(sb.toString());
	}

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		final Boolean prereqs = statusEntity.getPrereqs();
		if (hasCompletedWorkflows(statusEntity, user)){
			if (prereqs == null || !prereqs) {
				statusEntity.setPrereqs(true);
				statusEntity.setPrereqsInfo("");
				statusEntity.setPrereqsTime(new Date());
			}
			return;
		} 
		if (prereqs == null || prereqs || !FAILURE_MESSAGE.equals(statusEntity.getPrereqsInfo())) {
			statusEntity.setPrereqsInfo(FAILURE_MESSAGE);
			statusEntity.setPrereqs(false);
			statusEntity.setPrereqsTime(new Date());
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean hasCompletedWorkflows(final PcpStatusEntity statusEntity, final UserI user) {
		
		final CriteriaCollection cc = new CriteriaCollection("AND");
		cc.addClause("wrk:workFlowData.ID",statusEntity.getEntityId());
		//final CriteriaCollection cc = buildCC(statusEntity, statusEntity.getProject(), statusEntity.getPipeline(), 
		//			CLASS_NAME, COMPLETION_PIPELINE_PROPERTY_PART);
		final List<String> clauseList = getConfig(statusEntity.getProject(), statusEntity.getPipeline(), getClassNameForConfig(), COMPLETION_PIPELINE_PROPERTY_PART);
		final List<WrkWorkflowdata> workflows = WrkWorkflowdata.getWrkWorkflowdatasByField(cc, user, false,"wrk:workflowData.launch_time","desc");
		if(workflows.size()>0) { 
			for (final WrkWorkflowdata wf : workflows) {
				if (wf.getStatus().toUpperCase().startsWith("COMPLETE")) {
					for (final String clause : clauseList) {
						if (wf.getPipelineName().contains(clause)) {
							return true;
						}
					}	
				}
			}
		}
		return false;
	}

	@SuppressWarnings("rawtypes")
	private List<String> getConfig(String project, String pipeline, String className, String propertyName) {
		final StringBuilder keyValSb = new StringBuilder();
		keyValSb.append(project).append("-").append(pipeline).append("-").append(className).append("-").append(propertyName);
		final String keyVal = keyValSb.toString();
		if (!CONFIG_CACHE.containsKey(keyVal)) {
			final List<String> clauseList = new ArrayList<>();
			try {
				final Object complPipelineObj = PcpComponentUtils.getConfigValue(project, pipeline, 
					getClassNameForConfig(), COMPLETION_PIPELINE_PROPERTY_PART, false);
				if (complPipelineObj instanceof List) {
					for (final Object pnObj : (List)complPipelineObj) {
						clauseList.add(pnObj.toString().replaceAll("[\"']", ""));
					}
				} else {
					clauseList.addAll(Arrays.asList(COMPLETION_PIPELINE_NAMES));
				}
			} catch (PcpAutomationScriptExecutionException e) {
				clauseList.addAll(Arrays.asList(COMPLETION_PIPELINE_NAMES));
			}
			CONFIG_CACHE.put(keyVal, clauseList);
		}
		return CONFIG_CACHE.get(keyVal);
	}
	
	protected String getClassNameForConfig() {
		return getClass().getName().replace(".", "");
		
	}

	/*
	private static CriteriaCollection buildCC(PcpStatusEntity statusEntity, String project, String pipeline, 
			String className, String propertyName) {
		final CriteriaCollection cc = new CriteriaCollection("AND");
		cc.addClause("wrk:workFlowData.ID",statusEntity.getEntityId());
		cc.addClause(getSubCC(project, pipeline, className, propertyName));
		return cc;
	}
	*/

	/*
	@SuppressWarnings("rawtypes")
	private static CriteriaCollection getSubCC(String project, String pipeline, String className, String propertyName) {
		final StringBuilder keyValSb = new StringBuilder();
		keyValSb.append(project).append("-").append(pipeline).append("-").append(className).append("-").append(propertyName);
		final String keyVal = keyValSb.toString();
		CC_CACHE.clear();
		if (!CC_CACHE.containsKey(keyVal)) {
			final CriteriaCollection subCC = new CriteriaCollection("OR");
			final List<String> clauseList = new ArrayList<>();
			try {
				final Object complPipelineObj = PcpComponentUtils.getConfigValue(project, pipeline, 
					CLASS_NAME, COMPLETION_PIPELINE_PROPERTY_PART, false);
				if (complPipelineObj instanceof List) {
					for (final Object pnObj : (List)complPipelineObj) {
						clauseList.add(pnObj.toString().replaceAll("[\"']", ""));
					}
				} else {
					clauseList.addAll(Arrays.asList(COMPLETION_PIPELINE_NAMES));
				}
			} catch (PcpAutomationScriptExecutionException e) {
				clauseList.addAll(Arrays.asList(COMPLETION_PIPELINE_NAMES));
			}
			log.error("FINAL CLAUSE LIST:  " + clauseList.toString());
			for (final String pn : clauseList) {
				subCC.addClause("wrk:workFlowData.pipeline_name",pn);
			}
			CC_CACHE.put(keyVal,subCC);
		} 
		return CC_CACHE.get(keyVal);
	}
	*/

}
