package org.nrg.ccf.intradb.pcpcomponents.snapshot;

import java.util.Date;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceCheckUtils;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

@PipelineValidator
public class SnapshotGenerationValidator implements PipelineValidatorI {

	@Override
	public void validate(final PcpStatusEntity statusEntity, final UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		boolean sessionOk = true;
		//int scanCount = 0;
		int badCount = 0;
		for (XnatImagescandataI scan : session.getScans_scan()) {
			final String scanType = scan.getType();
			//scanCount+=1;
			if (scanType.toLowerCase().contains("physio") || scanType.toLowerCase().contains("phoenixzip")) {
				continue;
			}
			final String seriesDesc = scan.getSeriesDescription();
			final String scanId = scan.getId();
			boolean hasDicom = false;
			boolean hasSecondary = false;
			boolean hasSnapshots = false;
			boolean snapshotsOk = true;
			for (XnatAbstractresourceI resource : scan.getFile()) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				if (resource.getLabel().equals(CommonConstants.DICOM_RESOURCE)) {
					hasDicom = true;
				} else if (resource.getLabel().equalsIgnoreCase(CommonConstants.SNAPSHOTS_RESOURCE)) {
					hasSnapshots = true;
					snapshotsOk = ResourceCheckUtils.checkSnapshotFiles((XnatResourcecatalog)resource, infoSB, scanId, seriesDesc);
					
				} else if (resource.getLabel().equalsIgnoreCase(CommonConstants.SECONDARY_RESOURCE)) {
					hasSecondary = true;
				}
			}
			if ((!hasSnapshots && hasDicom) || (!hasSnapshots && !hasSecondary)) {
				badCount+=1;
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - Missing SNAPSHOTS resource</li>");
			} else if (!snapshotsOk) {
				badCount+=1;
				infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - NIFTI resource missing files.</li>");
			}
		}
		// Some snapshots cannot be generated.  Let's not fail validation if fewer than 5% of scans or only
		// a single scan are missing snapshots.
		//if (badCount>1 && ((double)badCount/(double)scanCount)>.05) {
		// Revert the above change for now due to plexiviewer improvements.
		if (badCount>=1) {
				sessionOk = false;
		}
		if (sessionOk) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo("");
			} 
		} else {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoSB.toString())) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(infoSB.toString());
			} 
		}
	}

}
