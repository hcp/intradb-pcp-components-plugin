package org.nrg.ccf.intradb.pcpcomponents.validation;

import java.util.Date;

import org.nrg.ccf.intradb.pcpcomponents.dicomheaderextraction.DicomHeaderExtractionValidator;
import org.nrg.ccf.intradb.pcpcomponents.shared.SessionCompletePrereqChecker;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class ValidationPrereqChecker extends SessionCompletePrereqChecker implements PipelinePrereqCheckerI {

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		final Boolean prereqs = statusEntity.getPrereqs();
		boolean sessionOk = hasCompletedWorkflows(statusEntity, user);
		if (sessionOk) {
			sessionOk = DicomHeaderExtractionValidator.doValidation(session, infoSB);
			if (sessionOk && (prereqs == null || !prereqs)) {
				statusEntity.setPrereqs(true);
				statusEntity.setPrereqsInfo("");
				statusEntity.setPrereqsTime(new Date());
			} else if (!sessionOk && (prereqs == null || prereqs || !statusEntity.getPrereqsInfo().equals(infoSB.toString()))) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo(infoSB.toString());
			}
			return;
		} 
		if (prereqs == null || prereqs) {
			statusEntity.setPrereqsInfo(SessionCompletePrereqChecker.FAILURE_MESSAGE);
			statusEntity.setPrereqs(false);
			statusEntity.setPrereqsTime(new Date());
		}
		
	}

}
