package org.nrg.ccf.intradb.pcpcomponents.validation;

import java.util.Date;
import java.util.List;

import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.model.ValProtocoldataConditionI;
import org.nrg.xdat.model.ValProtocoldataScanCheckConditionI;
import org.nrg.xdat.model.ValProtocoldataScanCheckI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.ValProtocoldata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

@PipelineValidator
public class ValidationValidator implements PipelineValidatorI {

	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user) {
		final org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
	      	cc.addClause("val:protocolData.imageSession_ID",statusEntity.getEntityId());
	      	cc.addClause("val:protocolData.project",statusEntity.getProject());
		final List<ValProtocoldata> validations = ValProtocoldata.getValProtocoldatasByField(cc, user, false);
		// TODO:  Can we make sure created date is greater than submit time here?
		if (validations.size()>=1) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
			}
			statusEntity.setIssues(false);
			statusEntity.setIssuesInfo("");
			statusEntity.setIssuesTime(new Date());
			XnatImagesessiondata imageSession = null;
			for (final ValProtocoldata val : validations) {
				if (val.getCheck_status().equalsIgnoreCase("fail")) {
					if (imageSession == null) {
						imageSession = XnatImagesessiondata.getXnatImagesessiondatasById(statusEntity.getEntityId(), user, false);
					}
					if (!sessionCleaned(val,imageSession)) {
						statusEntity.setIssues(true);
						statusEntity.setIssuesInfo(getIssuesInfoText(val, imageSession));
						statusEntity.setIssuesTime(new Date());
						break;
					}
				}
			}
		} else {
			if (statusEntity.getValidated()) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
			}
			statusEntity.setIssues(false);
			statusEntity.setIssuesInfo("");
			statusEntity.setIssuesTime(new Date());
		}
		final String entityLabel = statusEntity.getEntityLabel();
		if ((entityLabel.contains("7T") || entityLabel.contains("MRS")) && !statusEntity.getImpeded()) {
			statusEntity.setImpeded(true);
			statusEntity.setImpededInfo("No validation rules defined for 7T or MRS");
			statusEntity.setImpededTime(new Date());
		}
	}

	private boolean sessionCleaned(ValProtocoldata val, XnatImagesessiondata imageSession) {

		// Exclude as issues all sessions where all checks are failed scans that have since been set to unusable. 
		for (final ValProtocoldataConditionI sessionCheck : val.getCheck_conditions_condition()) {
			if (sessionCheck.getStatus().toUpperCase().contains("FAIL")) {
				return false;
			}
			
		}
		for (final ValProtocoldataScanCheckI scanCheck : val.getScans_scanCheck()) {
			if (scanCheck.getStatus().toUpperCase().contains("FAIL") && !scanUnusable(scanCheck,imageSession)) {
				return false;
			}
		}
		return true;
		
	}

	private boolean scanUnusable(ValProtocoldataScanCheckI scanCheck, XnatImagesessiondata imageSession) {
		final XnatImagescandataI scan = getScan(scanCheck, imageSession);
		return (scan != null) ? scan.getQuality().equalsIgnoreCase("UNUSABLE") : false;
	}

	private XnatImagescandataI getScan(ValProtocoldataScanCheckI scanCheck, XnatImagesessiondata imageSession) {
		final String scanID = scanCheck.getScanId();
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			if (scan.getId().equals(scanID)) {
				return scan;
			}
		}
		return null;
	}

	private String getIssuesInfoText(ValProtocoldata val, XnatImagesessiondata imageSession) {
		final StringBuffer sb = new StringBuffer();
		for (final ValProtocoldataConditionI sessionCheck : val.getCheck_conditions_condition()) {
			if (sessionCheck.getStatus().toUpperCase().contains("FAIL")) {
				sb.append("<li>Failed session check (CHECK_ID=" + sessionCheck.getId() + "):  " + 
						sessionCheck.getDiagnosis() + "</li>\n");
			}
			
		}
		for (final ValProtocoldataScanCheckI scanCheck : val.getScans_scanCheck()) {
			if (scanCheck.getStatus().toUpperCase().contains("FAIL")) {
				final String scanId = scanCheck.getScanId();
				for (final ValProtocoldataScanCheckConditionI check : scanCheck.getConditions_condition()) {
					if (check.getStatus().toUpperCase().contains("FAIL")) {
						final XnatImagescandataI scan = getScan(scanCheck, imageSession);
						sb.append("<li>Failed scan check (SCAN=" + scanId + ", SCAN_QUALITY=" + 
								((scan != null) ? scan.getQuality() : "UNKNOWN") +
								", CHECK_ID=" + check.getId() + "):  " + check.getDiagnosis() + "</li>\n");
					}
				}
			}
		}
		return (sb.length()>0) ? sb.toString() : "Validation current status is FAILED for this session.";
	}

}
