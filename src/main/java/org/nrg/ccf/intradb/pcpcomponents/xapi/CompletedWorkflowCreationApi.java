package org.nrg.ccf.intradb.pcpcomponents.xapi;

import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.common.utilities.utils.CcfWorkflowUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
//import lombok.extern.slf4j.Slf4j;

/*
 * NOTES ABOUT THIS CLASS:  The DicomHeaderExtraction prereq checker looks for an indication 
 * of session upload/transfer configuration.   Sometimes sessions are verified to be complete even
 * if we don't have an XSync workflow that XSync was complete and verified.  Sometimes we fix issues
 * with XSync transfers via script rather than doing a full send and we need a workflow for 
 * DicomExtractionPrereq's sake that indicates the XSync has been corrected and verified.
 */

//@Slf4j
@Lazy
@XapiRestController
@Api(description = "Create XSync/Session Complete Workflow API")
public class CompletedWorkflowCreationApi extends AbstractXapiRestController {
	
	@Autowired
	@Lazy
	public CompletedWorkflowCreationApi(UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
	}
	
	@ApiOperation(value = "Generate Xsync (FixedAndOrVerified) workflow", 
			notes = "Generate Xsync (FixedAndOrVerified) workflow", 
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/sessionVerificationWorkflow/project/{projectId}/experiment/{experimentId}/generateXsyncCorrectedWorkflow"}, restrictTo=AccessLevel.Owner,
    	method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> generateXsyncCorrectedWorkflow(@PathVariable("projectId") @ProjectId final String projectId,
    		@PathVariable("experimentId") final String experimentId) throws NrgServiceException {
		
		generateWorkflow(projectId, experimentId, "Xsync (FixedAndOrVerified)");
		return new ResponseEntity<Void>(HttpStatus.OK);
	
    }
	
	@ApiOperation(value = "Generate SessionVerifiedComplete workflow", 
			notes = "Generate SessionVerifiedComplete workflow", 
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/sessionVerificationWorkflow/project/{projectId}/experiment/{experimentId}/generateSessionVerifiedCompleteWorkflow"}, restrictTo=AccessLevel.Owner,
    	method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> generateSessionVerifiedCompleteWorkflow(@PathVariable("projectId") @ProjectId final String projectId,
    		@PathVariable("experimentId") final String experimentId) throws NrgServiceException {
		
		generateWorkflow(projectId, experimentId, "SessionVerifiedComplete");
		return new ResponseEntity<Void>(HttpStatus.OK);
	
    }

	private void generateWorkflow(String projectId, String experimentId, String workflowLabel) throws NrgServiceException {
		
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		
		if (mrSession == null) {
			throw new NrgServiceException("ERROR:  Requested experiment is null or is not an MR Session");
		}
		try {
			CcfWorkflowUtils.generateStatusCompleteWorkflow(getSessionUser(), mrSession.getItem(), workflowLabel);
		} catch (Exception e) {
			throw new NrgServiceException(e);
		}
		
	}
	
}
