package org.nrg.ccf.intradb.pcpcomponents.xsync;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.common.utilities.components.NodeUtils;
import org.nrg.ccf.common.utilities.components.XsyncUtils;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConstants.PcpStatus;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xsync.configuration.ProjectSyncConfiguration;
import org.springframework.http.ResponseEntity;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineSubmitter
public class XsyncSubmitter implements PipelineSubmitterI, ConfigurableComponentI {
	
	private final XsyncUtils _xsyncUtils = XDAT.getContextService().getBean(XsyncUtils.class);
	private final NodeUtils _nodeUtils = XDAT.getContextService().getBean(NodeUtils.class);
	public static final String CLASS_NAME = XsyncSubmitter.class.getName().replace(".", "");
	public static final String DEST_PROJECT_PART = "-dest-project";
	public static final String DEST_PROJECT_FIELD = CLASS_NAME + DEST_PROJECT_PART;

	@Override
	public List<String> getConfigurationYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"destination-project:\n" +
				"    id: " + DEST_PROJECT_FIELD + "\n" +
				"    name: " + DEST_PROJECT_FIELD + "\n" +
				"    kind:  panel.input.text\n" + 
				"    label: XSync Destination Project:\n" +  
				"    description:  (OPTIONAL)  NOTE:  This configuration field is only used if XSync is not set up  " +
				"                  and only local syncs are being used.  Otherwise, the destination project configured " +
				"                  in XSync will be used";
		returnList.add(ele);
		return returnList;
	}
	
	@Override
	public List<String> getParametersYaml(String projectId, String pipelineId) {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"overwriteExisting:\n" +
				"    id: overwriteExisting\n" +
				"    kind: panel.input.switchbox\n" +
				"    label: Overwrite Existing Session?\n" +
				"    value: false\n" +
				"    onText: Yes\n" +
				"    offText: No\n" +
				"preserveNotesAndRatings:\n" +
				"    id: preserveNotesAndRatings\n" +
				"    kind: panel.input.switchbox\n" +
				"    label: Preserve Notes and Ratings?\n" +
				"    description: WARNING: When yes, any quality ratings from the source session scans other than 'unusable' ratings will be " +
					"replaced with non-unusable ratings from any corresponding existing destination session scans\n" +
				"    value: true\n" +
				"    onText: Yes\n" +
				"    offText: No\n" +
				"useLocalSync:\n" +
				"    id: useLocalSync\n" +
				"    kind: panel.input.switchbox\n" +
				"    label: Use local sync instead of XSync?\n" +
				"    description: (Recommended)  When yes, bypasses XSync and uses a more efficient sync operation, however " +
					"syncing is currently limited to a full copy of the local session, containing all resources.  Currently " +
					"this option obtains the project from XSync configuration, so XSync must be configured for the project.\n" +
				"    value: true\n" +
				"    onText: Yes\n" +
				"    offText: No\n";
		returnList.add(ele);
		return returnList;
	}
	
	@Override
	public boolean submitJob(PcpStatusEntity entity, PcpStatusEntityService entityService, PipelineValidatorI validator,
			Map<String, String> parameters, UserI user) throws PcpSubmitException {
		final boolean overwriteExisting = (parameters.get("overwriteExisting") != null) ? Boolean.valueOf(parameters.get("overwriteExisting")) : false;
		final ProjectSyncConfiguration xsyncConfig = _xsyncUtils.getProjectSyncConfiguration(entity.getProject(), user);
		String remoteProject = null;
		if (xsyncConfig != null) {
			remoteProject = xsyncConfig.getProjectSyncConfigurationFromDB().getSyncinfo().getRemoteProjectId();
		}
		if (remoteProject == null || remoteProject.trim().length()==0) {
			try {
				remoteProject = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(), 
								this.getClass().getName(), DEST_PROJECT_PART, false).toString();
			} catch (PcpAutomationScriptExecutionException e) {
				// Do nothing
			}
		}
		if (remoteProject == null) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("Remote project is not defined in this project's configuration");
			entity.setStatusTime(new Date());
			entityService.update(entity);
			return false;
		}
		final XnatMrsessiondata oldRemoteSession = ExperimentUtils.getMrsessionByLabel(remoteProject, entity.getEntityLabel(), user);
		if (!overwriteExisting) {
			if (oldRemoteSession != null) {
				entity.setStatus(PcpStatus.ERROR);
				entity.setStatusInfo("Destination session exists and overwriteExisting parameter is 'false'.");
				entity.setStatusTime(new Date());
				entityService.update(entity);
				return false;
			}
		}
		final boolean preserveNotes = (parameters.get("preserveNotesAndRatings") != null) ? Boolean.valueOf(parameters.get("preserveNotesAndRatings")) : true;
		final boolean useLocalSync = (parameters.get("useLocalSync") != null) ? Boolean.valueOf(parameters.get("useLocalSync")) : true;
		if (!useLocalSync && (xsyncConfig == null || !xsyncConfig.getProjectSyncConfigurationFromDB().getSyncEnabled())) {
			entity.setStatus(PcpStatus.ERROR);
			entity.setStatusInfo("ERROR:  XSync is not currently enabled and the local sync (non-XSync) option was not selected.");
			entity.setStatusTime(new Date());
			entityService.update(entity);
			return false;
		}
		try {
			entity.setStatus(PcpStatus.RUNNING);
			final Date statusTime = new Date();
			entity.setStatusInfo("Status set to running at (TIME=" + statusTime + ", NODE=" + _nodeUtils.getXnatNode() + ")");
			entity.setStatusTime(statusTime);
			entityService.update(entity);
			ResponseEntity<String> response = (useLocalSync) ? 
						_xsyncUtils.localSyncSingleExperiment(entity.getEntityId(), user, remoteProject) :
						_xsyncUtils.syncSingleExperiment(entity.getEntityId(), user, true);
			if (response.getStatusCode().is2xxSuccessful()) {
				entity.setStatus(PcpStatus.COMPLETE);
				entity.setStatusInfo("Session successfully synced");
				entity.setStatusTime(new Date());
				entityService.update(entity);
				if (preserveNotes && (oldRemoteSession != null)) {
					XnatMrsessiondata newRemoteSession = ExperimentUtils.getMrsessionByLabel(remoteProject, entity.getEntityLabel(), user);
					if (restoreNotesAndQualityRatings(newRemoteSession, oldRemoteSession)) {
						try {
							SaveItemHelper.authorizedSave(newRemoteSession, user, false, true, 
									EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.MODIFY_VIA_WEB_SERVICE, null, null));
						} catch (Exception e) {
							log.error("ERROR:  Couldn't update session to restore notes and ratings (SESSION=" + newRemoteSession.getLabel() +  ")");
						}
					};
				}
				return true;
			} else {
				entity.setStatus(PcpStatus.ERROR);
				entity.setStatusInfo("ERROR:  Sync process not successful - (HTTPStatus=" + response.getStatusCode() + ", Response=" + 
						response.getBody() + ")"
				);
			}
		} catch (Exception e) {
				entity.setStatus(PcpStatus.ERROR);
				entity.setStatusInfo("ERROR:  Sync process threw an exception - " + e.toString());
		}
		entity.setStatusTime(new Date());
		entityService.update(entity);
		return false;
		
	}

	private boolean restoreNotesAndQualityRatings(XnatMrsessiondata newRemoteSession, XnatMrsessiondata oldRemoteSession) {
		boolean isModified = false;
		if (newRemoteSession == null) {
			return isModified;
		}
		final String oldNote = oldRemoteSession.getNote();
		if (oldNote != null && !oldNote.equals(newRemoteSession.getNote()) && oldNote.length()>0) {
			isModified = true;
			newRemoteSession.setNote(((newRemoteSession.getNote()!=null) ? newRemoteSession.getNote() : "") +  oldNote);
		}
		final List<XnatImagescandataI> newScans = newRemoteSession.getScans_scan();
		for (final XnatImagescandataI oldScan : oldRemoteSession.getScans_scan()) {
			final XnatImagescandataI newScan = ExperimentUtils.getCorrespondingScan(newScans, oldScan);
			if (newScan == null) {
				continue;
			}
			if (!newScan.getQuality().equalsIgnoreCase("unusable") && !oldScan.getQuality().equalsIgnoreCase("unusable") &&
					!newScan.getQuality().equals(oldScan.getQuality())) {
				isModified = true;
				newScan.setQuality(oldScan.getQuality());
			}
			final String oldScanNote = oldScan.getNote();
			if (oldScanNote != null && !oldScanNote.equals(newScan.getNote()) && oldScanNote.length()>0) {
				isModified = true;
				newScan.setNote(((newScan.getNote()!=null) ? newScan.getNote() : "") +  oldScan.getNote());
			}
		}
		return isModified;
	}
	
}
