package org.nrg.ccf.intradb.pcpcomponents.xsync;

import java.util.Date;

import org.nrg.ccf.common.utilities.components.XsyncUtils;
import org.nrg.ccf.common.utilities.pojos.SessionComparisonAdvice;
import org.nrg.ccf.common.utilities.pojos.SessionComparisonResult;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.constants.PcpConstants.PcpStatus;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcpcomponents.abst.AbstractPipelineValidator;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xsync.configuration.ProjectSyncConfiguration;
import com.google.common.collect.ImmutableList;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineValidator
public class XsyncValidator extends AbstractPipelineValidator implements PipelineValidatorI {
	
	private final XsyncUtils _xsyncUtils = XDAT.getContextService().getBean(XsyncUtils.class);
	private SessionComparisonAdvice _comparisonAdvice = 
			new SessionComparisonAdvice(ImmutableList.<String>of(), ImmutableList.<String>of(), 
					ImmutableList.of("NIFTI","SNAPSHOTS","DEFACE_QC"), ImmutableList.of("DICOM","DICOM_ORIG"));
	
	public void setComparisonAdvice(SessionComparisonAdvice comparisonAdvice) {
		_comparisonAdvice = comparisonAdvice;
	}
	
	@Override
	public void validate(PcpStatusEntity entity, UserI user) {
		log.info("Begin XSyncUtils validation:  " + entity.getEntityLabel());
		
		final ProjectSyncConfiguration xsyncConfig = _xsyncUtils.getProjectSyncConfiguration(entity.getProject(), user);
		String remoteProject = null;
		if (xsyncConfig != null) {
			remoteProject = xsyncConfig.getProjectSyncConfigurationFromDB().getSyncinfo().getRemoteProjectId();
		}
		if (remoteProject == null || remoteProject.trim().length()==0) {
			try {
				remoteProject = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(), 
								XsyncSubmitter.class.getName(), XsyncSubmitter.DEST_PROJECT_PART, false).toString();
			} catch (PcpAutomationScriptExecutionException e) {
				// Do nothing
			}
		}
		if (remoteProject == null) {
			validationFailed(entity, "Remote project is not defined in this project's configuration");
			return;
		}
		final XnatImagesessiondata remoteSession = ExperimentUtils.getImagesessionByLabel(remoteProject, entity.getEntityLabel(), user);
		if (remoteSession == null) {
			validationFailed(entity, "No matching session in the destination project");
			return;
		}
		final XnatImagesessiondata localSession = ExperimentUtils.getImagesessionByLabel(entity.getProject(), entity.getEntityLabel(), user);
		if (localSession == null) {
			validationFailed(entity, "Could not return local session");
			return;
		}
		final SessionComparisonResult compareResult = ExperimentUtils.compareSessions(localSession, remoteSession, _comparisonAdvice); 
		validationPassed(entity, "Remote session exists");
		final SessionComparisonResult customResult = doCustomSessionComparison(localSession, remoteSession, _comparisonAdvice);
		if (compareResult.getIsMatch() && compareResult.getIsMatch()) {
			noIssuesRaised(entity, "Remote session matches local session");
		} else if (!customResult.getIsMatch()) {
			issuesRaised(entity, "Remote session does not match local session:  " + customResult.getResult());
		} else { 
			issuesRaised(entity, "Remote session does not match local session:  " + compareResult.getResult());
		}
		log.info("XSyncUtils validation complete:  " + entity.getEntityLabel());
		return;
	}
	
	public SessionComparisonResult doCustomSessionComparison(XnatImagesessiondata localSession, XnatImagesessiondata remoteSession, 
			SessionComparisonAdvice _comparisonAdvice2) {
		// No custom comparisons in default class.  This is meant to be overridden.
		return SessionComparisonResult.matches();
	}
	
}
