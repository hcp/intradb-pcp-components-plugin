package org.nrg.ccf.intradb.pcpcomponents.xsync.projectspecific;

import java.util.HashMap;
import java.util.Map;

import org.nrg.ccf.common.utilities.pojos.SessionComparisonAdvice;
import org.nrg.ccf.intradb.pcpcomponents.xsync.XsyncValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;

import com.google.common.collect.ImmutableList;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@PipelineValidator
public class XSyncValidator_MDD extends XsyncValidator {
	
	public XSyncValidator_MDD() {
		super();
		SessionComparisonAdvice advice = new SessionComparisonAdvice(ImmutableList.<String>of(), ImmutableList.<String>of(), 
				ImmutableList.of("NIFTI","LINKED_DATA"), ImmutableList.of("DICOM"));
		Map<String, Map<String, String>> alternates = advice.getAlternateFilePathTransforms();
		Map<String,String> transformMap = new HashMap<String, String>();
		transformMap.put("/EVs/","/EVs_ORIG/");
		alternates.put("^.*/EVs/.*$", transformMap);
		setComparisonAdvice(advice);
	}

}
